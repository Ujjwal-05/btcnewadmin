import React from 'react';

const data = [
  {
    "Start Date *": "2024-06-15",
    "End Date *": "2024-06-20",
    "Recurring Behavior of order by value (%)": [
      {
        "By Value": "User Category I",
        "Dine-in": "16.20",
        "Delivery": "9.92",
        "Takeaway": "3.35"
      },
      {
        "By Value": "User Category II",
        "Dine-in": "26.96",
        "Delivery": "15.58",
        "Takeaway": "4.74"
      },
      {
        "By Value": "User Category III",
        "Dine-in": "12.60",
        "Delivery": "8.06",
        "Takeaway": "2.59"
      }
    ],
    "Recurring Behavior of order by number(%)": [
      {
        "By Number": "User Category I",
        "Dine-in": "16.00",
        "Delivery": "2.14",
        "Takeaway": "4.13"
      },
      {
        "By Number": "User Category II",
        "Dine-in": "29.33",
        "Delivery": "7.71",
        "Takeaway": "6.19"
      },
      {
        "By Number": "User Category III",
        "Dine-in": "21.87",
        "Delivery": "8.44",
        "Takeaway": "4.20"
      }
    ],
    "Recurring Behavior (AOV)": [
      {
        "AOV": "User Category I",
        "Dine-in": "478.00",
        "Delivery": "2,192.00",
        "Takeaway": "383.00"
      },
      {
        "AOV": "User Category II",
        "Dine-in": "434.00",
        "Delivery": "953.00",
        "Takeaway": "361.00"
      },
      {
        "AOV": "User Category III",
        "Dine-in": "272.00",
        "Delivery": "451.00",
        "Takeaway": "291.00"
      }
    ]
  }
];

const Table = ({ data }) => {
  return (
    <table className="w-full text-left text-sm font-light">
      <thead className="text-xs uppercase bg-gray-50">
        <tr>
          <th className="px-6 py-3">Category</th>
          <th className="px-6 py-3">Dine-in</th>
          <th className="px-6 py-3">Delivery</th>
          <th className="px-6 py-3">Takeaway</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <React.Fragment key={index}>
            {Object.entries(item).map(([key, value]) => (
              <React.Fragment key={key}>
                <tr>
                  <td colSpan="4" className="px-6 py-4 bg-gray-100 font-medium">
                    {key}
                  </td>
                </tr>
                {Array.isArray(value) && value.map((subItem, subIndex) => (
                  <tr key={subIndex}>
                    <td className="px-6 py-4 font-medium">{subItem[Object.keys(subItem)[0]]}</td>
                    <td className="px-6 py-4">{subItem['Dine-in']}</td>
                    <td className="px-6 py-4">{subItem['Delivery']}</td>
                    <td className="px-6 py-4">{subItem['Takeaway']}</td>
                  </tr>
                ))}
                {!Array.isArray(value) && (
                  <tr>
                    <td colSpan="4" className="px-6 py-4">{value}</td>
                  </tr>
                )}
              </React.Fragment>
            ))}
          </React.Fragment>
        ))}
      </tbody>
    </table>
  );
};

const Behaviour = () => {
  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4">Behaviour Data</h1>
      <Table data={data} />
    </div>
  );
};

export default Behaviour;
