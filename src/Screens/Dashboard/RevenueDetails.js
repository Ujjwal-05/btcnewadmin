import React, { useState } from 'react';

const RevenueDetails = () => {
  const [startDate, setStartDate] = useState('2024-06-15');
  const [endDate, setEndDate] = useState('2024-06-20');

  const handleStartDateChange = (event) => {
    setStartDate(event.target.value);
  };

  const handleEndDateChange = (event) => {
    setEndDate(event.target.value);
  };

  const handleSearch = () => {
    // Handle search logic here
    console.log('Start Date:', startDate);
    console.log('End Date:', endDate);
  };

  return (
    <div className="container mx-auto p-4">
      <h2 className="text-2xl font-bold mb-4">Revenue Details</h2>

      {/* Lifetime Collection */}
      <div className="bg-white shadow-md rounded-lg p-4 mb-4">
        <h3 className="text-xl font-bold mb-2">Lifetime Collection</h3>
        <table className="w-full">
          <tbody>
            <TableRow label="Number of orders:" value="183,024" />
            <TableRow label="Sales value (Net of discount):" value="₹72,045,604" />
            <TableRow label="Total loyalty points earned:" value="3,775,182" />
          </tbody>
        </table>
      </div>

      {/* Orders Details */}
      <div className="bg-white shadow-md rounded-lg p-4 mb-4">
        <div className="flex items-center justify-between mb-4">
          <div className="flex items-center">
            <input
              type="date"
              className="border rounded-md p-2 mr-2"
              value={startDate}
              onChange={handleStartDateChange}
            />
            <input
              type="date"
              className="border rounded-md p-2"
              value={endDate}
              onChange={handleEndDateChange}
            />
          </div>
          <button
            className="bg-cyan-300 hover:bg-cyan-200 text-white font-bold py-2 px-4 rounded"
            onClick={handleSearch}
          >
            SEARCH
          </button>
        </div>
        <h3 className="text-xl font-bold mb-2">Orders Details</h3>
        <table className="w-full">
          <tbody>
            <TableRow label="Number of orders:" value="2,762" />
            <TableRow label="Sales value (Net of discount):" value="₹1,303,404" />
            <TableRow label="Loyalty points used:" value="15,222" />
          </tbody>
        </table>
      </div>
    </div>
  );
};

const TableRow = ({ label, value }) => (
  <tr>
    <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{label}</td>
    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{value}</td>
  </tr>
);

export default RevenueDetails;
