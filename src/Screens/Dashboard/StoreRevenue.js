import React from 'react';

const StoreRevenue = () => {
  const data = [
    {
      storeName: 'Roastery',
      revenue: 461553,
    },
    {
      storeName: 'Bakery',
      revenue: 2762,
    },
    {
        storeName: 'Connaught Place',
        revenue: 2762,
      },
      {
        storeName: 'Koramangala',
        revenue: 2762,
      },
      {
        storeName: 'Khan Market',
        revenue: 2762,
      },
    // Add more store data here if needed
  ];

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Store Revenue</h1>
      <div className="overflow-x-auto">
        <table className="min-w-full bg-white border-gray-200 shadow-md rounded-lg overflow-hidden">
          <thead className="bg-gray-200">
            <tr>
              <th className="px-6 py-4 text-left">Store Name</th>
              <th className="px-6 py-4 text-left">Revenue (INR)</th>
            </tr>
          </thead>
          <tbody className="divide-y divide-gray-200">
            {data.map((store, index) => (
              <tr key={index} className={index % 2 === 0 ? 'bg-gray-100' : 'bg-white'}>
                <td className="px-6 py-4 whitespace-nowrap">{store.storeName}</td>
                <td className="px-6 py-4 whitespace-nowrap">{store.revenue.toLocaleString()}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default StoreRevenue;
