import React from 'react';

const ProductRevenue = () => {
  const data = [
    {
      productName: 'Cappuccino',
      price: 230,
      quantity: 354,
      productTotal: 81420,
      discount: 267,
      netAmount: 81153,
      sharing: 6.28,
    },
    {
      productName: 'Vietnamese Style Iced Coffee',
      price: 240,
      quantity: 285,
      productTotal: 68400,
      discount: 567,
      netAmount: 67833,
      sharing: 5.25,
    },
    {
      productName: 'Vienna Roast',
      price: 870,
      quantity: 80,
      productTotal: 69600,
      discount: 3980,
      netAmount: 65621,
      sharing: 5.08,
    },
    {
      productName: 'Iced Latte',
      price: 260,
      quantity: 140,
      productTotal: 36400,
      discount: 250,
      netAmount: 36150,
      sharing: 2.8,
    },
    {
      productName: 'Silver Oak Cafe Blend',
      price: 470,
      quantity: 72,
      productTotal: 33840,
      discount: 2272,
      netAmount: 31569,
      sharing: 2.44,
    },
    {
      productName: 'French Roast',
      price: 470,
      quantity: 68,
      productTotal: 31960,
      discount: 976,
      netAmount: 30985,
      sharing: 2.4,
    },
    {
      productName: 'Iced Cappuccino',
      price: 265,
      quantity: 114,
      productTotal: 30210,
      discount: 0,
      netAmount: 30210,
      sharing: 2.34,
    },
    {
      productName: 'Attikan Estate',
      price: 490,
      quantity: 66,
      productTotal: 32340,
      discount: 2918,
      netAmount: 29422,
      sharing: 2.28,
    },
    {
      productName: 'Iced Americano',
      price: 220,
      quantity: 128,
      productTotal: 28160,
      discount: 0,
      netAmount: 28160,
      sharing: 2.18,
    },
  ];

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Product Revenue</h1>
      <div className="overflow-x-auto">
        <table className="w-full table-auto border-collapse border border-gray-300">
          <thead>
            <tr className="bg-gray-200">
              <th className="px-6 py-4 text-left">Product Name</th>
              <th className="px-6 py-4 text-left">Price (INR)</th>
              <th className="px-6 py-4 text-left">Quantity</th>
              <th className="px-6 py-4 text-left">Product Total</th>
              <th className="px-6 py-4 text-left">Discount</th>
              <th className="px-6 py-4 text-left">Net Amount</th>
              <th className="px-6 py-4 text-left">Sharing (%)</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item, index) => (
              <tr key={index} className={index % 2 === 0 ? 'bg-white' : 'bg-gray-100'}>
                <td className="px-6 py-4">{item.productName}</td>
                <td className="px-6 py-4">{item.price}</td>
                <td className="px-6 py-4">{item.quantity}</td>
                <td className="px-6 py-4">{item.productTotal}</td>
                <td className="px-6 py-4">{item.discount}</td>
                <td className="px-6 py-4">{item.netAmount}</td>
                <td className="px-6 py-2">{item.sharing}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ProductRevenue;
