import React from 'react';
import Sidebar from '../../Components/Sidebar';
import RevenueDetails from '../Dashboard/RevenueDetails';
import Behaviour from '../Dashboard/Behaviour';
import ProductRevenue from '../Dashboard/ProductRevenue';
import StoreRevenue from '../Dashboard/StoreRevenue';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const Dashboard = () => {
  return (
    <div className="flex">

      {/* Main Content */}
      <div className="flex-1">
        <div className="container mx-auto px-4 py-8">
          <div className="dashboard bg-white rounded-lg p-4 shadow-md">
            {/* Header */}
            <div className="header flex justify-between items-center mb-8">
              <h1 className="text-2xl font-bold">Dashboard</h1>
              <div className="user flex items-center">
                <span className="text-xl font-semibold mr-2">UJJWAL CHAUDHARY</span>
                <img
                  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200&d=mp&r=g"
                  alt="User Avatar"
                  className="w-12 h-12 rounded-full"
                />
              </div>
            </div>

            {/* Date Selector and Search Button */}
            <div className="input-group mb-8">
              <input
                type="date"
                placeholder="Start Date"
                className="p-2 border border-gray-300 rounded-lg"
              />
              <input
                type="date"
                placeholder="End Date"
                className="p-2 border border-gray-300 rounded-lg ml-4"
              />
              <button className="bg-cyan-300 text-white px-5 py-2 rounded-lg ml-4">
                SEARCH
              </button>
            </div> 

            {/* Dashboard Cards */}
            <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
              {/* Sign up User */}
              <div className="card bg-red-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">638</h2>
                <p className="text-lg ml-4 mb-4 font-light">Sign up User</p>
              </div>

              {/* Android User */}
              <div className="card bg-purple-600 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">278</h2>
                <p className="text-lg ml-4 mb-4 font-light">Android User</p>
              </div>

              {/* iOS User */}
              <div className="card bg-blue-400 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">357</h2>
                <p className="text-lg ml-4 mb-4 font-light">iOS User</p>
              </div>

              {/* Referral Users */}
              <div className="card bg-teal-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">82</h2>
                <p className="text-lg ml-4 mb-4 font-light">Referral Users</p>
              </div>

              {/* Total Loyalty Points */}
              <div className="card bg-orange-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">64,899</h2>
                <p className="text-lg ml-4 mb-4 font-light">Total Loyalty Points</p>
              </div>

              {/* Redeemed Loyalty Points */}
              <div className="card bg-green-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">13,897</h2>
                <p className="text-lg ml-4 mb-4 font-light">Redeemed Loyalty Points</p>
              </div>

              {/* Inactive Users */}
              <div className="card bg-gray-700 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">0</h2>
                <p className="text-lg ml-4 mb-4 font-light">Inactive Users</p>
              </div>

              {/* Razorpay Collection */}
              <div className="card bg-indigo-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">₹1295765</h2>
                <p className="text-lg ml-4 mb-4 font-light">Razorpay Collection</p>
              </div>

              {/* Paytm Collection */}
              <div className="card bg-pink-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">₹259765</h2>
                <p className="text-lg ml-4 mb-4 font-light">Paytm Collection</p>
              </div>

              {/* Cash Collection */}
              <div className="card bg-yellow-500 rounded-lg text-white relative flex flex-col justify-end items-start shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">₹54987</h2>
                <p className="text-lg ml-4 mb-4 font-light">Cash Collection</p>
              </div>
            </div>
             <RevenueDetails />
            <Behaviour />
            <ProductRevenue />
            <StoreRevenue /> 
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
