import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import RevenueDetails from '../Dashboard/RevenueDetails';
import Behaviour from '../Dashboard/Behaviour';
import ProductRevenue from '../Dashboard/ProductRevenue';
import StoreRevenue from '../Dashboard/StoreRevenue';

// Add all solid icons to the library
library.add(fas);

const HomePage = () => {
  return (
    <div className="flex justify-center">
      {/* Main Content */}
      <div className="flex-1">
        <div className="container mx-auto px-4 py-8">
          <div className="dashboard bg-white rounded-lg p-4 shadow-md">
            {/* Header */}
            <div className="header flex justify-between items-center mb-8">
              <h1 className="text-2xl font-bold">Dashboard</h1>
              <div className="user flex items-center">
                <span className="text-xl font-semibold mr-2">UJJWAL CHAUDHARY</span>
                <img
                  src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200&d=mp&r=g"
                  alt="User Avatar"
                  className="w-12 h-12 rounded-full"
                />
              </div>
            </div>

            {/* Date Selector and Search Button */}
            <div className="input-group mb-8">
              <input
                type="date"
                placeholder="Start Date"
                className="p-3 border border-gray-300 rounded-lg"
              />
              <input
                type="date"
                placeholder="End Date"
                className="p-3 border border-gray-300 rounded-lg ml-4"
              />
              <button className="bg-cyan-300 hover:bg-cyan-200 text-white px-6 py-3 rounded-lg ml-4">
                SEARCH
              </button>
            </div>

            {/* Dashboard Cards */}
            <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
              {/* Sign up User */}
              <div className="card bg-red-500 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-red-600 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">638</h2>
                <p className="text-lg ml-4 mb-4 font-light">Sign up User</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'user']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Android User */}
              <div className="card bg-purple-600 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-purple-700 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">278</h2>
                <p className="text-lg ml-4 mb-4 font-light">Android User</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fab', 'android']} className="text-white text-2xl" />
                </div>
              </div>

              {/* iOS User */}
              <div className="card bg-blue-400 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-blue-500 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">357</h2>
                <p className="text-lg ml-4 mb-4 font-light">iOS User</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'robot']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Referral Users */}
              <div className="card bg-teal-500 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-teal-600 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">82</h2>
                <p className="text-lg ml-4 mb-4 font-light">Referral Users</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'users']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Total Loyalty Points */}
              <div className="card bg-orange-500 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-orange-600 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">64,899</h2>
                <p className="text-lg ml-4 mb-4 font-light">Total Loyalty Points</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'coins']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Redeemed Loyalty Points */}
              <div className="card bg-green-500 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-green-600 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">13,897</h2>
                <p className="text-lg ml-4 mb-4 font-light">Redeemed Loyalty Points</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'gift']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Inactive Users */}
              <div className="card bg-gray-700 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-gray-800 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">0</h2>
                <p className="text-lg ml-4 mb-4 font-light">Inactive Users</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'user-slash']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Razorpay Collection */}
              <div className="card bg-indigo-500 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-indigo-600 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">₹1295765</h2>
                <p className="text-lg ml-4 mb-4 font-light">Razorpay Collection</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'credit-card']} className="text-white text-2xl" />
                </div>
              </div>

              {/* Paytm Collection */}
              <div className="card bg-pink-500 rounded-lg text-white relative flex flex-col justify-end items-start transition duration-300 ease-in-out hover:bg-pink-600 shadow-md h-64">
                <h2 className="text-3xl ml-4 mb-2 font-light">₹189764</h2>
                <p className="text-lg ml-4 mb-4 font-light">Paytm Collection</p>
                <div className="icon absolute bottom-4 right-4 w-10 h-10 flex justify-center items-center">
                  <FontAwesomeIcon icon={['fas', 'mobile-alt']} className="text-white text-2xl" />
                </div>
              </div>
            </div>

            {/* Graphs and Reports */}
            <div className="flex flex-col mt-8 space-y-8">
              {/* Row 1 */}
              <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
                {/* Revenue Details */}
                <div className="bg-white p-4 rounded-lg shadow-md h-96">
                  <RevenueDetails />
                </div>

                {/* Behaviour */}
                <div className="bg-white p-4 rounded-lg shadow-md h-96">
                  <Behaviour />
                </div>
              </div>

              {/* Row 2 */}
              <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
                {/* Product Revenue */}
                <div className="bg-white p-4 rounded-lg shadow-md h-96">
                  <ProductRevenue />
                </div>

                {/* Store Revenue */}
                <div className="bg-white p-4 rounded-lg shadow-md h-96">
                  <StoreRevenue />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
