import React, { useState } from 'react';
import LoginLayout from '../../Components/LoginLayout';
import LoadingSpinner from '../../Components/LoadingSpinner'; // Import LoadingSpinner component
import { useNavigate } from 'react-router-dom';

const OTPVerify = () => {
  const [isLoading, setIsLoading] = useState(false); // State to control loading animation
  const navigate = useNavigate();

  const onContinueClick = () => {
    setIsLoading(true); // Show loading animation
    // Simulate an API call or any asynchronous operation
    setTimeout(() => {
      setIsLoading(false); // Hide loading animation
      navigate('/dashboard');
    }, 2000); // Replace with your actual async operation
  };

  const onCancelClick = () => {
    navigate('/');
  };

  return (
    <>
      {isLoading && <LoadingSpinner />} {/* Render LoadingSpinner when isLoading is true */}
      <div className="flex flex-col items-center justify-center min-h-screen bg-cyan-400">
        <div className="max-w-lg w-full">
          <LoginLayout 
            isLogin={false} 
            isVerifOtp={true} 
            buttonClick={onContinueClick} 
            cancelClick={onCancelClick} 
          />
        </div>

        <div className="flex justify-end mt-4">
          <button
            onClick={onCancelClick}
            type="button"
            className="w-20 p-2 mr-2 font-light text-white bg-stone-300 rounded-md hover:bg-stone-400 focus:outline-none focus:ring-2 focus:ring-stone-500"
          >
            Cancel
          </button>
          <button
            onClick={onContinueClick}
            type="button"
            className="w-20 p-2 font-light text-white bg-cyan-400 rounded-md hover:bg-cyan-400 "
          >
            Continue
          </button>
        </div>
      </div>
    </>
  );
};

export default OTPVerify;
