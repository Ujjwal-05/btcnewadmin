import React, { useState } from 'react';
import LoginLayout from '../../Components/LoginLayout'; // Adjust the path as per your project structure
import LoadingSpinner from '../../Components/LoadingSpinner'; // Adjust the path as per your project structure
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState('');
  const navigate = useNavigate();

  const onPhoneNumberChange = (event) => {
    let formattedNumber = event.target.value;
    // Remove any non-numeric characters from the input
    formattedNumber = formattedNumber.replace(/\D/g, '');
    // Limit the input to 10 digits after the '+91' prefix
    if (formattedNumber.length > 10) {
      formattedNumber = formattedNumber.slice(0, 10);
    }
    // Prepend "+91" to the formatted number
    formattedNumber = '+91' + formattedNumber;
    setPhoneNumber(formattedNumber);
  };

  const onButtonClick = () => {
    setIsLoading(true);
    // Simulate an API call or any asynchronous operation
    setTimeout(() => {
      setIsLoading(false);
      navigate('/otpVerify');
    }, 2000);
  };

  return (
    <>
      {isLoading && <LoadingSpinner />}
      <LoginLayout
        isLogin={true}
        isVerifOtp={false}
        buttonClick={onButtonClick}
        phoneNumber={phoneNumber} // Pass formatted phoneNumber as prop
        onPhoneNumberChange={onPhoneNumberChange} // Pass onChange handler as prop
      />
    </>
  );
};

export default Login;
