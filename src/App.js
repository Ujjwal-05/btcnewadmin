import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import MixBags from './Components/Mixbags';
import Grinds from './Components/Grinds';
import Login from './Screens/Login';
import OTPVerify from './Screens/OtpVerify';
import HomePage from './Screens/HomePage';
import Frequencies from './Components/Frequencies';
import FrequencyDuration from './Components/Frequencyduration';
import ProductPackSizes from './Components/ProductPackSizes';
import ProductAttributes from './Components/ProductAttributes';
import ProductTags from './Components/ProductTags';
import SamplerPackCoffee from './Components/SamplerPackCoffee';
import User from './Components/UsersComp/User';
import UserGroup from './Components/UsersComp/UserGroup';
import UserPermission from './Components/UsersComp/UserPermission';
import BTCAppMainCategory from './Components/ProductComp/BTCAppMainCategory';
import BTCAppSubCategory from './Components/ProductComp/BTCAppSubCategory';
import ProductGrouping from './Components/ProductComp/ProductGrouping';
import SyncProducts from './Components/ProductComp/SyncProducts';
import ProductCollection from './Components/ProductComp/ProductCollection';
import Pack from './Components/Pack';
import Subscription from './Components/Subscription';
import Discount from './Components/Discount';
import Feed from './Components/Feed';
import PaymentMethod from './Components/PaymentMethod';
import Banner from './Components/Banner';
import NotificationList from './Components/NotificationList';
import Store from './Components/Store';
import Referral from './Components/Referral';
import Order from './Components/Order';
import BuildSetup from './Components/BuildSetup';
import Settings from './Components/Settings';
import RedeemLoyaltyPointsOnProduct from './Components/RedeemLoyaltyPointsOnProduct';
import PointsOnWalletTopup from './Components/PointsOnWalletTopup';
import EarnLoyaltyPoints from './Components/EarnLoyaltyPoints';
import SubscriptionSummary from './Components/SubscriptionSummary';
import TotalPaymentCollection from './Components/TotalPaymentCollection';
import OrderSummary from './Components/OrderSummary';
import Dashboard from './Screens/Dashboard/Dashboard';
import Navbar from './Components/navbar.js';
import Sidebar from './Components/Sidebar';
import Product from './Components/Product.js';
import AddCategory from './Components/ProductComp/AddCategory.js';
import EditSubCategory from './Components/ProductComp/EditSubCategory.js';
import EditProduct from './Components/ProductComp/EditProduct.js';
import EditSync from './Components/ProductComp/EditSync.js';
import EditCollection from './Components/ProductComp/EditCollection.js';

function App() {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  return (
    <Router>
      <div >
        <Sidebar isOpen={isSidebarOpen} />
        <div className={`flex-1 transition-all duration-300 ease-in-out ${isSidebarOpen ? 'ml-64' : 'ml-0'}`}>
          <Navbar toggleSidebar={toggleSidebar} />
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/otpVerify" element={<OTPVerify />} />
            <Route path="/home" element={<HomePage />} />
            <Route path="/mixbags" element={<MixBags data={[
              { id: 1, name: 'Attikan Estate', weight: '500g', active: true },
              { id: 2, name: 'Attikan Estate', weight: '250g', active: true }
            ]} />} />
            <Route path="/grinds" element={<Grinds />} />
            <Route path="/frequencies" element={<Frequencies />} />
            <Route path="/frequency-duration" element={<FrequencyDuration />} />
            <Route path="/product-pack-sizes" element={<ProductPackSizes />} />
            <Route path="/product-attributes" element={<ProductAttributes />} />
            <Route path="/product-tags" element={<ProductTags />} />
            <Route path="/sampler-pack-coffee" element={<SamplerPackCoffee />} />
            <Route path="/user" element={<User />} />
            <Route path="/user-group" element={<UserGroup />} />
            <Route path="/user-permission" element={<UserPermission />} />
            <Route path="/btc-app-main-category" element={<BTCAppMainCategory />} />
            <Route path="/btc-app-sub-category" element={<BTCAppSubCategory />} />
            <Route path="/product-grouping" element={<ProductGrouping />} />
            <Route path="/sync-products" element={<SyncProducts />} />
            <Route path="/product-collection" element={<ProductCollection />} />
            <Route path="/pack" element={<Pack />} />
            <Route path="/subscription" element={<Subscription />} />
            <Route path="/discount" element={<Discount />} />
            <Route path="/feed" element={<Feed />} />
            <Route path="/payment-method" element={<PaymentMethod />} />
            <Route path="/banner" element={<Banner />} />
            <Route path="/notification-list" element={<NotificationList />} />
            <Route path="/store" element={<Store />} />
            <Route path="/referral" element={<Referral />} />
            <Route path="/order" element={<Order />} />
            <Route path="/build-setup" element={<BuildSetup />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="/redeem-loyalty-points-on-product" element={<RedeemLoyaltyPointsOnProduct />} />
            <Route path="/points-on-wallet-topup" element={<PointsOnWalletTopup />} />
            <Route path="/earn-loyalty-points" element={<EarnLoyaltyPoints />} />
            <Route path="/subscription-summary" element={<SubscriptionSummary />} />
            <Route path="/total-payment-collection" element={<TotalPaymentCollection />} />
            <Route path="/order-summary" element={<OrderSummary />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/product" element={<Product/>} />
            <Route path="/add-category/:userId?" element={<AddCategory/>} />
            <Route path="/edit-sub-category" element={<EditSubCategory/>} />
            <Route path="/edit-product/:userId" element={<EditProduct/>} />
            <Route path="/edit-sync/:userId" element={<EditSync/>} />
            <Route path="/edit-collection/:productName" element={<EditCollection/>} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
