import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

const EditCollection = () => {
  const { productName } = useParams();
  const [product, setProduct] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchProduct = async () => {
      if (productName === 'new') {
        // Initialize an empty product
        setProduct({ name: '', type: '', status: 'Active' });
      } else {
        // Fetch existing product details
        const mockProduct = { name: productName, type: 'Manual', status: 'Active' };
        setProduct(mockProduct);
      }
    };
    fetchProduct();
  }, [productName]);

  const handleSave = async () => {
    try {
      console.log('Saving product:', product);
      await new Promise((resolve) => setTimeout(resolve, 1000));
      navigate('/product');
    } catch (error) {
      console.error('Error saving product:', error);
    }
  };

  const handleCancel = () => {
    navigate('/product');
  };

  if (!product) return <div>Loading...</div>;

  return (
    <div className="m-2 p-2 bg-slate-100 rounded-lg mt-14">
      <div className="bg-white rounded-lg p-8 m-2">
        <h3 className="text-2xl font-medium text-cyan-500 mb-4">
          {productName === 'new' ? 'Add New Product' : 'Edit Product'}
        </h3>
        <form>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">
              Name
            </label>
            <input
              type="text"
              id="name"
              value={product.name}
              onChange={(e) => setProduct({ ...product, name: e.target.value })}
              className="border border-gray-300 rounded w-full py-2 px-3 text-gray-700"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="type">
              Type
            </label>
            <input
              type="text"
              id="type"
              value={product.type}
              onChange={(e) => setProduct({ ...product, type: e.target.value })}
              className="border border-gray-300 rounded w-full py-2 px-3 text-gray-700"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="status">
              Status
            </label>
            <select
              id="status"
              value={product.status}
              onChange={(e) => setProduct({ ...product, status: e.target.value })}
              className="border border-gray-300 rounded w-full py-2 px-3 text-gray-700"
            >
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>
          <div className="flex justify-end mt-4">
            <button
              type="button"
              onClick={handleCancel}
              className="bg-gray-300 text-gray-800 hover:bg-gray-400 px-4 py-2 rounded-md mr-2"
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={handleSave}
              className="bg-cyan-500 text-white hover:bg-cyan-600 px-4 py-2 rounded-md"
            >
              Save
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditCollection;
