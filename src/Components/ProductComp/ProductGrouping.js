import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { FaCoffee, FaPlus, FaEdit, FaTrash } from "react-icons/fa";


const ProductGrouping = () => {
  const initialUsers = [
    { userId: 120, name: "Dhiraj", mainCategory: "Category A", subCategory: "Sub A1", sequence: 1, status: "Active" },
    { userId: 108, name: "Purav", mainCategory: "Category B", subCategory: "Sub B1", sequence: 2, status: "Active" },
    { userId: 149, name: "Anup Kumar", mainCategory: "Category A", subCategory: "Sub A2", sequence: 3, status: "Inactive" },
    { userId: 167, name: "Hritik Singh", mainCategory: "Category C", subCategory: "Sub C1", sequence: 4, status: "Active" },
    { userId: 135, name: "Shivani", mainCategory: "Category B", subCategory: "Sub B2", sequence: 5, status: "Active" },
    { userId: 175, name: "Saurabh", mainCategory: "Category C", subCategory: "Sub C2", sequence: 6, status: "Inactive" },
    { userId: 164, name: "Gaurav", mainCategory: "Category A", subCategory: "Sub A3", sequence: 7, status: "Active" },
    { userId: 172, name: "Nischal Patnaik", mainCategory: "Category B", subCategory: "Sub B3", sequence: 8, status: "Inactive" },
    { userId: 219, name: "Abhishek Kumar", mainCategory: "Category C", subCategory: "Sub C3", sequence: 9, status: "Active" },
  ];

  const [users, setUsers] = useState(initialUsers);
  const [editingUserId, setEditingUserId] = useState(null);
  const [editedUser, setEditedUser] = useState({
    name: "",
    sequence: "",
    status: "",
    mainCategory: "",
    subCategory: "",
  });

  // Search states
  const [searchName, setSearchName] = useState("");
  const [searchMainCategory, setSearchMainCategory] = useState("");
  const [searchSubCategory, setSearchSubCategory] = useState("");
  const [searchSequence, setSearchSequence] = useState("");
  const [searchStatus, setSearchStatus] = useState("All");

  const handleEdit = (userId) => {
    const userToEdit = users.find((user) => user.userId === userId);
    const productDetail={
      name: userToEdit?.name,
      sequence: userToEdit?.sequence.toString(),
      status: userToEdit?.status,
      mainCategory: userToEdit?.mainCategory,
      subCategory: userToEdit?.subCategory,

    }
    navigate('/edit-product/:userId', { state: { isEdit: true, productDetail:productDetail } });
    // const userToEdit = users.find((user) => user.userId === userId);
    // if (userToEdit) {
    //   setEditingUserId(userId);
    //   setEditedUser({
    //     name: userToEdit.name,
    //     sequence: userToEdit.sequence.toString(),
    //     status: userToEdit.status,
    //     mainCategory: userToEdit.mainCategory,
    //     subCategory: userToEdit.subCategory,
    //   });
    // }
  };

  const navigate = useNavigate(); // Initialize useNavigate

  const handleAddProduct = () => {
    navigate("/edit-product/:userId"); // Navigate to the EditProduct page
  };

  const handleSaveEdit = () => {
    if (editingUserId !== null) {
      setUsers(
        users.map((user) =>
          user.userId === editingUserId
            ? {
              ...user,
              name: editedUser.name,
              sequence: parseInt(editedUser.sequence),
              status: editedUser.status,
              mainCategory: editedUser.mainCategory,
              subCategory: editedUser.subCategory,
            }
            : user
        )
      );
      setEditingUserId(null);
      setEditedUser({
        name: "",
        sequence: "",
        status: "",
        mainCategory: "",
        subCategory: "",
      });
    }
  };

  const handleCancelEdit = () => {
    setEditingUserId(null);
    setEditedUser({
      name: "",
      sequence: "",
      status: "",
      mainCategory: "",
      subCategory: "",
    });
  };

  const handleDelete = (userId) => {
    setUsers(users.filter((user) => user.userId !== userId));
  };

  const handleInputChange = (event, field) => {
    const value = event.target.value;
    setEditedUser((prev) => ({
      ...prev,
      [field]: value,
    }));
  };

  // Filtered users based on search
  const filteredUsers = users.filter((user) => {
    const isStatusMatch = searchStatus === "All" || user.status.toLowerCase() === searchStatus.toLowerCase();
    return (
      user.name.toLowerCase().includes(searchName.toLowerCase()) &&
      user.mainCategory.toLowerCase().includes(searchMainCategory.toLowerCase()) &&
      user.subCategory.toLowerCase().includes(searchSubCategory.toLowerCase()) &&
      user.sequence.toString().includes(searchSequence) &&
      isStatusMatch
    );
  });

  return (
    <div className="flex min-h-screen">
      <div className="flex-1"> {/* Adjusted to account for Sidebar width */}
        <div className=" rounded-lg overflow-hidden m-4 p-1">
          <div className="overflow-x-auto">
            <div className="bg-white p-4 m-4 rounded-lg shadow-md">
              <div className="flex justify-between">
                <h1 className="text-2xl font-bold rounded-md p-4 bg-white text-cyan-400 flex items-center">
                  <FaPlus className="text-white m-4 bg-cyan-400 rounded-lg p-2 w-10 h-10 " />
                  Product Grouping
                </h1>
                <button
                  onClick={handleAddProduct}
                  className="bg-cyan-500 hover:bg-cyan-600 text-white rounded-lg p-2 h-10 w-22 mt-8"
                >
                  Add Product
                </button>
              </div>
              <div className="bg-slate-100 p-4 m-4 rounded-lg">
                <table className="min-w-full divide-y bg-white divide-gray-200">
                  <thead className="bg-white">
                    <tr className="bg-gray-200">
                      <th className="px-6 py-3 text-left text-xs bg-white font-medium text-gray-700 uppercase tracking-wider">
                        Icon
                      </th>
                      <th className="px-6 py-3 text-left text-xs bg-white font-medium text-gray-700 uppercase tracking-wider">
                        Name
                      </th>
                      <th className="px-6 py-3 text-left text-xs bg-white font-medium text-gray-700 uppercase tracking-wider">
                        Main Category
                      </th>
                      <th className="px-6 py-3 text-left bg-white text-xs font-medium text-gray-700 uppercase tracking-wider">
                        Sub Category
                      </th>
                      <th className="px-6 py-3 text-left text-xs bg-white font-medium text-gray-700 uppercase tracking-wider">
                        Sequence
                      </th>
                      <th className="px-6 py-3 text-left text-xs bg-white font-medium text-gray-700 uppercase tracking-wider">
                        Status
                      </th>
                      <th className="px-6 py-3 text-left text-xs bg-white font-medium text-gray-700 uppercase tracking-wider">
                        Action
                      </th>
                    </tr>
                    <tr>
                      <th className="px-6 py-3"></th>
                      <th className="px-6 py-3">
                        <input
                          type="text"
                          placeholder="Search Name"
                          value={searchName}
                          onChange={(e) => setSearchName(e.target.value)}
                          className="border rounded-md px-2 py-1 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-400"
                        />
                      </th>
                      <th className="px-6 py-3">
                        <input
                          type="text"
                          placeholder="Search Main Category"
                          value={searchMainCategory}
                          onChange={(e) => setSearchMainCategory(e.target.value)}
                          className="border rounded-md px-2 py-1 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-400"
                        />
                      </th>
                      <th className="px-6 py-3">
                        <input
                          type="text"
                          placeholder="Search Sub Category"
                          value={searchSubCategory}
                          onChange={(e) => setSearchSubCategory(e.target.value)}
                          className="border rounded-md px-2 py-1 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-400"
                        />
                      </th>
                      <th className="px-6 py-3">
                        <input
                          type="text"
                          placeholder="Search Sequence"
                          value={searchSequence}
                          onChange={(e) => setSearchSequence(e.target.value)}
                          className="border rounded-md px-2 py-1 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-400"
                        />
                      </th>
                      <th className="px-6 py-3">
                        <select
                          id="statusFilter"
                          value={searchStatus}
                          onChange={(e) => setSearchStatus(e.target.value)}
                          className="p-1 border w-full rounded-md text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-400"
                        >
                          <option value="All">All</option>
                          <option value="Active">Active</option>
                          <option value="Inactive">Inactive</option>
                        </select>
                      </th>
                      <th className="px-6 py-3"></th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {filteredUsers.map((user) => (
                      <tr key={user.userId}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <FaCoffee className="text-cyan-400" />
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">{user.name}</td>
                        <td className="px-6 py-4 whitespace-nowrap">{user.mainCategory}</td>
                        <td className="px-6 py-4 whitespace-nowrap">{user.subCategory}</td>
                        <td className="px-6 py-4 whitespace-nowrap">{user.sequence}</td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <span
                            className={`px-2 inline-flex text-sm leading-5 font-light rounded-full ${user.status === "Active"
                              ? "bg-cyan-500 px-4  py-2 text-white"
                              : "bg-gray-100 p-2 text-gray-800"
                              }`}
                          >
                            {user.status}
                          </span>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium">
                          <button
                            onClick={() => handleEdit(user.userId)}
                            className="text-gray-600 p-2 mr-2"
                          >
                            <FaEdit />
                          </button>
                          <button
                            onClick={() => handleDelete(user.userId)}
                            className="text-red-600 hover:text-red-900"
                          >
                            <FaTrash />
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          {editingUserId !== null && (
            <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
              <div className="bg-white p-8 rounded-md shadow-md w-full max-w-md">
                <h2 className="text-2xl font-medium text-cyan-500 mb-4">Edit User</h2>
                <div className="mb-4">
                  <label htmlFor="editName" className="block text-sm font-medium text-gray-700 mb-1">
                    Name
                  </label>
                  <input
                    type="text"
                    id="editName"
                    value={editedUser.name}
                    onChange={(e) => handleInputChange(e, "name")}
                    className="border rounded-md px-3 py-2 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-500"
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="editMainCategory" className="block text-sm font-medium text-gray-700 mb-1">
                    Main Category
                  </label>
                  <input
                    type="text"
                    id="editMainCategory"
                    value={editedUser.mainCategory}
                    onChange={(e) => handleInputChange(e, "mainCategory")}
                    className="border rounded-md px-3 py-2 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-500"
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="editSubCategory" className="block text-sm font-medium text-gray-700 mb-1">
                    Sub Category
                  </label>
                  <input
                    type="text"
                    id="editSubCategory"
                    value={editedUser.subCategory}
                    onChange={(e) => handleInputChange(e, "subCategory")}
                    className="border rounded-md px-3 py-2 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-500"
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="editSequence" className="block text-sm font-medium text-gray-700 mb-1">
                    Sequence
                  </label>
                  <input
                    type="number"
                    id="editSequence"
                    value={editedUser.sequence}
                    onChange={(e) => handleInputChange(e, "sequence")}
                    className="border rounded-md px-3 py-2 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-500"
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="editStatus" className="block text-sm font-medium text-gray-700 mb-1">
                    Status
                  </label>
                  <select
                    id="editStatus"
                    value={editedUser.status}
                    onChange={(e) => handleInputChange(e, "status")}
                    className="border rounded-md px-3 py-2 w-full text-gray-500 font-light focus:outline-none focus:ring-2 focus:ring-cyan-500"
                  >
                    <option value="">Select Status</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                  </select>
                </div>
                <div className="flex justify-end">
                  <button
                    onClick={handleCancelEdit}
                    className="bg-gray-300 text-gray-700 px-4 py-2 rounded-md mr-2"
                  >
                    Cancel
                  </button>
                  <button
                    onClick={handleSaveEdit}
                    className="bg-cyan-500 text-white px-4 py-2 rounded-md"
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ProductGrouping;
