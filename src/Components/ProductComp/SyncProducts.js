import React, { useState } from 'react';
import { FaEdit, FaPlus, FaTrash } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';

const SyncProducts = () => {
  const initialUsers = [
    { userId: 120, name: "Dhiraj", categoryName: "Electronics", price: "$100", ristaShopify: "Rista", grouped: false, status: "Active" },
    { userId: 108, name: "Purav", categoryName: "Fashion", price: "$200", ristaShopify: "Shopify", grouped: true, status: "Inactive" },
    { userId: 149, name: "Anup Kumar", categoryName: "Groceries", price: "$50", ristaShopify: "Rista", grouped: false, status: "Active" },
    { userId: 167, name: "Hritik Singh", categoryName: "Furniture", price: "$300", ristaShopify: "Shopify", grouped: false, status: "Active" },
    { userId: 135, name: "Shivani", categoryName: "Books", price: "$20", ristaShopify: "Rista", grouped: true, status: "Inactive" },
    { userId: 175, name: "Saurabh", categoryName: "Music", price: "$150", ristaShopify: "Shopify", grouped: false, status: "Active" },
    { userId: 164, name: "Gaurav", categoryName: "Toys", price: "$30", ristaShopify: "Rista", grouped: false, status: "Inactive" },
    { userId: 172, name: "Nischal Patnaik", categoryName: "Sports", price: "$80", ristaShopify: "Shopify", grouped: true, status: "Active" },
    { userId: 219, name: "Abhishek Kumar", categoryName: "Health", price: "$60", ristaShopify: "Rista", grouped: false, status: "Inactive" },
  ];

  const [users, setUsers] = useState(initialUsers);
  const [searchName, setSearchName] = useState('');
  const [searchCategory, setSearchCategory] = useState('');
  const [searchPrice, setSearchPrice] = useState('');
  const [searchStatus, setSearchStatus] = useState('All');

  const navigate = useNavigate();

  const handleEdit = (userId) => {
    navigate(`/edit-sync/${userId}`);
  };

  const handleAddProduct = () => {
    navigate('/edit-sync/new');
  };

  const handleDelete = (userId) => {
    setUsers(users.filter((user) => user.userId !== userId));
  };

  const filteredUsers = users.filter((user) =>
    user.name.toLowerCase().includes(searchName.toLowerCase()) &&
    user.categoryName.toLowerCase().includes(searchCategory.toLowerCase()) &&
    user.price.toLowerCase().includes(searchPrice.toLowerCase()) &&
    (searchStatus === 'All' || user.status === searchStatus)
  );

  return (
    <div className="flex flex-col min-h-screen p-4 md:p-6 lg:p-8">
      <div className="overflow-x-auto">
        <div className="bg-white rounded-lg shadow-md">
          <div className="flex flex-col lg:flex-row justify-between items-center p-4">
            <h1 className="text-2xl font-bold rounded-md p-4 bg-white text-cyan-400 flex items-center">
              <FaPlus className="text-white mr-6 bg-cyan-400 rounded-lg p-2 w-10 h-10" />
              Sync Products
            </h1>
            <button
              onClick={handleAddProduct}
              className="bg-cyan-500 hover:bg-cyan-600 text-white rounded-lg px-4 py-2 mt-4 lg:mt-0"
            >
              Add Product
            </button>
          </div>
          <div className="bg-slate-100 p-4 shadow-md rounded-lg">
            <table className="min-w-full divide-y bg-white divide-gray-200">
              <thead className="bg-gray-100">
                <tr>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Name</th>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Category Name</th>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Price</th>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Rista/Shopify</th>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Status</th>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Grouped</th>
                  <th className="px-4 py-2 text-left text-sm font-medium text-gray-700 uppercase tracking-wider">Action</th>
                </tr>
                <tr>
                  <th className="px-4 py-2">
                    <input
                      type="text"
                      placeholder="Search ..."
                      className="border rounded-md px-4 py-3 w-full font-light focus:ring-2 focus:ring-cyan-400"
                      value={searchName}
                      onChange={(event) => setSearchName(event.target.value)}
                    />
                  </th>
                  <th className="px-4 py-2">
                    <input
                      type="text"
                      placeholder="Search ..."
                      className="border rounded-md px-2 py-1 w-full font-light focus:ring-2 focus:ring-cyan-400"
                      value={searchCategory}
                      onChange={(event) => setSearchCategory(event.target.value)}
                    />
                  </th>
                  <th className="px-4 py-2">
                    <input
                      type="text"
                      placeholder="Search ..."
                      className="border rounded-md px-2 py-1 w-full font-light focus:ring-2 focus:ring-cyan-400"
                      value={searchPrice}
                      onChange={(event) => setSearchPrice(event.target.value)}
                    />
                  </th>
                  <th className="px-4 py-2"></th>
                  <th className="px-4 py-2">
                    <select
                      value={searchStatus}
                      onChange={(event) => setSearchStatus(event.target.value)}
                      className="border rounded-md px-2 py-1 w-full font-light focus:outline-cyan-500"
                    >
                      <option value="All">All</option>
                      <option value="Active">Active</option>
                      <option value="Inactive">Inactive</option>
                    </select>
                  </th>
                  <th className="px-4 py-2"></th>
                  <th className="px-4 py-2"></th>
                </tr>
              </thead>
              <tbody className="divide-y bg-white divide-gray-200">
                {filteredUsers.map((user) => (
                  <tr key={user.userId} className="hover:bg-gray-100">
                    <td className="px-4 py-6 text-sm text-gray-500">{user.name}</td>
                    <td className="px-4 py-6 text-sm text-gray-500">{user.categoryName}</td>
                    <td className="px-4 py-6 text-sm text-gray-500">{user.price}</td>
                    <td className="px-4 py-6 text-sm text-gray-500">{user.ristaShopify}</td>
                    <td className="px-4 py-6 text-sm text-gray-500">
                      <span
                        className={`px-3 py-2 text-sm font-medium rounded-full ${user.status === "Active" ? "bg-cyan-500 text-white" : "bg-gray-200 text-gray-700"}`}
                      >
                        {user.status}
                      </span>
                    </td>
                    <td className="px-4 py-6 text-sm text-gray-500">{user.grouped ? "Yes" : "No"}</td>
                    <td className="px-4 py-6 text-sm text-gray-500 flex gap-2">
                      <FaEdit
                        onClick={() => handleEdit(user.userId)}
                        className="text-cyan-500 cursor-pointer hover:text-cyan-700"
                      />
                      <FaTrash
                        onClick={() => handleDelete(user.userId)}
                        className="text-red-500 cursor-pointer hover:text-red-700"
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SyncProducts;
