import React, { useState } from 'react';
import { FaEdit, FaTrash, FaPlus } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';

const ProductCollection = () => {
  const [products, setProducts] = useState([
    { name: 'All Beverage', type: 'Manual', status: 'Active' },
    { name: 'All Coffee Beans', type: 'Manual', status: 'Inactive' },
    { name: 'All Food Items', type: 'Manual', status: 'Active' },
    { name: 'Coffee & Coffee Products', type: 'Automated', status: 'Active' },
    { name: 'Only 250g Coffee Beans', type: 'Manual', status: 'Inactive' },
  ]);

  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage, setRecordsPerPage] = useState(10);

  const navigate = useNavigate();

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentProducts = products.slice(indexOfFirstRecord, indexOfLastRecord);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const handleRecordsPerPageChange = (event) => {
    setRecordsPerPage(parseInt(event.target.value, 10));
    setCurrentPage(1);
  };

  const handleEditProduct = (productName) => {
    navigate(`/edit-collection/${productName}`);
  };

  const handleDeleteProduct = (productName) => {
    setProducts(products.filter((product) => product.name !== productName));
  };

  const handleAddProduct = () => {
    navigate('/edit-collection/new');
  };

  return (
    <div className="flex min-h-screen">
      <div className="flex-1 p-4">
        <div className="bg-white rounded-lg shadow-md overflow-hidden">
          <div className="flex justify-between items-center p-4 bg-white border-b border-gray-200">
            <h1 className="text-2xl font-bold text-cyan-400 flex items-center">
              <FaPlus className="text-white bg-cyan-500 rounded-lg p-2 w-10 h-10" />
              <span className="ml-2">Product Collection</span>
            </h1>
            <button
              onClick={handleAddProduct}
              className="bg-cyan-500 hover:bg-cyan-600 text-white rounded-lg px-4 py-2"
            >
              Add Product
            </button>
          </div>
          <div className="p-4">
            <div className="overflow-x-auto">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-100">
                  <tr>
                    <th className="px-6 py-3 text-left text-sm font-medium text-gray-700 uppercase">Name</th>
                    <th className="px-6 py-3 text-left text-sm font-medium text-gray-700 uppercase">Type</th>
                    <th className="px-6 py-3 text-left text-sm font-medium text-gray-700 uppercase">Status</th>
                    <th className="px-6 py-3 text-left text-sm font-medium text-gray-700 uppercase">Actions</th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {currentProducts.map((product) => (
                    <tr key={product.name} className="hover:bg-gray-100">
                      <td className="px-6 py-4 text-sm font-medium text-gray-900">{product.name}</td>
                      <td className="px-6 py-4 text-sm text-gray-500">{product.type}</td>
                      <td className="px-6 py-4 text-sm text-gray-500">
                        <span
                          className={`inline-flex items-center px-3 py-2 rounded-full text-sm font-medium ${
                            product.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-300 text-gray-800'
                          }`}
                        >
                          {product.status}
                        </span>
                      </td>
                      <td className="px-6 py-4 text-sm font-medium">
                        <button
                          onClick={() => handleEditProduct(product.name)}
                          className="text-cyan-500 hover:text-cyan-700"
                        >
                          <FaEdit />
                        </button>
                        <button
                          onClick={() => handleDeleteProduct(product.name)}
                          className="text-red-500 hover:text-red-700 ml-4"
                        >
                          <FaTrash />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="flex justify-between items-center mt-4">
              <div className="flex items-center">
                <label className="mr-2 text-sm text-gray-600">Records per page:</label>
                <select
                  value={recordsPerPage}
                  onChange={handleRecordsPerPageChange}
                  className="border border-gray-300 rounded-md py-2 px-3 text-gray-700"
                >
                  {[10, 25, 50, 100].map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <span className="text-sm text-gray-600">
                  Page {currentPage} of {Math.ceil(products.length / recordsPerPage)}
                </span>
              </div>
              <div className="flex">
                <button
                  onClick={() => paginate(currentPage - 1)}
                  disabled={currentPage === 1}
                  className="bg-cyan-500 text-white py-2 px-4 rounded-md hover:bg-cyan-700 disabled:bg-gray-300"
                >
                  Previous
                </button>
                <button
                  onClick={() => paginate(currentPage + 1)}
                  disabled={currentPage === Math.ceil(products.length / recordsPerPage)}
                  className="bg-cyan-500 text-white py-2 px-4 rounded-md hover:bg-cyan-700 disabled:bg-gray-300 ml-2"
                >
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductCollection;
