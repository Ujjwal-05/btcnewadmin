// src/components/EditSubcategory.js

import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

const EditSubcategory = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { state } = location;

  const [name, setName] = useState(state.name || "");
  const [sequence, setSequence] = useState(state.sequence || "");
  const [mainCategory, setMainCategory] = useState(state.mainCategory || "");
  const [status, setStatus] = useState(state.status || "");

  const handleSave = () => {
    // Here you should handle saving the data, for example, by calling an API or updating the state in a parent component.
    console.log("Saved Data:", { name, sequence, mainCategory, status });
    navigate('/product'); // Navigate back to the previous page or a confirmation page
  };

  const handleCancel = () => {
    navigate('/product'); 
  };

  return (
    <div className="flex flex-col min-h-screen">
      <div className="flex-1 p-4 sm:p-6 lg:p-8">
        <div className="bg-gray-100 p-4 rounded-lg shadow-md">
          <div className="bg-white p-4 rounded-lg shadow-md">
          <h1 className="text-3xl font-light text-cyan-500 mb-4">Edit Subcategory</h1>
          <div className="flex flex-col space-y-6">
            <div className="flex flex-col">
              <label className="text-gray-700 m-2">Name</label>
              <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                className="p-2 border focus:outline-cyan-500 rounded-md"
              />
            </div>
            <div className="flex flex-col">
              <label className="text-gray-700 m-2">Sequence</label>
              <input
                type="number"
                value={sequence}
                onChange={(e) => setSequence(e.target.value)}
                className="p-2 border focus:outline-cyan-500 rounded-md"
              />
            </div>
            <div className="flex flex-col">
              <label className="text-gray-700 m-2">Main Category</label>
              <input
                type="text"
                value={mainCategory}
                onChange={(e) => setMainCategory(e.target.value)}
                className="p-2 border focus:outline-cyan-500 rounded-md"
              />
            </div>
            <div className="flex flex-col">
              <label className="text-gray-700 focus:outline-cyan-500 m-2 ">Status</label>
              <select
                value={status}
                onChange={(e) => setStatus(e.target.value)}
                className="p-2 border focus:outline-cyan-500 rounded-md"
              >
                <option value="Active">Active</option>
                <option value="Inactive">Inactive</option>
              </select>
            </div>
            <div className="flex justify-end space-x-4">
            <button
              onClick={handleSave}
              className="bg-cyan-500 hover:bg-cyan-600 w-16 text-white rounded-lg p-2"
            >
              Save
            </button>
            <button
              onClick={handleCancel}
              className="bg-gray-500 hover:bg-gray-600 w-16  text-white rounded-lg p-2"
            >
              Cancel
            </button>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditSubcategory;
