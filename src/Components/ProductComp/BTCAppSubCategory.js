// src/components/BTCAppSubCategory.js

import React, { useState } from "react";
import { FaPlus } from "react-icons/fa";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCoffee, faEdit } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

const BTCAppSubCategory = () => {
  const initialUsers = [
    { userId: 120, name: "Dhiraj", mainCategory: "Category A", sequence: 1, status: "Active" },
    { userId: 108, name: "Purav", mainCategory: "Category B", sequence: 2, status: "Active" },
    { userId: 149, name: "Anup Kumar", mainCategory: "Category A", sequence: 3, status: "Inactive" },
    { userId: 167, name: "Hritik Singh", mainCategory: "Category C", sequence: 4, status: "Active" },
    { userId: 135, name: "Shivani", mainCategory: "Category B", sequence: 5, status: "Active" },
    { userId: 175, name: "Saurabh", mainCategory: "Category C", sequence: 6, status: "Inactive" },
    { userId: 164, name: "Gaurav", mainCategory: "Category A", sequence: 7, status: "Active" },
    { userId: 172, name: "Nischal Patnaik", mainCategory: "Category B", sequence: 8, status: "Inactive" },
    { userId: 219, name: "Abhishek Kumar", mainCategory: "Category C", sequence: 9, status: "Active" },
  ];

  const [users, setUsers] = useState(initialUsers);
  const [statusFilter, setStatusFilter] = useState("All");
  const [searchNameTerm, setSearchNameTerm] = useState("");
  const [searchCategoryTerm, setSearchCategoryTerm] = useState("");
  const [searchSequenceTerm, setSearchSequenceTerm] = useState("");

  const filteredUsers = users.filter((user) => {
    const statusMatches = statusFilter === "All" || user.status === statusFilter;
    const nameMatches = user.name.toLowerCase().includes(searchNameTerm.toLowerCase());
    const categoryMatches = user.mainCategory.toLowerCase().includes(searchCategoryTerm.toLowerCase());
    const sequenceMatches =
      searchSequenceTerm === "" || user.sequence.toString().includes(searchSequenceTerm);

    return statusMatches && nameMatches && categoryMatches && sequenceMatches;
  });

  const navigate = useNavigate();

  const handleStatusToggle = (userId) => {
    setUsers((prevUsers) =>
      prevUsers.map((user) =>
        user.userId === userId
          ? { ...user, status: user.status === "Active" ? "Inactive" : "Active" }
          : user
      )
    );
  };

  const handleEdit = (user) => {
    navigate('/edit-sub-category', { state: { ...user } });
  };

  return (
    <div className="flex flex-col min-h-screen">
      <div className="flex-1 p-4 sm:p-6 lg:p-8">
        <div className="bg-white p-4 rounded-lg shadow-md">
          <div className="flex flex-col lg:flex-row justify-between items-center lg:items-start mb-4">
            <h1 className="text-2xl font-light rounded-md p-4 bg-white text-cyan-400 flex items-center">
              <FaPlus className="text-white m-2 bg-cyan-400 rounded-lg p-2 w-10 h-10" />
              BTC App Sub Category
            </h1>
            <button
              className="bg-cyan-500 hover:bg-cyan-600 text-white rounded-lg h-10 w-full lg:w-28 p-2 mt-6"
            >
              Add Product
            </button>
          </div>
          <div className="bg-slate-100 p-4 rounded-lg overflow-x-auto">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-200">
                <tr>
                  <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Icon
                  </th>
                  <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Name
                  </th>
                  <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Sequence
                  </th>
                  <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Status
                  </th>
                  <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Main Category
                  </th>
                  <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Action
                  </th>
                </tr>
                <tr className="bg-white">
                  <td className="px-4 py-3"></td>
                  <td className="px-4 py-3">
                    <input
                      type="text"
                      placeholder="Search by Name"
                      value={searchNameTerm}
                      onChange={(e) => setSearchNameTerm(e.target.value)}
                      className="p-2 text-gray-600 shadow-sm border-gray-300 rounded-md w-full"
                    />
                  </td>
                  <td className="px-4 py-3">
                    <input
                      type="text"
                      placeholder="Search by Sequence"
                      value={searchSequenceTerm}
                      onChange={(e) => setSearchSequenceTerm(e.target.value)}
                      className="p-2 text-gray-600 shadow-sm border-gray-300 rounded-md w-full"
                    />
                  </td>
                  <td className="px-4 py-3">
                    <select
                      id="statusFilter"
                      value={statusFilter}
                      onChange={(e) => setStatusFilter(e.target.value)}
                      className="p-2 text-gray-600 shadow-sm border-gray-300 rounded-md w-full"
                    >
                      <option value="All">All</option>
                      <option value="Active">Active</option>
                      <option value="Inactive">Inactive</option>
                    </select>
                  </td>
                  <td className="px-4 py-3">
                    <input
                      type="text"
                      placeholder="Search by Category"
                      value={searchCategoryTerm}
                      onChange={(e) => setSearchCategoryTerm(e.target.value)}
                      className="p-2 text-gray-600 shadow-sm border-gray-300 rounded-md w-full"
                    />
                  </td>
                  <td className="px-4 py-3"></td>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {filteredUsers.map((user) => (
                  <tr key={user.userId}>
                    <td className="px-4 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      <FontAwesomeIcon icon={faCoffee} />
                    </td>
                    <td className="px-4 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      {user.name}
                    </td>
                    <td className="px-4 py-4 whitespace-nowrap text-sm text-gray-500">
                      {user.sequence}
                    </td>
                    <td className="px-4 py-4 whitespace-nowrap text-sm text-gray-500">
                      <button
                        onClick={() => handleStatusToggle(user.userId)}
                        className={`px-4 py-2 rounded-full ${
                          user.status === "Active"
                            ? "bg-cyan-500 text-white"
                            : "bg-gray-300 text-gray-700"
                        }`}
                      >
                        {user.status}
                      </button>
                    </td>
                    <td className="px-4 py-4 whitespace-nowrap text-sm text-gray-500">
                      {user.mainCategory}
                    </td>
                    <td className="px-4 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      <button
                        onClick={() => handleEdit(user)}
                        className="text-cyan-500 hover:text-cyan-700"
                      >
                        <FontAwesomeIcon icon={faEdit} />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BTCAppSubCategory;
