import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

function EditProduct() {
    const location = useLocation();
    const { isEdit, productDetail } = location.state || {};
    const [categoryName, setCategoryName] = useState(isEdit ? productDetail.name : '');
    const [description, setDescription] = useState(isEdit ? productDetail.description : '');
    const [subcategory, setSubcategory] = useState(isEdit ? productDetail.subcategory : '');
    const [status, setStatus] = useState(isEdit ? productDetail.status : 'active');
    const [rewardCategory, setRewardCategory] = useState(isEdit ? productDetail.rewardCategory : 'none');
    const [dropdownVisible, setDropdownVisible] = useState(false);
    const [subcategoryImage, setSubcategoryImage] = useState(null);
    const [imageError, setImageError] = useState('');

    const navigate = useNavigate();

    const handleCategoryNameChange = (event) => {
        setCategoryName(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    };

    const handleSubcategoryChange = (selectedSubcategory) => {
        setSubcategory(selectedSubcategory);
        setDropdownVisible(false);
    };

    const handleStatusChange = (event) => {
        setStatus(event.target.value);
    };

    const handleRewardCategoryChange = (event) => {
        setRewardCategory(event.target.value);
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        const validFormats = ['image/jpeg', 'image/png', 'image/jpg'];
        const maxFileSize = 2 * 1024 * 1024; // 2MB
        const img = new Image();

        if (file) {
            if (!validFormats.includes(file.type)) {
                setImageError('Invalid file format. Only jpg, jpeg, and png are allowed.');
                setSubcategoryImage(null);
                return;
            }

            if (file.size > maxFileSize) {
                setImageError('File size exceeds 2MB.');
                setSubcategoryImage(null);
                return;
            }

            img.src = URL.createObjectURL(file);
            img.onload = () => {
                if (img.width > 1440 || img.height > 1440) {
                    setImageError('Image resolution exceeds 1440x1440.');
                    setSubcategoryImage(null);
                } else {
                    setImageError('');
                    setSubcategoryImage(file);
                }
            };
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        if (!subcategoryImage) {
            setImageError('Please upload a valid subcategory image.');
            return;
        }

        const newCategory = {
            categoryName,
            description,
            subcategory,
            status,
            rewardCategory,
            subcategoryImage
        };

        try {
            const formData = new FormData();
            formData.append('categoryName', categoryName);
            formData.append('description', description);
            formData.append('subcategory', subcategory);
            formData.append('status', status);
            formData.append('rewardCategory', rewardCategory);
            formData.append('subcategoryImage', subcategoryImage);

            const response = await fetch('https://your-api-endpoint.com/categories', {
                method: 'POST',
                body: formData,
            });

            if (!response.ok) {
                throw new Error('Failed to add category');
            }

            setCategoryName('');
            setDescription('');
            setSubcategory('');
            setStatus('active');
            setRewardCategory('none');
            setSubcategoryImage(null);

            navigate(0); // Refresh the page

        } catch (error) {
            console.error('Error adding category:', error);
        }
    };

    const handleCancel = () => {
        navigate(-1);
    };

    const handleInputFocus = () => {
        setDropdownVisible(true);
    };

    const handleInputBlur = () => {
        setTimeout(() => {
            setDropdownVisible(false);
        }, 200); // Delay to allow click event on dropdown items
    };

    const subcategoryOptions = ['Sub-category 1', 'Sub-category 2', 'Sub-category 3', 'Sub-category 4'];

    return (
        <div className="container mx-auto p-6 max-w-5xl bg-white rounded-lg">
            <h1 className="text-3xl font-medium mb-6 text-cyan-600">Edit Product</h1>
            <form onSubmit={handleSubmit} className="bg-white shadow-lg rounded-lg p-8 space-y-6">
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="category-name">
                        Product Name
                    </label>
                    <input
                        className="block w-full p-2 border-gray-300 rounded-md shadow-sm focus:outline-cyan-500 sm:text-sm"
                        id="category-name"
                        type="text"
                        placeholder="Name of the Category"
                        value={categoryName}
                        onChange={handleCategoryNameChange}
                        required
                    />
                </div>
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="description">
                        Description
                    </label>
                    <textarea
                        className="block w-full p-1 border-gray-300 rounded-md shadow-sm focus:outline-cyan-500 sm:text-sm"
                        id="description"
                        placeholder="Description of Category"
                        value={description}
                        onChange={handleDescriptionChange}
                        required
                    />
                </div>
                <div className="mb-6 relative">
                    <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="subcategory">
                        Sub-Category
                    </label>
                    <input
                        className="block w-full p-2 border-gray-300 cursor-pointer rounded-md shadow-sm focus:outline-cyan-500 sm:text-sm"
                        id="subcategory"
                        type="text"
                        placeholder="Select Sub-category"
                        onFocus={handleInputFocus}
                        onBlur={handleInputBlur}
                        value={subcategory}
                        readOnly
                    />
                    {dropdownVisible && (
                        <ul className="absolute z-10 w-full bg-white border border-gray-300 rounded-md shadow-lg mt-1">
                            {subcategoryOptions.map((option) => (
                                <li
                                    key={option}
                                    className={`p-2 cursor-pointer ${subcategory === option ? 'bg-cyan-100' : 'hover:bg-gray-100'}`}
                                    onMouseDown={() => handleSubcategoryChange(option)}
                                >
                                    {option}
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
                <div className="mb-6">
                    <fieldset className="flex flex-col space-y-4">
                        <legend className="text-gray-700 text-sm font-medium mb-2">Reward Category</legend>
                        <div className="flex items-center space-x-6">
                            <label className="flex items-center">
                                <input
                                    type="radio"
                                    id="reward-yes"
                                    name="reward-category"
                                    value="reward"
                                    checked={rewardCategory === 'reward'}
                                    onChange={handleRewardCategoryChange}
                                    className="form-radio text-cyan-500"
                                />
                                <span className="ml-2 text-gray-700">Yes</span>
                            </label>
                            <label className="flex items-center">
                                <input
                                    type="radio"
                                    id="reward-no"
                                    name="reward-category"
                                    value="no-reward"
                                    checked={rewardCategory === 'no-reward'}
                                    onChange={handleRewardCategoryChange}
                                    className="form-radio text-cyan-500"
                                />
                                <span className="ml-2 text-gray-700">No</span>
                            </label>
                        </div>
                    </fieldset>
                </div>
                <div className="mb-6">
                    <fieldset className="flex flex-col space-y-4">
                        <legend className="text-gray-700 text-sm font-medium mb-2">Type</legend>
                        <div className="flex items-center space-x-6">
                            <label className="flex items-center">
                                <input
                                    type="radio"
                                    id="type-veg"
                                    name="type"
                                    value="veg"
                                    checked={rewardCategory === 'veg'}
                                    onChange={handleRewardCategoryChange}
                                    className="form-radio text-cyan-500"
                                />
                                <span className="ml-2 text-gray-700">Veg</span>
                            </label>
                            <label className="flex items-center">
                                <input
                                    type="radio"
                                    id="type-non-veg"
                                    name="type"
                                    value="non-veg"
                                    checked={rewardCategory === 'non-veg'}
                                    onChange={handleRewardCategoryChange}
                                    className="form-radio text-cyan-500"
                                />
                                <span className="ml-2 text-gray-700">Non-Veg</span>
                            </label>
                        </div>
                    </fieldset>
                </div>
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="subcategory-image">
                        Subcategory Image
                    </label>
                    <input
                        id="subcategory-image"
                        type="file"
                        accept="image/jpeg, image/png, image/jpg"
                        onChange={handleImageChange}
                        className="block w-full text-gray-700"
                    />
                    {imageError && <p className="text-red-500 text-sm mt-2">{imageError}</p>}
                </div>
                <div className="flex space-x-4">
                    <button
                        type="submit"
                        className="px-4 py-2 bg-cyan-500 text-white font-semibold rounded-md hover:bg-cyan-600 focus:outline-none focus:ring-2 focus:ring-cyan-500"
                    >
                        {isEdit ? 'Update' : 'Add'} Product
                    </button>
                    <button
                        type="button"
                        onClick={handleCancel}
                        className="px-4 py-2 bg-gray-300 text-gray-700 font-semibold rounded-md hover:bg-gray-400 focus:outline-none"
                    >
                        Cancel
                    </button>
                </div>
            </form>
        </div>
    );
}

export default EditProduct;
