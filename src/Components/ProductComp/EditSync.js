import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

const EditSync = () => {
  const { userId } = useParams();
  const navigate = useNavigate();
  
  const [product, setProduct] = useState({
    name: '',
    categoryName: '',
    price: '',
    ristaShopify: 'Rista', // default value
    status: 'Active', // default value
    grouped: false // default value
  });
  const [isNew, setIsNew] = useState(false);

  useEffect(() => {
    if (userId === 'new') {
      // Initialize state for new product
      setIsNew(true);
    } else {
      // Fetch product details from API or state using userId
      // For example purposes, we'll use a static product
      // Replace this with actual fetch logic
      const fetchedProduct = {
        name: 'Sample Product',
        categoryName: 'Sample Category',
        price: '$100',
        ristaShopify: 'Rista',
        status: 'Active',
        grouped: false
      };
      setProduct(fetchedProduct);
    }
  }, [userId]);

  const handleChange = (event) => {
    const { name, value, type, checked } = event.target;
    setProduct((prevProduct) => ({
      ...prevProduct,
      [name]: type === 'checkbox' ? checked : value
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (isNew) {
      // Handle new product creation
      console.log('Creating new product:', product);
    } else {
      // Handle existing product update
      console.log('Updating product:', userId, product);
    }
    // Navigate back or to another page
    navigate('/product');
  };

  return (
    <div className="p-4 bg-slate-100 m-4 rounded-lg ">
    <div className="flex flex-col">
      <div className="bg-white rounded-lg shadow-md p-4">
        <h1 className="text-2xl font-light text-cyan-500 mb-4">{isNew ? 'Add New Product' : 'Edit Product'}</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">Name</label>
            <input
              type="text"
              name="name"
              value={product.name}
              onChange={handleChange}
              className="border rounded-md px-3 py-2 w-full"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">Category Name</label>
            <input
              type="text"
              name="categoryName"
              value={product.categoryName}
              onChange={handleChange}
              className="border rounded-md px-3 py-2 w-full"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">Price</label>
            <input
              type="text"
              name="price"
              value={product.price}
              onChange={handleChange}
              className="border rounded-md px-3 py-2 w-full"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">Rista/Shopify</label>
            <select
              name="ristaShopify"
              value={product.ristaShopify}
              onChange={handleChange}
              className="border rounded-md px-3 py-2 w-full"
            >
              <option value="Rista">Rista</option>
              <option value="Shopify">Shopify</option>
            </select>
          </div>
          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">Status</label>
            <select
              name="status"
              value={product.status}
              onChange={handleChange}
              className="border rounded-md px-3 py-2 w-full"
            >
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>
          <div className="mb-4">
            <label className="block text-sm font-medium text-gray-700">Grouped</label>
            <input
              type="checkbox"
              name="grouped"
              checked={product.grouped}
              onChange={handleChange}
              className="mr-2"
            />
            Grouped
          </div>
          <button
            type="submit"
            className="bg-cyan-500 hover:bg-cyan-600 text-white rounded-lg px-4 py-2"
          >
            {isNew ? 'Add Product' : 'Save Changes'}
          </button>
        </form>
      </div>
    </div>
    </div>
  );
};

export default EditSync;
