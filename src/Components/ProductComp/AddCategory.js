import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

const AddCategory = () => {
  const [categoryName, setCategoryName] = useState('');
  const [description, setDescription] = useState('');
  const [subcategory, setSubcategory] = useState('');
  const [categoryStatus, setCategoryStatus] = useState('active');
  const [rewardCategory, setRewardCategory] = useState('none');
  const [isEditing, setIsEditing] = useState(false);

  const navigate = useNavigate();
  const location = useLocation();
  const { state } = location;

  useEffect(() => {
    if (state) {
      setCategoryName(state.categoryName || '');
      setDescription(state.description || '');
      setSubcategory(state.subcategory || '');
      setCategoryStatus(state.status || 'active');
      setRewardCategory(state.rewardCategory || 'none');
      setIsEditing(!!state.categoryName);
    }
  }, [state]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const categoryData = {
      categoryName,
      description,
      subcategory,
      status: categoryStatus,
      rewardCategory
    };

    try {
      const response = await fetch('https://your-api-endpoint.com/categories', {
        method: isEditing ? 'PUT' : 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(categoryData)
      });

      if (!response.ok) {
        throw new Error(`Failed to ${isEditing ? 'edit' : 'add'} category`);
      }

      // Clear form and navigate back
      setCategoryName('');
      setDescription('');
      setSubcategory('');
      setCategoryStatus('active');
      setRewardCategory('none');
      setIsEditing(false);

      navigate('/categories');
    } catch (error) {
      console.error(`Error ${isEditing ? 'editing' : 'adding'} category:`, error);
    }
  };

  const handleCancel = () => {
    navigate(-1);
  };

  return (
    <div className="container mx-auto p-6 max-w-5xl bg-white rounded-lg">
      <h1 className="text-3xl font-extrabold mb-6 text-cyan-600">
        {isEditing ? 'Edit Category' : 'Add a Category'}
      </h1>
      <form onSubmit={handleSubmit} className="bg-white shadow-lg rounded-lg p-8 space-y-6">
        <div className="mb-6">
          <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="category-name">
            Category Name
          </label>
          <input
            className="block w-full p-2 border-gray-300 rounded-md shadow-sm focus:outline-cyan-500 sm:text-sm"
            id="category-name"
            type="text"
            placeholder="Name of the Category"
            value={categoryName}
            onChange={(e) => setCategoryName(e.target.value)}
            required
          />
        </div>
        <div className="mb-6">
          <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="description">
            Description
          </label>
          <textarea
            className="block w-full p-1 border-gray-300 rounded-md shadow-sm focus:outline-cyan-500 sm:text-sm"
            id="description"
            placeholder="Description of Category"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </div>
        <div className="mb-6">
          <label className="block text-gray-700 text-sm font-medium mb-2" htmlFor="subcategory">
            Sub-category
          </label>
          <input
            className="block w-full p-2 border-gray-300 cursor-pointer rounded-md shadow-sm focus:outline-cyan-500 sm:text-sm"
            id="subcategory"
            type="text"
            placeholder="Select Sub-category"
            value={subcategory}
            onChange={(e) => setSubcategory(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <fieldset className="flex flex-col space-y-4">
            <legend className="text-gray-700 text-sm font-medium mb-2">Status</legend>
            <div className="flex items-center space-x-6">
              <label className="flex items-center">
                <input
                  type="radio"
                  id="active"
                  name="status"
                  value="active"
                  checked={categoryStatus === 'active'}
                  onChange={(e) => setCategoryStatus(e.target.value)}
                  className="form-radio text-cyan-500"
                />
                <span className="ml-2 text-gray-700">Active</span>
              </label>
              <label className="flex items-center">
                <input
                  type="radio"
                  id="inactive"
                  name="status"
                  value="inactive"
                  checked={categoryStatus === 'inactive'}
                  onChange={(e) => setCategoryStatus(e.target.value)}
                  className="form-radio text-cyan-500"
                />
                <span className="ml-2 text-gray-700">Inactive</span>
              </label>
            </div>
          </fieldset>
        </div>
        <div className="mb-6">
          <fieldset className="flex flex-col space-y-4">
            <legend className="text-gray-700 text-sm font-medium mb-2">Reward Category</legend>
            <div className="flex items-center space-x-6">
              <label className="flex items-center">
                <input
                  type="radio"
                  id="reward"
                  name="reward-category"
                  value="reward"
                  checked={rewardCategory === 'reward'}
                  onChange={(e) => setRewardCategory(e.target.value)}
                  className="form-radio text-cyan-500"
                />
                <span className="ml-2 text-gray-700">Yes</span>
              </label>
              <label className="flex items-center">
                <input
                  type="radio"
                  id="no-reward"
                  name="reward-category"
                  value="no-reward"
                  checked={rewardCategory === 'no-reward'}
                  onChange={(e) => setRewardCategory(e.target.value)}
                  className="form-radio text-cyan-500"
                />
                <span className="ml-2 text-gray-700">No</span>
              </label>
            </div>
          </fieldset>
        </div>
        <div className="flex justify-end space-x-4">
          <button
            className="bg-gray-500 hover:bg-gray-600 text-white font-bold py-2 px-4 rounded-md"
            type="button"
            onClick={handleCancel}
          >
            Cancel
          </button>
          <button
            className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded-md"
            type="submit"
          >
            {isEditing ? 'Save Changes' : 'Add Category'}
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddCategory;
