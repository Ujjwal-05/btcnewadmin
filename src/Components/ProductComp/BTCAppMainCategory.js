import React, { useState } from "react";
import { FaCoffee, FaPlus } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

const initialUsers = [
  { name: "Dhiraj", sequence: 1, status: "Active" },
  { name: "Purav", sequence: 2, status: "Active" },
  { name: "Anup Kumar", sequence: 3, status: "Inactive" },
  { name: "Raj", sequence: 4, status: "Inactive" },
  { name: "Rao", sequence: 5, status: "Active" },
  { name: "Anu", sequence: 6, status: "Inactive" },
  
];

const UserRow = ({ user, onEdit, toggleStatus }) => (
  <tr key={user.name} className="hover:bg-gray-100">
    <td className="px-6 py-4"><FaCoffee className="text-2xl text-gray-600" /></td>
    <td className="px-6 py-4">{user.name}</td>
    <td className="px-6 py-4">{user.sequence}</td>
    <td className="px-6 py-4">
      <button
        onClick={() => toggleStatus(user.name)}
        className={`${user.status === "Active" ? "bg-cyan-500 text-white" : "bg-gray-300 text-gray-700"} py-2 px-4 rounded-full text-sm`}
      >
        {user.status}
      </button>
    </td>
    <td className="px-6 py-4">
      <button onClick={() => onEdit(user.name, user.status)} className="text-cyan-600 hover:text-cyan-900">
        <FontAwesomeIcon icon={faEdit} className="mr-2" />
      </button>
    </td>
  </tr>
);

const BTCAppMainCategory = () => {
  const [users, setUsers] = useState(initialUsers);
  const [editingUser, setEditingUser] = useState(null);
  const [filters, setFilters] = useState({ name: "", sequence: "", status: "" });
  const navigate = useNavigate();

  const handleEdit = (name, status) => navigate('/add-category', { state: { categoryName: name, categoryStatus: status } });
  const handleAddProduct = () => navigate("/add-category");

  const handleSaveEdit = () => {
    if (editingUser) {
      setUsers(users.map(user => user.name === editingUser.name ? editingUser : user));
      setEditingUser(null);
    }
  };

  const toggleStatus = name => setUsers(users.map(user => user.name === name ? { ...user, status: user.status === "Active" ? "Inactive" : "Active" } : user));
  const handleFilterChange = (e, field) => setFilters({ ...filters, [field]: e.target.value });

  const filteredUsers = users.filter(user =>
    (filters.status === "" || user.status === filters.status) &&
    user.name.toLowerCase().includes(filters.name.toLowerCase()) &&
    user.sequence.toString().includes(filters.sequence)
  );

  return (
    <div className="flex min-h-screen">
      <div className="flex-1">
        <div className="rounded-lg overflow-hidden p-1">
          <div className="overflow-x-auto">
            <div className="bg-white p-4 m-4 rounded-lg shadow-md">
              <div className="flex justify-between">
                <h1 className="text-2xl font-light rounded-md p-4 bg-white text-cyan-400 flex items-center">
                  <FaPlus className="text-white m-4 bg-cyan-400 rounded-lg p-2 w-10 h-10" />
                  BTC App Main Category
                </h1>
                <button onClick={handleAddProduct} className="bg-cyan-500 hover:bg-cyan-600 text-white rounded-lg h-10 w-26 p-2 mt-10">
                  Add Product
                </button>
              </div>
              <div className="bg-slate-100 p-4 m-4 rounded-lg">
                <table className="min-w-full divide-y bg-white divide-gray-200">
                  <thead className="bg-white">
                    <tr className="bg-gray-200">
                      <th className="px-6 py-3">Icon</th>
                      <th className="px-6 py-3">Name</th>
                      <th className="px-6 py-3">Sequence</th>
                      <th className="px-6 py-3">Status</th>
                      <th className="px-6 py-3">Action</th>
                    </tr>
                    <tr className="bg-white">
                      <td></td>
                      <td className="px-6 py-3"><input type="text" value={filters.name} onChange={e => handleFilterChange(e, "name")} placeholder="Search by name" className="p-2 focus:outline-none text-gray-600 shadow-sm font-light sm:text-sm border border-gray-300 rounded-md" /></td>
                      <td className="px-6 py-3"><input type="text" value={filters.sequence} onChange={e => handleFilterChange(e, "sequence")} placeholder="Search by sequence" className="p-2 focus:outline-none text-gray-600 shadow-sm font-light sm:text-sm border border-gray-300 rounded-md" /></td>
                      <td className="px-6 py-3"><select value={filters.status} onChange={e => handleFilterChange(e, "status")} className="p-2 focus:outline-none text-gray-600 shadow-sm font-light sm:text-sm border-gray-300 rounded-md"><option value="">All</option><option value="Active">Active</option><option value="Inactive">Inactive</option></select></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody className="divide-y divide-gray-200">
                    {filteredUsers.map(user => <UserRow key={user.name} user={user} onEdit={handleEdit} toggleStatus={toggleStatus} />)}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {editingUser && (
            <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
              <div className="bg-white p-8 rounded-lg shadow-lg max-w-sm w-full">
                <h2 className="text-lg font-semibold mb-4 text-gray-700">Edit User</h2>
                <div className="mb-4">
                  <label htmlFor="editName" className="block text-sm font-medium text-gray-700">Name</label>
                  <input type="text" id="editName" value={editingUser.name} onChange={e => setEditingUser({ ...editingUser, name: e.target.value })} className="mt-1 p-2 focus:outline-none text-gray-600 shadow-sm font-light sm:text-sm border-gray-300 rounded-md w-full" />
                  </div>
                <div className="mb-4"><label htmlFor="editSequence" className="block text-sm font-medium text-gray-700">Sequence</label><input type="text" id="editSequence" value={editingUser.sequence} onChange={e => setEditingUser({ ...editingUser, sequence: parseInt(e.target.value) })} className="mt-1 p-2 focus:outline-none text-gray-600 shadow-sm font-light sm:text-sm border-gray-300 rounded-md w-full" /></div>
                <div className="mb-4"><label htmlFor="editStatus" className="block text-sm font-medium text-gray-700">Status</label><select id="editStatus" value={editingUser.status} onChange={e => setEditingUser({ ...editingUser, status: e.target.value })} className="mt-1 p-2 focus:outline-none text-gray-600 shadow-sm font-light sm:text-sm border-gray-300 rounded-md w-full"><option value="Active">Active</option><option value="Inactive">Inactive</option></select></div>
                <div className="flex justify-end"><button onClick={handleSaveEdit} className="bg-cyan-500 text-white py-2 px-4 rounded-lg mr-2">Save</button><button onClick={() => setEditingUser(null)} className="bg-gray-300 text-gray-700 py-2 px-4 rounded-lg">Cancel</button></div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default BTCAppMainCategory;
