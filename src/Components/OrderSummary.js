import React, { useState } from "react";

function OrderSummary() {
  const [orders, setOrders] = useState([
    {
      userId: 110511,
      userName: "Sarabjeet Singh Sachdeva",
      orderId: 203806,
      orderDate: "2024-07-10T09:36:47.000Z",
      productName: "Iced Americano",
      productPrice: 290,
      quantity: 1,
      discountCode: "-",
      discountValue: "-",
      orderAmount: 254,
      productId: 11,
      storeName: "GK 2 (Unit 2)",
    },
    {
      userId: 9604,
      userName: "Naman Ahuja",
      orderId: 203805,
      orderDate: "2024-07-10T09:36:11.000Z",
      productName: "Chicken Sausage, Smoked Barbeque and Cheese Croissant",
      productPrice: 270,
      quantity: 1,
      discountCode: "-",
      discountValue: "-",
      orderAmount: 135,
      productId: 1919,
      storeName: "GK 1",
    },
    {
      userId: 164015,
      userName: "Siva K",
      orderId: 203803,
      orderDate: "2024-07-10T09:25:36.000Z",
      productName: "Seethargundu Estate - Light",
      productPrice: 540,
      quantity: 1,
      discountCode: "-",
      discountValue: "-",
      orderAmount: 598,
      productId: 1872,
      storeName: "HSR Sector",
    },
    {
      userId: 164015,
      userName: "Siva K",
      orderId: 203803,
      orderDate: "2024-07-10T09:25:36.000Z",
      productName: "Almond & Oats Choco Chip Cookies",
      productPrice: 70,
      quantity: 1,
      discountCode: "-",
      discountValue: "-",
      orderAmount: 598,
      productId: 1556,
      storeName: "HSR Sector",
    },
    {
      userId: 84214,
      userName: "Harish Das",
      orderId: 203800,
      orderDate: "2024-07-10T09:19:25.000Z",
      productName: "Latte",
      productPrice: 250,
      quantity: 1,
      discountCode: "-",
      discountValue: "-",
      orderAmount: 526,
      productId: 8,
      storeName: "Malad West",
    },
    {
      userId: 84214,
      userName: "Harish Das",
      orderId: 203800,
      orderDate: "2024-07-10T09:19:25.000Z",
      productName: "Trioccino",
      productPrice: 320,
      quantity: 1,
      discountCode: "-",
      discountValue: "-",
      orderAmount: 526,
      productId: 1778,
      storeName: "Malad West",
    },
  ]);

  const [startDate, setStartDate] = useState(new Date("2024-07-05T00:00:00.000Z"));
  const [endDate, setEndDate] = useState(new Date("2024-07-10T23:59:59.000Z"));
  const [selectedStore, setSelectedStore] = useState("All");

  const handleStartDateChange = (event) => {
    const startDate = new Date(event.target.value);
    setStartDate(startDate);
  };

  const handleEndDateChange = (event) => {
    const endDate = new Date(event.target.value);
    setEndDate(endDate);
  };

  const handleStoreChange = (event) => {
    setSelectedStore(event.target.value);
  };

  const filteredOrders = orders.filter((order) => {
    const orderDate = new Date(order.orderDate);
    return (
      orderDate >= startDate &&
      orderDate <= endDate &&
      (selectedStore === "All" || order.storeName === selectedStore)
    );
  });

  return (
    <div className="p-6 mt-10 mr-4 ml-4 mb-4 rounded-lg shadow-slate-600 bg-slate-100">
    <div className=" bg-white p-2 rounded-lg shadow-lg">
      <div className="flex justify-between items-center mb-4 p-2 rounded-lg">
        <div className="flex mb-4">
          <div className="flex flex-col">
            <label htmlFor="start-date" className=" font-bold text-gray-700 p-2 focus:outline-cyan-400">
              Start Date *
            </label>
            <input
              type="date"
              id="start-date"
              value={startDate.toISOString().split("T")[0]}
              onChange={handleStartDateChange}
              className="border rounded-md p-2 focus:outline-cyan-400"
            />
          </div>
          <div className="flex flex-col">
            <label htmlFor="end-date" className=" font-bold text-gray-700 p-2">
              End Date *
            </label>
            <input
              type="date"
              id="end-date"
              value={endDate.toISOString().split("T")[0]}
              onChange={handleEndDateChange}
              className="border rounded-md p-2 focus:outline-cyan-400"
            />
          </div>
        </div>
        <div className="flex flex-col ">
          <label htmlFor="store" className="font-bold text-gray-700 p-2">
            Select Store
          </label>
          <select
            id="store"
            value={selectedStore}
            onChange={handleStoreChange}
            className="border rounded-md p-2 focus:outline-cyan-400 mb-4"
          >
            <option value="All">All</option>
            <option value="GK 1">GK 1</option>
            <option value="GK 2 (Unit 2)">GK 2 (Unit 2)</option>
            <option value="HSR Sector">HSR Sector</option>
            <option value="Malad West">Malad West</option>
          </select>
        </div>
      </div>
      <table className="w-full border-collapse">
        <thead>
          <tr>
            <th className="px-4 py-2 border font-medium">Order ID</th>
            <th className="px-4 py-2 border font-medium">Order Date</th>
            <th className="px-4 py-2 border font-medium">Product Name</th>
            <th className="px-4 py-2 border font-medium">Quantity</th>
            <th className="px-4 py-2 border font-medium">Price</th>
            <th className="px-4 py-2 border font-medium">Discount Code</th>
            <th className="px-4 py-2 border font-medium">Discount Value</th>
            <th className="px-4 py-2 border font-medium">Order Amount</th>
            <th className="px-4 py-2 border font-medium">Product ID</th>
            <th className="px-4 py-2 border font-medium">Store Name</th>
          </tr>
        </thead>
        <tbody>
          {filteredOrders.map((order) => (
            <tr key={order.orderId}>
              <td className="px-4 py-2 border font-light">{order.orderId}</td>
              <td className="px-4 py-2 border font-light">{new Date(order.orderDate).toLocaleString()}</td>
              <td className="px-4 py-2 border font-light">{order.productName}</td>
              <td className="px-4 py-2 border font-light">{order.quantity}</td>
              <td className="px-4 py-2 border font-light">{order.productPrice}</td>
              <td className="px-4 py-2 border font-light">{order.discountCode}</td>
              <td className="px-4 py-2 border font-light">{order.discountValue}</td>
              <td className="px-4 py-2 border font-light">{order.orderAmount}</td>
              <td className="px-4 py-2 border font-light">{order.productId}</td>
              <td className="px-4 py-2 border font-light">{order.storeName}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
  );
}

export default OrderSummary;
