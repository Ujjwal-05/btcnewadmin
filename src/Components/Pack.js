import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

function Pack() {
  const initialPacks = [
    {
      title: '9th Birthday Blend',
      type: 'PREPAID',
      status: 'Active',
    },
    {
      title: '9th Birthday Blend',
      type: 'RECURRING',
      status: 'Inactive',
    },
    {
      title: 'Attikan Estate',
      type: 'PREPAID',
      status: 'Active',
    },
    {
      title: 'Attikan Estate',
      type: 'RECURRING',
      status: 'Inactive',
    },
    {
      title: 'Baarbara Estate',
      type: 'PREPAID',
      status: 'Inactive',
    },
    {
      title: 'Baarbara Estate',
      type: 'RECURRING',
      status: 'Inactive',
    },
    {
      title: 'Cold Brew Blend Bold',
      type: 'PREPAID',
      status: 'Active',
    },
    {
      title: 'Cold Brew Blend Bold',
      type: 'RECURRING',
      status: 'Inactive',
    },
  ];

  const [packs, setPacks] = useState(initialPacks);
  const [filteredPacks, setFilteredPacks] = useState(initialPacks); // State to manage filtered packs
  const statusOptions = ['Active', 'Inactive'];
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState(null);
  const [editFormData, setEditFormData] = useState({ title: '', type: '', status: '' });

  // Handle search input change
  const handleSearchChange = (e) => {
    const searchValue = e.target.value.toLowerCase();
    if (searchValue === '') {
      setFilteredPacks(packs); // Reset to default packs when search bar is empty
    } else {
      const filtered = packs.filter(pack =>
        pack.title.toLowerCase().includes(searchValue)
      );
      setFilteredPacks(filtered);
    }
  };

  // Effect to update filteredPacks whenever packs change
  useEffect(() => {
    setFilteredPacks(packs);
  }, [packs]);

  const handleEdit = (index) => {
    setCurrentEditIndex(index);
    setEditFormData(packs[index]);
    setIsEditModalOpen(true);
  };

  const handleEditChange = (e) => {
    const { name, value } = e.target;
    setEditFormData({ ...editFormData, [name]: value });
  };

  const handleEditSubmit = (e) => {
    e.preventDefault();
    const updatedPacks = [...packs];
    updatedPacks[currentEditIndex] = editFormData;
    setPacks(updatedPacks);
    setIsEditModalOpen(false);
  };

  return (
    <div className="p-4 min-h-screen">
      <div className="bg-white rounded-lg shadow-md overflow-hidden">
        <div className="flex items-center p-4">
          <button className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded mr-2">
            +
          </button> 
          <h2 className="text-2xl font-normal text-cyan-500 p-4 mr-4">Pack</h2>
         
        </div>
        <div className="bg-slate-100 p-4 m-4 rounded-lg">
        <input
            type="text"
            placeholder="Search by Title"
            onChange={handleSearchChange}
            className="px-3 py-2 border rounded-lg ml-2 text-gray-700 mb-4 focus:outline-none focus:border-cyan-500"
          />
          <div className="bg-slate-100 p-2">
          <table className="w-full text-left text-gray-500 dark:text-gray-400 rounded-lg">
            <thead className="bg-white rounded-lg border-b-2 m-2">
              <tr className="">
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Title</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Type</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Status</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Action</th>
              </tr>
            </thead>
            <tbody>
              {filteredPacks.map((pack, index) => (
                <tr key={index} className="bg-white border-b dark:bg-gray-600 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-900">
                  <td className="px-6 py-4 font-light text-gray-600">{pack.title}</td>
                  <td className="px-6 py-4 font-light lowercase text-sm text-gray-600">{pack.type}</td>
                  <td className="px-6 py-4">
                    <span
                      className={`inline-block px-3 py-2 rounded-full cursor-pointer text-sm font-normal ${
                        pack.status === 'Inactive' ? 'bg-gray-200 text-gray-700' : 'bg-cyan-400 text-white'
                      }`}
                      onClick={() => {
                        const updatedStatus = pack.status === 'Active' ? 'Inactive' : 'Active';
                        const updatedPacks = [...packs];
                        updatedPacks[index].status = updatedStatus;
                        setPacks(updatedPacks);
                      }}
                    >
                      {pack.status}
                    </span>
                  </td>
                  <td className="px-6 py-4">
                    <button onClick={() => handleEdit(index)} className="text-gray-600 hover:text-gray-900">
                    <FontAwesomeIcon icon={faEdit} className="mr-2" />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          </div>
        </div>
      </div>

      {isEditModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-75">
          <div className="bg-white p-6 rounded-lg shadow-lg w-1/3">
            <h2 className="text-2xl font-normal text-cyan-500 mb-4">Edit Pack</h2>
            <form onSubmit={handleEditSubmit}>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="title">
                  Title
                </label>
                <input
                  id="title"
                  name="title"
                  type="text"
                  value={editFormData.title}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-700  focus:outline-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="type">
                  Type
                </label>
                <input
                  id="type"
                  name="type"
                  type="text"
                  value={editFormData.type}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-700  focus:outline-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="status">
                  Status
                </label>
                <select
                  id="status"
                  name="status"
                  value={editFormData.status}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-700  focus:outline-cyan-500"
                >
                  {statusOptions.map((option) => (
                    <option key={option} value={option}>{option}</option>
                  ))}
                </select>
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsEditModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-normal py-2 px-4 rounded mr-2"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="bg-cyan-500 hover:bg-cyan-700 text-white font-normal py-2 px-4 rounded"
                >
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default Pack;
