import React, { useState } from 'react';

const Referral = () => {
  const [referralCode, setReferralCode] = useState('');
  const [purchaseAmount, setPurchaseAmount] = useState('');
  const [referrerCode, setReferrerCode] = useState('');
  const [referrerItem, setReferrerItem] = useState('');
  const [showReferralDropdown, setShowReferralDropdown] = useState(false);
  const [showReferrerDropdown, setShowReferrerDropdown] = useState(false);
  const [selectedReferral, setSelectedReferral] = useState('');
  const [selectedDiscount, setSelectedDiscount] = useState('');

  const handleReferralCodeChange = (event) => {
    setReferralCode(event.target.value);
  };

  const handlePurchaseAmountChange = (event) => {
    setPurchaseAmount(event.target.value);
  };

  const handleReferrerCodeChange = (event) => {
    setReferrerCode(event.target.value);
  };

  const handleReferrerItemChange = (event) => {
    setReferrerItem(event.target.value);
  };

  const handleReferralDropdownToggle = () => {
    setShowReferralDropdown(!showReferralDropdown);
  };

  const handleReferrerDropdownToggle = () => {
    setShowReferrerDropdown(!showReferrerDropdown);
  };

  const handleSelectReferral = (referral) => {
    setSelectedReferral(referral);
    setReferralCode(referral); // Update input field with selected referral
    setShowReferralDropdown(false); // Hide dropdown after selection
  };

  const handleSelectDiscount = (discount) => {
    setSelectedDiscount(discount);
    setReferrerCode(discount); // Update input field with selected discount
    setShowReferrerDropdown(false); // Hide dropdown after selection
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Handle form submission here
    console.log('Referral code:', referralCode);
    console.log('Purchase amount:', purchaseAmount);
    console.log('Referrer code:', referrerCode);
    console.log('Referrer item:', referrerItem);
    // Reset form fields
    setReferralCode('');
    setPurchaseAmount('');
    setReferrerCode('');
    setReferrerItem('');
  };

  // Sample list of referrals and discounts (replace with your actual data)
  const referrals = ['Referral A', 'Referral B', 'Referral C'];
  const discounts = ['DISCOUNT10', 'SUMMER20', 'SALE30'];

  return (
    <div className="container px-8 w-full">
      <div className="bg-gray-50 rounded-lg shadow-slate-900 pt-2 overflow-hidden w-full p-4 mt-10">
        <h1 className="text-2xl font-medium mb-6 text-cyan-500 pt-4 pl-4">
          Create Referral
        </h1>
        <form
          onSubmit={handleSubmit}
          className="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4 relative"
        >
          <div className="mb-6 relative">
            <label
              className="block text-gray-700 text-2xl font-normal mb-4"
              htmlFor="referralCode"
            >
              Referral Get
            </label>
            <div className="relative">
              <input
                className="shadow appearance-none w-full text-sm h-12 border rounded py-2 px-3 text-gray-800 leading-tight focus:outline-none focus:border-b-2 focus:border-cyan-400"
                id="referralCode"
                type="text"
                placeholder="Select Referral *"
                value={referralCode}
                onChange={handleReferralCodeChange}
                onClick={handleReferralDropdownToggle}
                readOnly // Prevent manual editing
              />
              {showReferralDropdown && (
                <div className="absolute z-50 mt-2 w-full bg-white shadow-lg rounded-lg border border-gray-200">
                  <ul>
                    {referrals.map((referral) => (
                      <li
                        key={referral}
                        className="px-4 py-2 cursor-pointer hover:bg-gray-100"
                        onClick={() => handleSelectReferral(referral)}
                      >
                        {referral}
                      </li>
                    ))}
                  </ul>
                </div>
              )}
            </div>
            <p className="text-gray-600 text-base font-light mt-2">
              Referral will get a free item once they register with a referrer code (one time).
            </p>
          </div>
          <hr />
          <div className="mb-6 mt-4 relative">
            <label
              className="block text-gray-700 text-2xl font-normal mb-4"
              htmlFor="purchaseAmount"
            >
              Referral Purchase X Amount of Items
            </label>
            <input
              className="shadow appearance-none w-full text-sm h-12 border rounded py-2 px-3 text-gray-800 leading-tight focus:outline-none focus:border-b-2 focus:border-cyan-400"
              id="purchaseAmount"
              type="number"
              placeholder="Amount"
              value={purchaseAmount}
              onChange={handlePurchaseAmountChange}
            />
            <p className="text-gray-600 text-base font-light mt-2">
              If A refers B, then A is the referrer, and B is the referral.
            </p>
          </div>
          <hr />
          <div className="mb-6 mt-4 relative">
            <label
              className="block text-gray-700 text-2xl font-normal mb-4"
              htmlFor="referrerCode"
            >
              Referrer Get (Code)
            </label>
            <div className="relative">
              <input
                className="shadow appearance-none w-full text-sm h-12 border rounded py-2 px-3 text-gray-800 leading-tight focus:outline-none focus:border-b-2 focus:border-cyan-400"
                id="referrerCode"
                type="text"
                placeholder="Select Discount Code"
                value={referrerCode}
                onChange={handleReferrerCodeChange}
                onClick={handleReferrerDropdownToggle}
                readOnly // Prevent manual editing
              />
              {showReferrerDropdown && (
                <div className="absolute z-50 mt-2 w-full bg-white shadow-lg rounded-lg border border-gray-200">
                  <ul>
                    {discounts.map((discount) => (
                      <li
                        key={discount}
                        className="px-4 py-2 cursor-pointer hover:bg-gray-100"
                        onClick={() => handleSelectDiscount(discount)}
                      >
                        {discount}
                      </li>
                    ))}
                  </ul>
                </div>
              )}
            </div>
            <p className="text-gray-600 text-base font-light mt-2">
              The referrer will get an item or discount on a selected item (one item).
            </p>
          </div>
          <hr />
          <div className="mb-6 mt-4">
            <label
              className="block text-gray-700 text-2xl font-normal mb-4"
              htmlFor="referrerItem"
            >
              Referrer Offer Validity (Days)
            </label>
            <input
              className="shadow appearance-none w-full text-sm h-12 border rounded py-2 px-3 text-gray-800 leading-tight focus:outline-none focus:border-b-2 focus:border-cyan-400"
              id="referrerItem"
              type="text"
              placeholder="Days"
              value={referrerItem}
              onChange={handleReferrerItemChange}
            />
            <p className="text-gray-600 text-base font-light mt-2">
              The referrer will get a specific item as a referral bonus (one item).
            </p>
          </div>
          <div className="flex items-center justify-end">
            <button
              className="bg-cyan-500 hover:bg-cyan-700 text-white text-sm font-light py-3 px-5 rounded-lg focus:outline-none focus:shadow-outline"
              type="submit"
            >
              SUBMIT
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Referral;
