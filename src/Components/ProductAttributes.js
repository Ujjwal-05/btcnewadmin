import React, { useState } from "react";
import Switch from "react-switch";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import FontAwesomeIcon
import {faEdit} from "@fortawesome/free-solid-svg-icons"; // Import the trash icon


const ProductAttribute = () => {
  const [packSizes, setPackSizes] = useState([
    { id: 1, name: "Small", status: "active" },
    { id: 2, name: "Medium", status: "active" },
    { id: 3, name: "Large", status: "inactive" },
    { id: 4, name: "Extra Large", status: "active" },
  ]);

  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editedPackSize, setEditedPackSize] = useState(null);
  const [error, setError] = useState("");

  const handleStatusChange = (index, checked) => {
    const newStatus = checked ? "active" : "inactive";
    const updatedPackSizes = [...packSizes];
    updatedPackSizes[index] = { ...updatedPackSizes[index], status: newStatus };
    setPackSizes(updatedPackSizes);
  };

  const handleEdit = (index) => {
    setEditedPackSize({ ...packSizes[index], index });
    setEditModalOpen(true);
  };

  const handleCloseModal = () => {
    setEditModalOpen(false);
    setEditedPackSize(null);
    setError("");
  };

  const handleSaveChanges = () => {
    if (!editedPackSize.name.trim()) {
      setError("Name is required.");
      return;
    }

    const updatedPackSizes = packSizes.map((packSize) =>
      packSize.id === editedPackSize.id ? editedPackSize : packSize
    );
    setPackSizes(updatedPackSizes);
    setEditModalOpen(false);
    setEditedPackSize(null);
    setError("");
  };

  const handleAddPackSize = () => {
    setEditedPackSize({
      id: packSizes.length + 1,
      name: "New Pack Size",
      status: "inactive",
    });
    setEditModalOpen(true);
    setError("");
  };

  return (
    <div className="flex">
      <div className="container mx-auto p-4 ">
        <div className="bg-white rounded-xl shadow-md overflow-hidden p-4">
          <div className="flex justify-between items-center mb-4">
            <h2 className="text-2xl font-light text-cyan-600 flex items-center">
              <button
                className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded"
                onClick={handleAddPackSize}
              >
                +
              </button>
              Product Attribute
            </h2>
            <button
              className="bg-cyan-300 hover:bg-cyan-400 m-3 shadow-lg text-white font-light py-2 px-4 rounded"
              onClick={handleAddPackSize}
            >
              Add Product Attribute
            </button>
          </div>
          <div className="p-4 bg-slate-100 rounded-xl">
            <div className="bg-white p-4 rounded-xl">
              <table className="w-full border-collapse">
                <thead>
                  <tr>
                    <th className="px-4 py-2 border-b text-left">Name</th>
                    <th className="px-4 py-2 border-b text-center">Status</th>
                    <th className="px-4 py-2 border-b text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {packSizes.map((packSize, index) => (
                    <tr key={packSize.id}>
                      <td className="px-6 py-4 border-b font-light">{packSize.name}</td>
                      <td className="px-6 py-4 border-b text-center font-light">
                        <div className="flex items-center justify-center font-light">
                          <Switch
                            onChange={(checked) => handleStatusChange(index, checked)}
                            checked={packSize.status === "active"}
                            onColor="#66E8F9"
                            onHandleColor="#FFFFFF"
                            handleDiameter={20}
                            uncheckedIcon={false}
                            checkedIcon={false}
                            height={12}
                            width={32}
                            className="react-switch"
                          />
                          <span className="ml-2">{packSize.status === "active" ? "Active" : "Inactive"}</span>
                        </div>
                      </td>
                      <td className="px-4 py-2 border-b text-center">
                        <button
                          className="inline-flex items-center"
                          onClick={() => handleEdit(index)}
                        >
                          <FontAwesomeIcon icon={faEdit} className="" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {/* Edit Modal */}
        {editModalOpen && (
          <div className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-white rounded-xl shadow-md p-6 w-full max-w-md">
              <h2 className="text-2xl font-light text-cyan-600 mb-4">
                {editedPackSize?.id ? "Edit Pack Size" : "Add Pack Size"}
              </h2>
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSaveChanges();
                }}
              >
                {error && <p className="text-red-500 mb-4">{error}</p>}
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                  <input
                    type="text"
                    className="border-2 border-gray-200 focus:outline-cyan-500 p-2 w-full"
                    value={editedPackSize?.name || ""}
                    onChange={(e) => setEditedPackSize({ ...editedPackSize, name: e.target.value })}
                  />
                </div>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Status:</label>
                  <div className="flex items-center">
                    <Switch
                      onChange={(checked) =>
                        setEditedPackSize({ ...editedPackSize, status: checked ? "active" : "inactive" })
                      }
                      checked={editedPackSize?.status === "active"}
                      onColor="#66E8F9"
                      onHandleColor="#FFFFFF"
                      handleDiameter={20}
                      uncheckedIcon={false}
                      checkedIcon={false}
                      height={12}
                      width={32}
                      className="react-switch"
                    />
                    <span className="ml-2">
                      {editedPackSize?.status === "active" ? "Active" : "Inactive"}
                    </span>
                  </div>
                </div>
                <div className="flex justify-end">
                  <button
                    type="button"
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                    onClick={handleCloseModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="submit"
                    className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                  >
                    {editedPackSize?.id ? "Save Changes" : "Add Pack Size"}
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductAttribute;
