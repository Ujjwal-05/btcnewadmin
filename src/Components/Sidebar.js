import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import companyLogo from './a1caa5d28d637e02d6e58bb5d246992f.png';
import {
  faTachometerAlt, faCoffee, faUser, faBox, faSync, faLayerGroup, faCoins,
  faPercentage, faRss, faCreditCard, faImage, faBell, faStore, faUserFriends,
  faShoppingCart, faChartBar, faTools, faCog, faChevronDown, faCircle
} from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom';

const Sidebar = ({ isOpen }) => {
  const [dropdowns, setDropdowns] = useState({
    master: false,
    user: false,
    product: false,
    loyalty: false,
    report: false
  });

  const navigate = useNavigate();

  const toggleDropdown = (name) => {
    setDropdowns(prevState => ({
      ...prevState,
      [name]: !prevState[name]
    }));
  };

  const handleNavigation = (path) => navigate(path);

  const menuItems = [
    { icon: faTachometerAlt, text: 'Dashboard', path: '/dashboard' },
    {
      icon: faBox, text: 'Masters', dropdown: 'master', items: [
        { text: 'Grinds', path: '/grinds' },
        { text: 'Mix Bags', path: '/mixbags' },
        { text: 'Frequencies', path: '/frequencies' },
        { text: 'Frequency Duration', path: '/frequency-duration' },
        { text: 'Product Pack Sizes', path: '/product-pack-sizes' },
        { text: 'Product Attributes', path: '/product-attributes' },
        { text: 'Product Tags', path: '/product-tags' },
        { text: 'Sampler Pack Coffee', path: '/sampler-pack-coffee' },
      ]
    },
    {
      icon: faUser, text: 'Users', dropdown: 'user', items: [
        { text: 'User', path: '/user' },
        { text: 'User Group', path: '/user-group' },
        { text: 'User Permission', path: '/user-permission' },
      ]
    },
    {
      icon: faCoffee, text: 'Product', path: '/product', 
    },
    {
      icon: faCoins, text: 'Loyalty', dropdown: 'loyalty', items: [
        { text: 'Redeem Loyalty Points On Product', path: '/redeem-loyalty-points-on-product' },
        { text: 'Points On Wallet Topup', path: '/points-on-wallet-topup' },
        { text: 'Earn Loyalty Points', path: '/earn-loyalty-points' },
      ]
    },
    { icon: faPercentage, text: 'Pack', path: '/pack' },
    { icon: faPercentage, text: 'Subscription', path: '/subscription' },
    { icon: faPercentage, text: 'Discount', path: '/discount' },
    { icon: faRss, text: 'Feed', path: '/feed' },
    { icon: faCreditCard, text: 'Payment Method', path: '/payment-method' },
    { icon: faImage, text: 'Banner', path: '/banner' },
    { icon: faBell, text: 'Notification List', path: '/notification-list' },
    { icon: faStore, text: 'Store', path: '/store' },
    { icon: faUserFriends, text: 'Referral', path: '/referral' },
    { icon: faShoppingCart, text: 'Order', path: '/order' },
    {
      icon: faChartBar, text: 'Report', dropdown: 'report', items: [
        { text: 'Order Summary', path: '/order-summary' },
        { text: 'Subscription Summary', path: '/subscription-summary' },
        { text: 'Total Payment Collection', path: '/total-payment-collection' },
      ]
    },
    { icon: faTools, text: 'Build Setup', path: '/build-setup' },
    { icon: faCog, text: 'Settings', path: '/settings' }
  ];

  return (
    <div
      className={`fixed top-0 left-0 h-full bg-white text-black transition-transform duration-300 ease-in-out ${isOpen ? 'translate-x-0' : '-translate-x-full'} w-64 flex flex-col`}
    >
      <div className="flex-shrink-0 flex items-center justify-center h-16 shadow-md">
        <h1 className="text-xl font-light">Blue Tokai Coffee</h1>
      </div>
      <div className="flex-shrink-0 flex items-center justify-center h-24   ">
        <img src={companyLogo} className="w-24 h-24 rounded-full shadow-md " alt="Company Logo" />
      </div>

      <nav className="flex-grow overflow-y-auto">
        <ul className="flex flex-col py-5">
          {menuItems.map((item, index) => (
            item.dropdown ? (
              <li key={index} className="relative">
                <button
                  className="flex items-center justify-between px-5 py-3 hover:bg-gray-300 rounded-full transition-colors duration-200 w-full text-left"
                  onClick={() => toggleDropdown(item.dropdown)}
                  aria-expanded={dropdowns[item.dropdown]}
                  aria-controls={`${item.dropdown}-menu`}
                >
                  <div className="flex items-center">
                    <FontAwesomeIcon icon={item.icon} className="mr-2" />
                    {item.text}
                  </div>
                  <FontAwesomeIcon icon={faChevronDown} className={`ml-2 transition-transform duration-200 ${dropdowns[item.dropdown] ? 'rotate-180' : 'rotate-0'}`} />
                </button>
                <ul id={`${item.dropdown}-menu`} className={`ml-6  bg-white text-base text-gray-800 rounded-lg shadow-lg transition-all duration-300 ${dropdowns[item.dropdown] ? 'max-h-screen' : 'max-h-0 overflow-hidden'}`}>
                  {item.items.map((subItem, subIndex) => (
                    <li key={subIndex} className="py-2 px-4 hover:bg-gray-200 rounded-full flex items-center">
                      <FontAwesomeIcon icon={faCircle} className="mr-2 text-xs" />
                      <a onClick={() => handleNavigation(subItem.path)} style={{ cursor: 'pointer' }}>
                        {subItem.text}
                      </a>
                    </li>
                  ))}
                </ul>
              </li>
            ) : (
              <li key={index} className="px-5 py-3 hover:bg-gray-300 rounded-full transition-colors duration-200">
                <a
                  className="flex items-center"
                  onClick={() => handleNavigation(item.path)}
                  style={{ cursor: 'pointer' }}
                >
                  <FontAwesomeIcon icon={item.icon} className="mr-2" />
                  {item.text}
                </a>
              </li>
            )
          ))}
        </ul>
      </nav>
    </div>
  );
};

export default Sidebar;
