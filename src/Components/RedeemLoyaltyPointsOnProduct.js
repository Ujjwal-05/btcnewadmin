import { useState } from 'react';

function RedeemLoyaltyPointsOnProduct() {
  const [products, setProducts] = useState([
    {
      id: 1,
      product: 'Brush & Bag with Artyfloraa',
      variation: '(Shopify)',
      subCategory: 'OTHERS',
      price: 1200,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 2,
      product: 'Build Your Own Ceramic Coffee Mug with It’s Cerapeutic',
      variation: '(Shopify)',
      subCategory: 'OTHERS',
      price: 2000,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 3,
      product: 'Espresso Brewing and Latte Art Workshop with Advance Training',
      variation: '(Shopify)',
      subCategory: 'OTHERS',
      price: 1500,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 4,
      product: 'FILTER PAPER 02 (V60) WHITE 2-4 CUPS (100Pcs)',
      variation: '(Shopify)',
      subCategory: 'Accessories',
      price: 650,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 5,
      product: 'Mocha',
      variation: '(Rista)',
      subCategory: 'Hot Brews',
      price: 200,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 6,
      product: 'Trioccino',
      variation: '(Rista)',
      subCategory: 'COLD BREWS',
      price: 260,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 7,
      product: '0.3 MM Cone Burr Shim',
      variation: '(Shopify)',
      subCategory: 'Spare Parts',
      price: 50,
      redeemPoints: 0,
      isChecked: false,
    },
    {
      id: 8,
      product: '100% Coffee',
      variation: 'With Soda (Rista)',
      subCategory: 'SPARKLING COOLERS',
      price: 190,
      redeemPoints: 0,
      isChecked: false,
    },
  ]);

  const handleCheckboxChange = (id) => {
    setProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id === id ? { ...product, isChecked: !product.isChecked } : product
      )
    );
  };

  return (
    <div className="container mx-auto p-4 rounded-lg">
      <div className="bg-white p-4 rounded-lg">
        <h1 className="text-2xl font-medium text-cyan-600 text-left mb-4 p-2">Redeem Loyalty Points On Product</h1>
        <div className="overflow-x-auto p-4 bg-slate-100 rounded-xl">
          <table className="min-w-full divide-y divide-gray-200 rounded-xl">
            <thead>
              <tr>
                <th className="px-6 py-3 bg-gray-50 text-left text-sm font-medium text-black uppercase tracking-wider">
                  <input type="checkbox" className="mr-2" />
                </th>
                <th className="px-6 py-3 bg-gray-50 text-left text-sm font-medium text-black uppercase tracking-wider">
                  Product
                </th>
                <th className="px-6 py-3 bg-gray-50 text-left text-sm font-medium text-black uppercase tracking-wider">
                  Variation
                </th>
                <th className="px-6 py-3 bg-gray-50 text-left text-sm font-medium text-black uppercase tracking-wider">
                  Sub Category
                </th>
                <th className="px-6 py-3 bg-gray-50 text-right text-sm font-medium text-black uppercase tracking-wider">
                  Price
                </th>
                <th className="px-6 py-3 bg-gray-50 text-right text-sm font-medium text-black uppercase tracking-wider">
                  Redeem Points
                </th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {products.map((product) => (
                <tr key={product.id}>
                  <td className="px-6 py-4 whitespace-nowrap">
                    <input
                      type="checkbox"
                      className="mr-2"
                      checked={product.isChecked}
                      onChange={() => handleCheckboxChange(product.id)}
                    />
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap">
                    <div className="text-sm font-medium text-gray-900">{product.product}</div>
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap">
                    <div className="text-sm text-gray-500">{product.variation}</div>
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap">
                    <div className="text-sm text-gray-500">{product.subCategory}</div>
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                    {product.price}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                    {product.redeemPoints}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default RedeemLoyaltyPointsOnProduct;
