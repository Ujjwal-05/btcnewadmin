import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";


const SamplerPackCoffee = () => {
  const [samplerPacks, setSamplerPacks] = useState([
    { name: "Sampler Pack 1" },
    { name: "Sampler Pack 2" },
    { name: "Sampler Pack 3" },
  ]);

  const [editModalOpen, setEditModalOpen] = useState(false);
  const [addModalOpen, setAddModalOpen] = useState(false);
  const [editedSamplerPack, setEditedSamplerPack] = useState(null);
  const [newSamplerPack, setNewSamplerPack] = useState({ name: "" });

  const handleEdit = (index) => {
    setEditedSamplerPack({ ...samplerPacks[index], index });
    setEditModalOpen(true);
  };

  const handleCloseModal = () => {
    setEditModalOpen(false);
    setAddModalOpen(false);
    setEditedSamplerPack(null);
  };

  const handleSaveChanges = () => {
    if (!editedSamplerPack) return;

    const updatedSamplerPacks = [...samplerPacks];
    updatedSamplerPacks[editedSamplerPack.index] = { name: editedSamplerPack.name };
    setSamplerPacks(updatedSamplerPacks);
    handleCloseModal();
  };

  const handleAddSamplerPack = () => {
    if (!newSamplerPack.name.trim()) return;

    const newPack = { name: newSamplerPack.name.trim() };
    setSamplerPacks([newPack, ...samplerPacks]);
    setNewSamplerPack({ name: "" });
    handleCloseModal();
  };

  const handleRemoveSamplerPack = (index) => {
    const updatedSamplerPacks = samplerPacks.filter((_, i) => i !== index);
    setSamplerPacks(updatedSamplerPacks);
  };

  return (
    <div className="flex">

      {/* Main Content */}
      <div className="flex-1 p-4">
        <div className="bg-white rounded-xl shadow-md overflow-hidden p-6">
          <div className="flex justify-between items-center mb-4">
            <div className="flex items-center">
              <FontAwesomeIcon icon={faPlus} className="text-cyan-600 mr-6 bg-cyan-200 p-4 rounded-md shadow-md" />
              <h2 className="text-2xl font-medium p-6 text-cyan-600">Sampler Pack Coffee</h2>
            </div>
            <button
              type="button"
              className="bg-cyan-500 hover:bg-cyan-600 text-white font-medium py-2 px-4 rounded"
              onClick={() => setAddModalOpen(true)}
            >
              Add 
            </button>
          </div>
          <div className="p-4 bg-slate-100 rounded-xl">
            <div className="bg-white p-4 rounded-xl">
              <table className="w-full border-collapse">
                <thead>
                  <tr>
                    <th className="px-4 py-2 border-b text-left">Name</th>
                    <th className="px-4 py-2 border-b text-center">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {samplerPacks.map((samplerPack, index) => (
                    <tr key={index} className="hover:bg-gray-200">
                      <td className="px-8 py-6 border-b font-light">{samplerPack.name}</td>
                      <td className="px-6 py-4 border-b text-center font-light">
                        <button
                          className="inline-flex items-center mr-2 "
                          onClick={() => handleEdit(index)}
                        >
                         <FontAwesomeIcon icon={faEdit} className="" />
                        </button>
                        <button
                          className="inline-flex items-center text-red-500 hover:text-red-700"
                          onClick={() => handleRemoveSamplerPack(index)}
                        >
                          <FontAwesomeIcon icon={faTrash} className="mr-2" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {/* Edit Modal */}
        {editModalOpen && (
          <div className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-white rounded-xl shadow-md p-6 w-full max-w-md">
              <h2 className="text-2xl font-light text-cyan-600 mb-4">Edit Sampler Pack Coffee</h2>
              <form>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                  <input
                    type="text"
                    className="rounded-md bg-slate-100 focus:outline-none focus:outline-cyan-400 p-2 w-full"
                    value={editedSamplerPack?.name || ""}
                    onChange={(e) =>
                      setEditedSamplerPack({ ...editedSamplerPack, name: e.target.value })
                    }
                  />
                </div>
                <div className="flex justify-end">
                  <button
                    type="button"
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                    onClick={handleCloseModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                    onClick={handleSaveChanges}
                  >
                    Save Changes
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}

        {/* Add Modal */}
        {addModalOpen && (
          <div className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-white rounded-xl shadow-md p-6 w-full max-w-md">
              <h2 className="text-2xl font-light text-cyan-600 mb-4">Add Sampler Pack Coffee</h2>
              <form>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                  <input
                    type="text"
                    className="rounded-md bg-slate-100 focus:outline-none focus:outline-cyan-400 p-2 w-full"
                    value={newSamplerPack.name}
                    onChange={(e) =>
                      setNewSamplerPack({ name: e.target.value })
                    }
                  />
                </div>
                <div className="flex justify-end">
                  <button
                    type="button"
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                    onClick={handleCloseModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                    onClick={handleAddSamplerPack}
                  >
                    Add Sampler Pack
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default SamplerPackCoffee;
