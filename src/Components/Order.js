import React, { useState } from 'react';

function Order() {
  const initialOrders = [
    {
      orderId: 199606,
      userName: 'Gautam Gambhir',
      mobile: '+91995392221',
      orderDate: '2024-07-02T03:25:22', // Using ISO format for consistency
      orderFrom: 'Application',
      status: 'Success',
      paymentMethod: 'Razorpay',
      total: 260.00,
    },
    {
      orderId: 199605,
      userName: 'Inderpreet Khurana',
      mobile: '+91981032969',
      orderDate: '2024-07-02T03:25:01',
      orderFrom: 'Application',
      status: 'Completed',
      paymentMethod: 'Razorpay',
      total: 260.00,
    },
    {
      orderId: 199604,
      userName: 'Anju Sivan',
      mobile: '+91964306892',
      orderDate: '2024-07-02T03:23:57',
      orderFrom: 'Application',
      status: 'Completed',
      paymentMethod: 'Razorpay',
      total: 240.00,
    },
    {
      orderId: 199603,
      userName: 'Chandra Sekhar',
      mobile: '+91967669191',
      orderDate: '2024-07-02T03:21:50',
      orderFrom: 'Application',
      status: 'Success',
      paymentMethod: 'Razorpay',
      total: 996.00,
    },
  ];

  const [orders, setOrders] = useState(initialOrders);
  const [startDate, setStartDate] = useState('2024-07-02');
  const [endDate, setEndDate] = useState('2024-07-02');
  const [selectedOrder, setSelectedOrder] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleStartDateChange = (e) => {
    setStartDate(e.target.value);
  };

  const handleEndDateChange = (e) => {
    setEndDate(e.target.value);
  };

  const handleSearch = () => {
    const filteredOrders = initialOrders.filter(order => {
      const orderDate = new Date(order.orderDate);
      const start = new Date(startDate);
      const end = new Date(endDate);

      return orderDate >= start && orderDate <= end;
    });

    setOrders(filteredOrders);
  };

  const handleEditClick = (order) => {
    setSelectedOrder(order);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setSelectedOrder(null);
  };

  return (
    <div className="bg-slate-100 m-4 p-4 rounded-md">
    <div className=" w-full">
      <div className="bg-white rounded-lg shadow-md p-4 overflow-hidden m-4">
        <h1 className="text-3xl font-normal mb-8 text-cyan-600">Order</h1>

        <div className="flex flex-col sm:flex-row sm:gap-6 mb-4 items-center">
          <div className="flex flex-col sm:flex-row gap-4 sm:gap-6 items-center">
            <div className="flex-1">
              <label htmlFor="startDate" className="block text-sm font-medium mb-2 text-gray-800">
                Start Date *
              </label>
              <input
                type="date"
                id="startDate"
                value={startDate}
                onChange={handleStartDateChange}
                className="mt-1 block w-full sm:w-36 rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:ring-cyan-400 focus:border-cyan-400 sm:text-sm"
              />
            </div>
            <div className="flex-1">
              <label htmlFor="endDate" className="block text-sm font-medium mb-2 text-gray-800">
                End Date *
              </label>
              <input
                type="date"
                id="endDate"
                value={endDate}
                onChange={handleEndDateChange}
                className="mt-1 block w-full sm:w-36 rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:ring-cyan-400 focus:border-cyan-400 sm:text-sm"
              />
            </div>
          </div>
          <button
            onClick={handleSearch}
            className="bg-cyan-400 hover:bg-cyan-500 text-white text-xs font-normal w-full sm:w-20 h-10 py-2 px-4 rounded-lg mt-4 sm:mt-5"
          >
            SEARCH
          </button>
        </div>

        <div className="overflow-x-auto">
          <table className="min-w-full divide-y divide-gray-200 bg-white">
            <thead className="bg-gray-100">
              <tr>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Order Id
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  User Name
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Mobile
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Order Date
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Order From
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Status
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Payment Method
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Total (₹)
                </th>
                <th
                  scope="col"
                  className="px-4 sm:px-8 py-4 text-xs font-medium tracking-wider text-left text-black uppercase"
                >
                  Action
                </th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {orders.map((order) => (
                <tr key={order.orderId} className="hover:bg-gray-50">
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm font-medium text-gray-800">
                    {order.orderId}
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    {order.userName}
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    {order.mobile}
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    {order.orderDate}
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    <button className="bg-cyan-400 text-sm font-light text-white py-1 px-3 rounded-full">
                      {order.orderFrom}
                    </button>
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    <button className={`bg-${order.status === 'Completed' ? 'red-500' : 'green-500'} font-light text-white py-1 px-3 rounded-full`}>
                      {order.status}
                    </button>
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    <button className="bg-sky-500 font-light text-white py-1 px-3 rounded-full">
                      {order.paymentMethod}
                    </button>
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    {order.total}
                  </td>
                  <td className="px-4 sm:px-8 py-4 whitespace-nowrap text-sm text-gray-800">
                    <button
                      onClick={() => handleEditClick(order)}
                      className="bg-gray-300 hover:bg-gray-400 text-gray-800 py-2 px-4 rounded-lg inline-flex items-center focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-400"
                    >
                      EDIT
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {isModalOpen && selectedOrder && (
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="bg-white p-4 sm:p-6 rounded-lg shadow-lg w-11/12 sm:w-96 mx-auto">
              <h2 className="text-lg font-medium text-gray-900 mb-4">Edit Order</h2>
              <div className="mb-4">
                <label className="block text-sm font-medium text-gray-700 mb-1">Order ID:</label>
                <input
                  type="text"
                  value={selectedOrder.orderId}
                  readOnly
                  className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-cyan-400 focus:ring focus:ring-cyan-400 focus:ring-opacity-50"
                />
              </div>
              <div className="mb-4">
                <label className="block text-sm font-medium text-gray-700 mb-1">User Name:</label>
                <input
                  type="text"
                  value={selectedOrder.userName}
                  readOnly
                  className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-cyan-400 focus:ring focus:ring-cyan-400 focus:ring-opacity-50"
                />
              </div>
              <div className="mb-4">
                <label className="block text-sm font-medium text-gray-700 mb-1">Mobile:</label>
                <input
                  type="text"
                  value={selectedOrder.mobile}
                  readOnly
                  className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-cyan-400 focus:ring focus:ring-cyan-400 focus:ring-opacity-50"
                />
              </div>
              <div className="mb-4">
                <label className="block text-sm font-medium text-gray-700 mb-1">Order Date:</label>
                <input
                  type="text"
                  value={selectedOrder.orderDate}
                  readOnly
                  className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-cyan-400 focus:ring focus:ring-cyan-400 focus:ring-opacity-50"
                />
              </div>
              <div className="flex justify-end mt-6">
                <button
                  onClick={closeModal}
                  className="bg-gray-300 hover:bg-gray-400 text-gray-800 py-2 px-4 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-400 mr-2"
                >
                  Cancel
                </button>
                <button
                  onClick={closeModal}
                  className="bg-cyan-400 hover:bg-cyan-500 text-white py-2 px-4 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-400"
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
    </div>
  );
}

export default Order;
