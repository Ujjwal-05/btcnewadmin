import React, { useState } from 'react';

function SubscriptionSummary() {
  const [deliveryDate, setDeliveryDate] = useState('');
  const [subscriptions, setSubscriptions] = useState([]);
  const [recordsPerPage, setRecordsPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);

  const handleSearch = () => {
    // Replace with actual API call
    const mockData = [
      {
        productName: 'Product 1',
        grind: 'Medium',
        variant: 'Whole Bean',
        quantity: 2,
      },
      {
        productName: 'Product 2',
        grind: 'Fine',
        variant: 'Ground Coffee',
        quantity: 1,
      },
      {
        productName: 'Product 3',
        grind: 'Coarse',
        variant: 'Espresso Roast',
        quantity: 3,
      },
      {
        productName: 'Product 4',
        grind: 'Medium',
        variant: 'French Roast',
        quantity: 2,
      },
      {
        productName: 'Product 5',
        grind: 'Fine',
        variant: 'Italian Roast',
        quantity: 1,
      },
    ];
    setSubscriptions(mockData);
  };

  const handleRecordsPerPageChange = (e) => {
    setRecordsPerPage(Number(e.target.value));
    setCurrentPage(1); // Reset to the first page when changing records per page
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * recordsPerPage;
  const endIndex = Math.min(startIndex + recordsPerPage, subscriptions.length);
  const paginatedSubscriptions = subscriptions.slice(startIndex, endIndex);

  return (
    <div>
    <div className="bg-slate-200 p-4 rounded-lg shadow-lg mt-16 mr-4 ml-4">
    <div className="mx-auto p-6 bg-gray-50 rounded-lg shadow-lg ">
      <h1 className="text-3xl font-normal text-cyan-600 mb-6 p-4">Subscription Summary</h1>

      <div className="mb-6">
        <label htmlFor="deliveryDate" className="block text-gray-700 font-medium mb-2">
          Delivery Date
        </label>
        <input
          type="date"
          id="deliveryDate"
          value={deliveryDate}
          onChange={(e) => setDeliveryDate(e.target.value)}
          className="shadow-sm border border-gray-300 rounded-lg w-18 py-2 px-4 mr-4 text-gray-700 focus:ring-2 focus:ring-blue-500 focus:border-blue-500"
        />

      <button
        onClick={handleSearch}
        className="bg-cyan-500 hover:bg-cyan-700 text-white font-light py-2 px-6 rounded-lg transition duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
      >
        SEARCH
      </button>
      </div>

      <div className="flex justify-between mb-6 mt-8">
        <div className="text-sm font-light text-gray-800">SUBSCRIPTION SUMMARY</div>
      </div>

      <div className="overflow-x-auto">
        <table className="min-w-full bg-white border border-gray-200 shadow-sm rounded-lg">
          <thead className="bg-gray-100">
            <tr>
              <th className="px-6 py-4 text-left text-xs font-semibold text-gray-800 uppercase tracking-wider">
                Product Name
              </th>
              <th className="px-6 py-4 text-left text-xs font-semibold text-gray-800 uppercase tracking-wider">
                Grind
              </th>
              <th className="px-6 py-4 text-left text-xs font-semibold text-gray-800 uppercase tracking-wider">
                Variant
              </th>
              <th className="px-6 py-4 text-left text-xs font-semibold text-gray-800 uppercase tracking-wider">
                Quantity
              </th>
            </tr>
          </thead>
          <tbody>
            {paginatedSubscriptions.length > 0 ? (
              paginatedSubscriptions.map((subscription, index) => (
                <tr key={index} className="bg-white border-b hover:bg-gray-50">
                  <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                    {subscription.productName}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-600">
                    {subscription.grind}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-600">
                    {subscription.variant}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-600">
                    {subscription.quantity}
                  </td>
                </tr>
              ))
            ) : (
              <tr className="bg-white border-b hover:bg-gray-50">
                <td
                  colSpan={4}
                  className="px-6 py-4 whitespace-nowrap text-center text-sm font-medium text-gray-600"
                >
                  Sorry, no matching records found
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>

      <div className="flex justify-between items-center mt-6">
        <div className="text-sm text-gray-500">Records per page:</div>
        <div className="flex items-center">
          <select
            value={recordsPerPage}
            onChange={handleRecordsPerPageChange}
            className="border border-gray-300 rounded-md px-3 py-1 mr-2  focus:outline-cyan-400"
          >
            <option value={10}>10</option>
            <option value={25}>25</option>
            <option value={50}>50</option>
          </select>
          <div className="text-sm text-gray-500 mr-2">
            {startIndex + 1} - {endIndex} of {subscriptions.length}
          </div>
          <button
            onClick={() => handlePageChange(currentPage - 1)}
            className="bg-gray-300 hover:bg-gray-400 text-gray-700 font-bold py-1 px-2 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
            disabled={currentPage === 1}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l3.293-3.293a1 1 0 011.414-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
          <button
            onClick={() => handlePageChange(currentPage + 1)}
            className="bg-gray-300 hover:bg-gray-400 text-gray-700 font-bold py-1 px-2 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
            disabled={currentPage === Math.ceil(subscriptions.length / recordsPerPage)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-3.293 3.293a1 1 0 01-1.414 1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        </div>
      </div>
    </div>
    </div>
    </div>
  );
}

export default SubscriptionSummary;
