import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
// Sample data
const stores = [
  {
    storeImage: 'https://placehold.co/150',
    name: 'Acme Ozone',
    totalTables: 80,
    phone: '7042206468',
    city: 'Thane',
    status: 'Active',
  },
  {
    storeImage: 'https://placehold.co/150',
    name: 'Aero Mall, Pune',
    totalTables: 71,
    phone: '9205931188',
    city: 'Pune',
    status: 'Active',
  },
  {
    storeImage: 'https://placehold.co/150',
    name: 'Aero Mall, Pune',
    totalTables: 71,
    phone: '9205931188',
    city: 'Pune',
    status: 'Inactive',
  },
  // Add more store objects as needed
];

const Store = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [phoneSearchTerm, setPhoneSearchTerm] = useState('');
  const [filteredStores, setFilteredStores] = useState(stores);
  const [editStore, setEditStore] = useState(null);

  useEffect(() => {
    filterStores(searchTerm, phoneSearchTerm);
  }, [searchTerm, phoneSearchTerm]);

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  const handlePhoneSearch = (e) => {
    setPhoneSearchTerm(e.target.value);
  };

  const filterStores = (name, phone) => {
    const filtered = stores.filter((store) =>
      store.name.toLowerCase().includes(name.toLowerCase()) &&
      store.phone.toLowerCase().includes(phone.toLowerCase())
    );
    setFilteredStores(filtered);
  };

  const toggleStatus = (index) => {
    const updatedStores = [...filteredStores];
    updatedStores[index].status = updatedStores[index].status === 'Active' ? 'Inactive' : 'Active';
    setFilteredStores(updatedStores);
  };

  const handleEdit = (store) => {
    setEditStore(store);
  };

  const handleSave = () => {
    const updatedStores = filteredStores.map((store) =>
      store.name === editStore.name ? editStore : store
    );
    setFilteredStores(updatedStores);
    setEditStore(null);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    const limitedValue = name === 'phone' ? value.slice(0, 10) : value;
    setEditStore({
      ...editStore,
      [name]: limitedValue,
    });
  };

  return (
    <div className=" mx-auto h-full w-full p-4">
      <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10 p-4 shadow-gray-400">
        <div className="flex justify-between items-center mb-6">
          <h1 className="text-2xl font-light text-cyan-600 p-4 ">Stores</h1>
          <button className="bg-cyan-500 hover:bg-cyan-700 text-white font-bold py-2 px-4 rounded-full">
            Sync Stores
          </button>
        </div>
        <div className="bg-white shadow-md rounded-lg p-4 m-4">
          <table className="w-full border-rounded ">
            <thead>
              <tr className="font-light text-gray-600 border-b-2 m-2">
                <th className="py-2 px-4 text-normal font-semibold text-left">Store Image</th>
                <th className="py-2 px-4 text-normal font-semibold text-left">Name</th>
                <th className="py-2 px-4 text-normal font-semibold text-left">Total Tables</th>
                <th className="py-2 px-4 text-normal font-semibold text-left">Phone</th>
                <th className="py-2 px-4 text-normal font-semibold text-left">City</th>
                <th className="py-2 px-4 text-normal font-semibold text-left">Status</th>
                <th className="py-2 px-4 text-normal font-semibold text-left">Action</th>
              </tr>
              <tr>
                <th></th>
                <th className="py-2">
                  <input
                    type="text"
                    className="border rounded-lg px-2 focus:outline-none py-1 w-full font-light"
                    placeholder="Search by name..."
                    value={searchTerm}
                    onChange={handleSearch}
                  />
                </th>
                <th></th>
                <th className="py-2">
                  <input
                    type="text"
                    className="border rounded-lg px-2 focus:outline-none py-1 w-full font-light"
                    placeholder="Search by phone..."
                    value={phoneSearchTerm}
                    onChange={handlePhoneSearch}
                  />
                </th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {filteredStores.map((store, index) => (
                <tr key={store.name} className="border-b hover:bg-gray-200">
                  <td className="py-2 px-4 border-t">
                    <img src={store.storeImage} alt={store.name} className="w-14 h-14 rounded" />
                  </td>
                  <td className="py-2 px-4 border-t text-gray-600">{store.name}</td>
                  <td className="py-2 px-4 border-t text-gray-600">{store.totalTables}</td>
                  <td className="py-2 px-4 border-t text-gray-600">{store.phone}</td>
                  <td className="py-2 px-4 border-t text-gray-600">{store.city}</td>
                  <td className="py-2 px-4 border-t text-gray-600">
                    <button
                      className={`font-light py-1.5 px-3 rounded-full ${
                        store.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-300 text-gray-700'
                      }`}
                      onClick={() => toggleStatus(index)}
                    >
                      {store.status}
                    </button>
                  </td>
                  <td className="py-2 px-4 border-t">
                    <button
                      className="text-gray font-bold py-1 px-2 rounded-full flex items-center"
                      onClick={() => handleEdit(store)}
                    >
                     <FontAwesomeIcon icon={faEdit} className="mr-2" />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {/* Edit Modal */}
      {editStore && (
        <div className="fixed inset-0 bg-gray-600 bg-opacity-50 flex justify-center items-center shadow-md">
          <div className="bg-white rounded-lg p-6 w-1/2">
            <h2 className="text-2xl font-normal text-cyan-500 mb-4 ">Edit Store</h2>
            <div className="mb-4">
              <label className="block text-gray-700 font-bold mb-2">Name</label>
              <input
                type="text"
                name="name"
                value={editStore.name}
                onChange={handleChange}
                className="border font-light rounded-lg px-2 py-1 w-full"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-bold mb-2">Total Tables</label>
              <input
                type="number"
                name="totalTables"
                value={editStore.totalTables}
                onChange={handleChange}
                className="border font-light rounded-lg px-2 py-1 w-full"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-bold mb-2">Phone</label>
              <input
                type="text"
                name="phone"
                value={editStore.phone}
                onChange={handleChange}
                maxLength={10}
                className="border font-light rounded-lg px-2 py-1 w-full"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-bold mb-2">City</label>
              <input
                type="text"
                name="city"
                value={editStore.city}
                onChange={handleChange}
                className="border font-light rounded-lg px-2 py-1 w-full"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700 font-bold mb-2">Status</label>
              <select
                name="status"
                value={editStore.status}
                onChange={handleChange}
                className="border font-light rounded-lg px-2 py-1 w-full"
              >
                <option value="Active">Active</option>
                <option value="Inactive">Inactive</option>
              </select>
            </div>
            <div className="flex justify-end">
              <button
                className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded-full mr-2"
                onClick={() => setEditStore(null)}
              >
                Cancel
              </button>
              <button
                className="bg-cyan-500 hover:bg-cyan-700 text-white font-bold py-2 px-4 rounded-full"
                onClick={handleSave}
              >
                Save
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Store;
