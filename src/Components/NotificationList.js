import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';


function NotificationList() {
  const [notifications, setNotifications] = useState([
    { id: 1, title: 'Test', screen: '/home', group: '-', action: 'Edit' },
    { id: 2, title: 'Update Available', screen: '/updates', group: 'System', action: 'Edit' },
    { id: 3, title: 'New Message', screen: '/messages', group: 'User', action: 'Edit' },
    { id: 4, title: 'Maintenance', screen: '/maintenance', group: 'System', action: 'Edit' },
  ]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage, setRecordsPerPage] = useState(100);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentNotification, setCurrentNotification] = useState(null);

  // Handle page change
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  // Handle records per page change
  const handleRecordsPerPageChange = (event) => {
    setRecordsPerPage(parseInt(event.target.value));
    setCurrentPage(1); // Reset to first page when changing records per page
  };

  // Calculate start and end index for pagination
  const startIndex = (currentPage - 1) * recordsPerPage;
  const endIndex = startIndex + recordsPerPage;
  const displayedNotifications = notifications.slice(startIndex, endIndex);

  // Handle edit button click
  const handleEditClick = (notification) => {
    setCurrentNotification(notification);
    setIsEditModalOpen(true);
  };

  // Handle save changes
  const handleSaveChanges = () => {
    setNotifications((prevNotifications) =>
      prevNotifications.map((notif) =>
        notif.id === currentNotification.id ? currentNotification : notif
      )
    );
    setIsEditModalOpen(false);
  };

  // Handle input change in modal
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setCurrentNotification({ ...currentNotification, [name]: value });
  };

  return (
    <div className="bg-cyan-400 p-4 min-h-screen">
      <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10 p-4">
        <div className="flex items-center mb-4">
          <button className="bg-cyan-300 hover:bg-cyan-400 text-white font-bold py-2 px-4 rounded mr-6">
            +
          </button>
          <h2 className="text-2xl font-light text-cyan-600">Notification List</h2>
        </div>
        <div className="bg-slate-100 p-4 m-4 rounded-lg shadow-md">
        <table className="w-full text-left p-2 bg-white mt-2 mr-2">
          <thead className="border-b-2 "> 
            <tr>
              <th className="px-4 font-normal uppercase py-2">Title</th>
              <th className="px-4 font-normal uppercase py-2">Screen</th>
              <th className="px-4 font-normal uppercase py-2">Group</th>
              <th className="px-4 font-normal uppercase py-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {displayedNotifications.map((notification) => (
              <React.Fragment key={notification.id}>
                <tr className="hover:bg-gray-100">
                  <td className="px-6 font-light py-4">{notification.title}</td>
                  <td className="px-6 font-light py-4">{notification.screen}</td>
                  <td className="px-6 font-light py-4">{notification.group}</td>
                  <td className="px-6 font-light py-4">
                    <button
                      className="text-gray-600 hover:text-gray-900"
                      onClick={() => handleEditClick(notification)}
                    >
                      <FontAwesomeIcon icon={faEdit} className="mr-2" />
                    </button>
                  </td>
                </tr>
                <tr className="border-t border-gray-200"></tr>
              </React.Fragment>
            ))}
          </tbody>
        </table>
        <div className="flex justify-between items-center mt-4">
          <div>
            <label htmlFor="recordsPerPage" className="mr-2">
              Records per page:
            </label>
            <select
              id="recordsPerPage"
              value={recordsPerPage}
              onChange={handleRecordsPerPageChange}
              className="bg-gray-200 rounded px-2 py-1"
            >
              <option value={10}>10</option>
              <option value={25}>25</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
          </div>
          <div className="flex items-center">
            <button
              onClick={() => handlePageChange(currentPage - 1)}
              disabled={currentPage === 1}
              className="bg-gray-200 hover:bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded"
            >
              <svg
                className="h-5 w-5"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M15 19l-7-7 7-7"
                />
              </svg>
            </button>
            <span className="mx-4">
              {currentPage} - {Math.ceil(notifications.length / recordsPerPage)}
            </span>
            <button
              onClick={() => handlePageChange(currentPage + 1)}
              disabled={
                currentPage === Math.ceil(notifications.length / recordsPerPage)
              }
              className="bg-gray-200 hover:bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded"
            >
              <svg
                className="h-5 w-5"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M9 5l7 7-7 7"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
      </div>

      {isEditModalOpen && (
        <div className="w-66 fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-6 rounded shadow-md">
            <h2 className="text-xl font-bold mb-4">Edit Notification</h2>
            <div className="mb-4">
              <label className="block text-gray-700">Title</label>
              <input
                type="text"
                name="title"
                value={currentNotification.title}
                onChange={handleInputChange}
                className="w-full px-3 py-2 border rounded"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700">Screen</label>
              <input
                type="text"
                name="screen"
                value={currentNotification.screen}
                onChange={handleInputChange}
                className="w-full px-3 py-2 border rounded"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700">Group</label>
              <input
                type="text"
                name="group"
                value={currentNotification.group}
                onChange={handleInputChange}
                className="w-full px-3 py-2 border rounded"
              />
            </div>
            <div className="flex justify-end">
              <button
                className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                onClick={() => setIsEditModalOpen(false)}
              >
                Cancel
              </button>
              <button
                className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                onClick={handleSaveChanges}
              >
                Save
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default NotificationList;

