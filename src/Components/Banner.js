import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';


const data = [
  {
    title: 'Attikan Estate Best Seller',
    displayOn: 'Home, Menu',
    redirectMode: 'product',
    preference: 'specific',
    startDate: '22/01/2024',
    endDate: '07/03/2024',
    status: 'Inactive',
  },
  {
    title: 'Save as you sip',
    displayOn: 'Menu',
    redirectMode: 'none',
    preference: null,
    startDate: '18/10/2021',
    endDate: '31/08/2023',
    status: 'Inactive',
  },
  {
    title: 'Customised Sampler Pack',
    displayOn: 'Home',
    redirectMode: 'product',
    preference: 'specific',
    startDate: '02/11/2021',
    endDate: '20/04/2023',
    status: 'Inactive',
  },
  {
    title: 'Pair your favourite coffee with delicious food',
    displayOn: 'Menu',
    redirectMode: 'main_category',
    preference: 'specific',
    startDate: '06/07/2022',
    endDate: '31/12/2022',
    status: 'Inactive',
  },
  {
    title: 'Easy Pour',
    displayOn: 'Home',
    redirectMode: 'product',
    preference: 'specific',
    startDate: '11/10/2022',
    endDate: '30/06/2023',
    status: 'Active',
  },
  {
    title: 'Pair Coffee With Delicious Food',
    displayOn: 'Menu',
    redirectMode: 'product',
    preference: 'list',
    startDate: '27/10/2022',
    endDate: '01/01/2023',
    status: 'Inactive',
  },
  {
    title: 'Cold Brew Bags',
    displayOn: 'Home',
    redirectMode: 'product',
    preference: 'specific',
    startDate: '27/10/2022',
    endDate: '01/01/2023',
    status: 'Inactive',
  },
];

function Banner() {
  const [tableData, setTableData] = useState(data);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState(null);
  const [editFormData, setEditFormData] = useState({
    title: '',
    displayOn: '',
    redirectMode: '',
    preference: '',
    startDate: '',
    endDate: '',
    status: '',
  });
  const [searchQuery, setSearchQuery] = useState('');

  const openEditModal = (index) => {
    setCurrentEditIndex(index);
    setEditFormData(tableData[index]);
    setIsEditModalOpen(true);
  };

  const closeEditModal = () => {
    setIsEditModalOpen(false);
    setEditFormData({
      title: '',
      displayOn: '',
      redirectMode: '',
      preference: '',
      startDate: '',
      endDate: '',
      status: '',
    });
    setCurrentEditIndex(null);
  };

  const handleEditChange = (e) => {
    const { name, value } = e.target;
    setEditFormData({ ...editFormData, [name]: value });
  };

  const handleEditSubmit = (e) => {
    e.preventDefault();
    const updatedTableData = [...tableData];
    updatedTableData[currentEditIndex] = editFormData;
    setTableData(updatedTableData);
    closeEditModal();
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const filteredTableData = tableData.filter((item) =>
    item.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="p-4 min-h-screen max-w-screen">
      <div className="bg-white rounded-lg shadow-md p-4 mt-10 w-full lg:w-11/12 mx-auto">
        <div className="flex flex-col md:flex-row items-center justify-between">
          <div className="flex items-center p-4 w-full md:w-auto">
            <button className="bg-cyan-300 hover:bg-cyan-400 m-2 text-white font-bold py-2 px-4 rounded mr-2">
              +
            </button>
            <h2 className="text-2xl font-normal text-cyan-600 m-2">Banner</h2>
          </div>
          <div className="flex items-center w-full md:w-auto space-x-4 mt-4 md:mt-0">
            <input
              type="text"
              placeholder="Search by title..."
              value={searchQuery}
              onChange={handleSearchChange}
              className="w-full md:w-auto px-3 py-2 border rounded-lg text-gray-500 mr-4 focus:outline-none focus:border-cyan-500"
            />
          </div>
        </div>
        <div className="bg-slate-200 p-4 m-4 rounded-lg shadow-lg">
          <table className="w-full table-fixed text-left text-gray-500 mt-4">
            <thead className="bg-white border-b-2">
              <tr>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Title</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Display On</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Redirect Mode</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Preference</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Start Date</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">End Date</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Status</th>
                <th className="px-2 md:px-6 py-3 text-sm font-normal text-gray-900 uppercase tracking-wider">Action</th>
              </tr>
            </thead>
            <tbody>
              {filteredTableData.map((item, index) => (
                <tr key={index} className="bg-white border-b hover:bg-gray-100">
                  <td className="px-2 md:px-6 py-4 whitespace-normal">{item.title}</td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">{item.displayOn}</td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">{item.redirectMode}</td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">{item.preference}</td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">{item.startDate}</td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">{item.endDate}</td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">
                    <span
                      className={`inline-block px-3 py-1 rounded-full text-sm font-normal ${
                        item.status === 'Inactive' ? 'bg-gray-200 text-gray-700' : 'bg-cyan-400 text-white'
                      }`}
                    >
                      {item.status}
                    </span>
                  </td>
                  <td className="px-2 md:px-6 py-4 whitespace-normal">
                    <button
                      onClick={() => openEditModal(index)}
                      className="text-gray-600 hover:text-gray-900"
                    > <FontAwesomeIcon icon={faEdit} />
                     
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {isEditModalOpen && (
        <div className="fixed inset-0 z-50 flex items-center justify-center">
          <div className="fixed inset-0 bg-black opacity-30"></div>
          <div className="bg-white w-full sm:w-4/5 md:w-3/5 lg:w-1/2 p-6 rounded-lg shadow-lg z-10 overflow-auto max-h-screen">
            <h2 className="text-2xl font-semibold mb-4">Edit Banner</h2>
            <form onSubmit={handleEditSubmit} className="space-y-4">
              <div className="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-4">
                <input
                  type="text"
                  name="title"
                  placeholder="Title"
                  value={editFormData.title}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
                <input
                  type="text"
                  name="displayOn"
                  placeholder="Display On"
                  value={editFormData.displayOn}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-4">
                <input
                  type="text"
                  name="redirectMode"
                  placeholder="Redirect Mode"
                  value={editFormData.redirectMode}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
                <input
                  type="text"
                  name="preference"
                  placeholder="Preference"
                  value={editFormData.preference}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="flex flex-col sm:flex-row space-y-4 sm:space-y-0 sm:space-x-4">
                <input
                  type="date"
                  name="startDate"
                  placeholder="Start Date"
                  value={editFormData.startDate}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
                <input
                  type="date"
                  name="endDate"
                  placeholder="End Date"
                  value={editFormData.endDate}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <select
                name="status"
                value={editFormData.status}
                onChange={handleEditChange}
                className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
              >
                <option value="Inactive">Inactive</option>
                <option value="Active">Active</option>
              </select>
              <div className="flex justify-end  items-center pt-4">
                <button
                  type="submit"
                  className="bg-cyan-400 hover:bg-cyan-500 text-white font-bold py-2 px-4 mr-2 rounded"
                >
                  Save
                </button>
                <button
                  type="button"
                  onClick={closeEditModal}
                  className="bg-gray-300 hover:bg-gray-400 text-gray-700 font-bold py-2 px-4 rounded"
                >
                  Cancel
                </button>
              </div> 
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default Banner;



