import React, { useState } from 'react';

const LoyaltyPoints = () => {
  const initialFormData = {
    setupType: '',
    amount: '',
    loyaltyPoints: '',
    applicableFor: '',
    status: '',
  };

  const [loyaltyPoints, setLoyaltyPoints] = useState([
    {
      setupType: 'Earn',
      amount: 100,
      loyaltyPoints: 10,
      applicableFor: 'Order',
      status: 'Active',
    },
    {
      setupType: 'Redeem',
      amount: 50,
      loyaltyPoints: 5,
      applicableFor: 'Product',
      status: 'Inactive',
    },
    {
      setupType: 'Earn',
      amount: 200,
      loyaltyPoints: 20,
      applicableFor: 'Order',
      status: 'Active',
    },
  ]);

  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage, setRecordsPerPage] = useState(10);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editIndex, setEditIndex] = useState(null);
  const [formData, setFormData] = useState(initialFormData);
  const [statusFilter, setStatusFilter] = useState('');

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;

  const filteredRecords = loyaltyPoints.filter(point => 
    statusFilter === '' || point.status === statusFilter
  );
  const currentRecords = filteredRecords.slice(indexOfFirstRecord, indexOfLastRecord);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleRecordsPerPageChange = (recordsPerPage) => {
    setRecordsPerPage(recordsPerPage);
  };

  const handleEdit = (index) => {
    const filteredIndex = loyaltyPoints.findIndex(point => point === filteredRecords[index]);
    setEditIndex(filteredIndex);
    setFormData(loyaltyPoints[filteredIndex]);
    setIsModalOpen(true);
  };

  const handleSave = () => {
    const updatedPoints = [...loyaltyPoints];
    updatedPoints[editIndex] = formData;
    setLoyaltyPoints(updatedPoints);
    closeEditModal();
  };

  const closeEditModal = () => {
    setIsModalOpen(false);
    setEditIndex(null);
    setFormData(initialFormData);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleStatusFilterChange = (e) => {
    setStatusFilter(e.target.value);
    setCurrentPage(1);
  };

  return (
    <div className="bg-white rounded-lg p-4 ml-4 mr-4 mt-16">
      <div className="flex justify-between items-center mb-4">
        <button className="bg-cyan-300 hover:bg-cyan-400 text-left text-white font-bold py-2 px-4 rounded">
          +
        </button>
        <h2 className="text-2xl font-medium text-cyan-400 p-4">Earn Loyalty Points</h2>
        <div className="flex items-center ml-auto">
          <svg
            className="h-5 w-5 text-gray-500 mr-6"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
              clipRule="evenodd"
            />
          </svg>
        </div>
      </div>
      <div className="bg-slate-100 rounded-xl shadow-md overflow-hidden p-4">
        <table className="w-full table-auto divide-y divide-gray-200 rounded-xl">
          <thead>
            <tr>
              <th className="px-8 py-5 bg-gray-50 text-left text-xs font-bold text-gray-700 uppercase tracking-wider">
                Setup Type
              </th>
              <th className="px-8 py-5 bg-gray-50 text-left text-xs font-bold text-gray-700 uppercase tracking-wider">
                Amount
              </th>
              <th className="px-8 py-5 bg-gray-50 text-left text-xs font-bold text-gray-700 uppercase tracking-wider">
                Loyalty Points
              </th>
              <th className="px-8 py-5 bg-gray-50 text-left text-xs font-bold text-gray-700 uppercase tracking-wider">
                Applicable For
              </th>
              <th className="px-8 py-5 bg-gray-50 text-left text-xs font-bold text-gray-700 uppercase tracking-wider">
                Status
              </th>
              <th className="px-8 py-5 bg-gray-50 text-left text-xs font-bold text-gray-700 uppercase tracking-wider">
                Action
              </th>
            </tr>
            <tr>
              <th colSpan="6" className="px-8 py-5 bg-gray-50 text-right text-xs  pr-48 font-bold text-gray-700 uppercase tracking-wider">
                <label htmlFor="statusFilter" className="mr-4 pr-30 text-gray-700">
                  Filter by Status:
                </label>
                <select
                  id="statusFilter"
                  value={statusFilter}
                  onChange={handleStatusFilterChange}
                  className="focus:ring-cyan-500 focus:border-cyan-500 p-2 focus:outline-cyan-400 shadow-sm sm:text-sm border-gray-300 rounded-md"
                >
                  <option className="p-1" value="">All</option>
                  <option value="Active">Active</option>
                  <option value="Inactive">Inactive</option>
                </select>
              </th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {currentRecords.map((point, index) => (
              <tr key={index}>
                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                  {point.setupType}
                </td>
                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                  {point.amount}
                </td>
                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                  {point.loyaltyPoints}
                </td>
                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                  {point.applicableFor}
                </td>
                <td className="p-3">
                  <button className={`p-2.5 text-sm rounded-3xl ${point.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-300 text-black'}`}>
                    {point.status}
                  </button>
                </td>
                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-500">
                  <button
                    className="focus:outline-none "
                    onClick={() => handleEdit(index)}
                  >
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {/* Edit Modal */}
      {isModalOpen && (
        <div className="z-18 fixed inset-0 overflow-y-auto">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div className="fixed inset-0 transition-opacity" aria-hidden="true">
              <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
              &#8203;
            </span>
            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="w-full">
                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                      <h3 className="text-xl font-medium leading-6 text-cyan-500">Edit Loyalty Point</h3>
                      <div className="mt-5">
                        <div className="mb-4">
                          <label htmlFor="setupType" className="block text-sm focus:outline-cyan-500 font-medium text-gray-700">
                            Setup Type
                          </label>
                          <select
                            id="setupType"
                            name="setupType"
                            value={formData.setupType}
                            onChange={handleInputChange}
                            className="mt-1 block w-full p-2 border border-gray-300 focus:outline-cyan-500 rounded-md shadow-sm  sm:text-sm"
                          >
                            <option value="Earn">Earn</option>
                            <option value="Redeem">Redeem</option>
                          </select>
                        </div>
                        <div className="mb-4">
                          <label htmlFor="amount" className="block text-sm font-medium text-gray-700">
                            Amount
                          </label>
                          <input
                            type="text"
                            id="amount"
                            name="amount"
                            value={formData.amount}
                            onChange={handleInputChange}
                            className="mt-1 block w-full p-2 border focus:outline-cyan-500 border-gray-300 rounded-md shadow-sm focus:ring-cyan-500 focus:border-cyan-500 sm:text-sm"
                          />
                        </div>
                        <div className="mb-4">
                          <label htmlFor="loyaltyPoints" className="block text-sm font-medium text-gray-700">
                            Loyalty Points
                          </label>
                          <input
                            type="text"
                            id="loyaltyPoints"
                            name="loyaltyPoints"
                            value={formData.loyaltyPoints}
                            onChange={handleInputChange}
                            className="mt-1 block w-full p-2 border focus:outline-cyan-500 border-gray-300 rounded-md shadow-sm focus:ring-cyan-500 focus:border-cyan-500 sm:text-sm"
                          />
                        </div>
                        <div className="mb-4">
                          <label htmlFor="applicableFor" className="block text-sm font-medium text-gray-700">
                            Applicable For
                          </label>
                          <select
                            id="applicableFor"
                            name="applicableFor"
                            value={formData.applicableFor}
                            onChange={handleInputChange}
                            className="mt-1 block w-full p-2 border focus:outline-cyan-500 border-gray-300 rounded-md shadow-sm focus:ring-cyan-500 focus:border-cyan-500 sm:text-sm"
                          >
                            <option value="Order">Order</option>
                            <option value="Product">Product</option>
                          </select>
                        </div>
                        <div className="mb-4">
                          <label htmlFor="status" className="block text-sm font-medium text-gray-700">
                            Status
                          </label>
                          <select
                            id="status"
                            name="status"
                            value={formData.status}
                            onChange={handleInputChange}
                            className="mt-1 block w-full p-2 border focus:outline-cyan-500 border-gray-300 rounded-md shadow-sm focus:ring-cyan-500 focus:border-cyan-500 sm:text-sm"
                          >
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                  <button
                    onClick={handleSave}
                    type="button"
                    className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-cyan-500 text-base font-medium text-white hover:bg-cyan-600 focus:outline-none  sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    Save
                  </button>
                  <button
                    onClick={closeEditModal}
                    type="button"
                    className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none  sm:mt-0 sm:w-auto sm:text-sm"
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      {/* Pagination */}
      <div className="flex justify-between mt-4">
        <div className="text-sm text-gray-700">
          Showing {indexOfFirstRecord + 1} to {Math.min(indexOfLastRecord, filteredRecords.length)} of {filteredRecords.length} entries
        </div>
        <div className="flex items-center">
          <select
            value={recordsPerPage}
            onChange={(e) => handleRecordsPerPageChange(Number(e.target.value))}
            className="focus:ring-cyan-500 focus:border-cyan-500 p-2 focus:outline-cyan-400 shadow-sm sm:text-sm border-gray-300 rounded-md mr-2"
          >
            <option value={10}>10</option>
            <option value={20}>20</option>
            <option value={30}>30</option>
          </select>
          <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
            <button
              onClick={() => handlePageChange(currentPage > 1 ? currentPage - 1 : 1)}
              disabled={currentPage === 1}
              className={`relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 ${
                currentPage === 1 ? 'cursor-default' : 'hover:bg-gray-100'
              }`}
            >
              <span className="sr-only">Previous</span>
              {/* Heroicon name: solid/chevron-left */}
              <svg
                className="h-5 w-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M10.707 4.293a1 1 0 00-1.414 0l-4 4a1 1 0 000 1.414l4 4a1 1 0 001.414-1.414L8.414 10l2.293-2.293a1 1 0 000-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
            <button
              onClick={() => handlePageChange(currentPage < Math.ceil(filteredRecords.length / recordsPerPage) ? currentPage + 1 : currentPage)}
              disabled={currentPage >= Math.ceil(filteredRecords.length / recordsPerPage)}
              className={`relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 ${
                currentPage >= Math.ceil(filteredRecords.length / recordsPerPage) ? 'cursor-default' : 'hover:bg-gray-100'
              }`}
            >
              <span className="sr-only focus:outline-none">Next</span>
              {/* Heroicon name: solid/chevron-right */}
              <svg
                className="h-5 w-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M9.293 15.707a1 1 0 001.414 0l4-4a1 1 0 000-1.414l-4-4a1 1 0 00-1.414 1.414L11.586 10 9.293 12.293a1 1 0 000 1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default LoyaltyPoints;
