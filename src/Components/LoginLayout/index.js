import React, { useState } from "react";
import logo from "./a1caa5d28d637e02d6e58bb5d246992f.png";
import "./load.css"; // Import your CSS file

const LoginLayout = ({ isLogin, isVerifOtp, buttonClick, cancelClick }) => {
  const [phoneNumber, setPhoneNumber] = useState('+91 ');
  const [isPhoneNumberFocused, setIsPhoneNumberFocused] = useState(false);

  // New state to track if 10 or more digits are entered
  const [isPhoneNumberValid, setIsPhoneNumberValid] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault(); // Prevents form submission
    buttonClick(); // Calls the buttonClick function passed as prop
  };

  const handlePhoneNumberChange = (event) => {
    let input = event.target.value;
    // Remove all non-numeric characters except dash and spaces
    input = input.replace(/[^\d-\s]/g, '');
    // Remove the initial '91' from the input if it exists
    if (input.startsWith('91')) {
      input = input.substring(2).trim();
    }
    // Ensure input starts with '91 ' and format accordingly
    let formattedNumber = '91 ' + input.replace(/(\d{5})(\d+)/, '$1-$2');
    // Limit the length of the formatted number to ensure it doesn't exceed 15 characters
    if (formattedNumber.length > 15) {
      formattedNumber = formattedNumber.slice(0, 15);
    }
    setPhoneNumber('+' + formattedNumber);

    // Update the phone number valid state
    const digitsOnly = formattedNumber.replace(/\D/g, ''); // Remove non-digit characters
    setIsPhoneNumberValid(digitsOnly.length >= 12);
  };

  const handlePhoneNumberLabelClick = () => {
    document.getElementById('phoneInput').focus();
  };

  const handlePhoneNumberFocus = () => {
    setIsPhoneNumberFocused(true);
  };

  const handlePhoneNumberBlur = () => {
    setIsPhoneNumberFocused(false);
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-cyan-400">
      <div className="relative mb-2">
        <div className="w-32 h-32 bg-cyan-400 flex items-center justify-center absolute top-[-4rem] left-[-4rem] rounded-full">
          <img src={logo} className="w-28 h-28 rounded-md" alt="Logo" />
        </div>
      </div>

      <main className="max-w-sm w-full p-3 pt-20 bg-white rounded-md shadow-md">
        <form className="flex flex-col space-y-3 p-2 w-full" onSubmit={handleSubmit}>
          {isLogin ? (
            <label
              className="text-sm text-gray-700 cursor-pointer"
              onClick={handlePhoneNumberLabelClick}
            >
              Enter your phone number:
            </label>
          ) : (
            <label className="text-sm text-gray-700">Enter your OTP number:</label>
          )}
          <div className={`flex items-center border border-gray-300 rounded-md px-2 py-1 relative ${isPhoneNumberFocused ? 'phone-input-focused' : ''}`}>
            {isLogin && (
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_India.svg/800px-Flag_of_India.svg.png"
                className="w-6 h-auto mr-2"
                alt="India Flag"
              />
            )}
            <input
              id="phoneInput"
              className={`flex-grow px-2 py-1 border-none focus:outline-none`}
              type={isLogin ? "tel" : "text"}
              placeholder={isLogin ? "+91 " : "Enter OTP"}
              maxLength={isLogin ? "15" : "6"} // Adjust max length based on input type
              required // Ensure input is required
              inputMode="numeric" // Allows only numeric input
              pattern="[0-9\s-]*" // Corrected regular expression pattern
              value={isLogin ? phoneNumber : undefined} // Controlled input: value is set to phoneNumber state
              onChange={isLogin ? handlePhoneNumberChange : undefined} // onChange event handler
              onFocus={handlePhoneNumberFocus} // Handle focus event
              onBlur={handlePhoneNumberBlur} // Handle blur event
            />
          </div>

          {isVerifOtp ? (
            <div className="flex justify-end">
              <button
                onClick={cancelClick}
                type="button"
                className="w-20 p-2 mr-2 font-light text-white bg-stone-300 rounded-md hover:bg-stone-400 focus:outline-none focus:ring-2 focus:ring-stone-500"
              >
                Cancel
              </button>
              <button
                type="submit"
                className={`w-20 p-2 font-light text-white rounded-md ${isPhoneNumberValid ? 'bg-cyan-500 hover:bg-cyan-600' : 'bg-gray-400 hover:bg-gray-500'}`}
              >
                Continue
              </button>
            </div>
          ) : (
            <button
              type="submit"
              className={`self-end w-20 p-2 font-light text-white rounded-md ${isPhoneNumberValid ? 'bg-cyan-500 hover:bg-cyan-600' : 'bg-gray-400 hover:bg-gray-500'}`}
            >
              VERIFY
            </button>
          )}
          <span className="text-xs text-gray-500">
            {isLogin
              ? "By tapping Verify, an SMS may be sent. Message & data rates may apply."
              : "Please enter the OTP sent to your mobile number."
            }
          </span>
        </form>
      </main>
    </div>
  );
};

export default LoginLayout;
