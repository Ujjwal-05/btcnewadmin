import { useState } from 'react';
import Switch from 'react-switch';

function Settings() {
  const [limit, setLimit] = useState(2);
  const [priorDays, setPriorDays] = useState(2);
  const [upcomingDays, setUpcomingDays] = useState(7);
  const [contactNumber, setContactNumber] = useState('+919606047077');
  const [sendShopify, setSendShopify] = useState(false);
  const [sendPosShopify, setSendPosShopify] = useState(true);
  const [displayPack, setDisplayPack] = useState(false);
  const [overrideStatus, setOverrideStatus] = useState(true);
  const [websiteLoyalty, setWebsiteLoyalty] = useState(true);
  const [cafeLoyalty, setCafeLoyalty] = useState(true);

  const handleSubmit = () => {
    // Handle form submission logic here
    console.log('Form submitted!');
  };

  return (
    <div className="container mx-auto p-4">
      <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10 p-4 shadow-gray-400">
        <div className="flex justify-start items-center mb-4 ">
        <button className="bg-cyan-300 hover:bg-cyan-400  text-white font-bold py-2 px-4 mr-4 rounded">
            +
          </button>
          <h1 className="text-2xl font-light p-4 text-cyan-600">Settings</h1>
          
        </div>
        <div className="bg-slate-50 rounded-xl p-4">
          <div className="bg-white p-4">
            <form onSubmit={handleSubmit}>
              <div className="mb-4 flex items-center">
                <label htmlFor="limit" className="block text-gray-700 pb-4 font-bold w-1/2 pr-2">
                  Limit of Single User Login From the Multiple Devices * :
                </label>
                <input
                  type="number"
                  id="limit"
                  placeholder="Enter limit"
                  className="shadow appearance-none border rounded w-1/2 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={limit}
                  onChange={(e) => setLimit(parseInt(e.target.value, 10))}
                />
              </div>
              <div className="mb-4 flex items-center">
                <label htmlFor="priorDays" className="block pb-4 text-gray-700 font-bold w-1/2 pr-2">
                  Reschedule Subscription Delivery Date Prior to X Days * :
                </label>
                <input
                  type="number"
                  id="priorDays"
                  placeholder="Enter prior days"
                  className="shadow appearance-none border rounded w-1/2 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={priorDays}
                  onChange={(e) => setPriorDays(parseInt(e.target.value, 10))}
                />
              </div>
              <div className="mb-4 flex items-center">
                <label htmlFor="upcomingDays" className="block pb-4 text-gray-700 font-bold w-1/2 pr-2">
                  Reschedule Subscription Delivery Date Change to upcoming Y Days * :
                </label>
                <input
                  type="number"
                  id="upcomingDays"
                  placeholder="Enter upcoming days"
                  className="shadow appearance-none border rounded w-1/2 py-2 px-3  text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={upcomingDays}
                  onChange={(e) => setUpcomingDays(parseInt(e.target.value, 10))}
                />
              </div>
              <div className="mb-4 flex items-center">
                <label htmlFor="contactNumber" className="block text-gray-700 pb-4 font-bold w-1/2 pr-2">
                  Contact Us Number * :
                </label>
                <input
                  type="text"
                  id="contactNumber"
                  placeholder="Enter contact number"
                  className="shadow appearance-none border rounded w-1/2 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  value={contactNumber}
                  onChange={(e) => setContactNumber(e.target.value)}
                />
              </div>
              <div className="mb-4 flex items-center">
                <label className="block text-gray-700 font-bold w-1/2 pb-4 pr-2">
                  Send Subscription Order To Shopify :
                </label>
                <Switch
                  id="sendShopify"
                  checked={sendShopify}
                  onChange={setSendShopify}
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
              </div>
              <div className="mb-4 flex items-center">
                <label className="block text-gray-700 font-bold pb-4 w-1/2 pr-2">
                  Send Order To POS & Shopify :
                </label>
                <Switch
                  id="sendPosShopify"
                  checked={sendPosShopify}
                  onChange={setSendPosShopify}
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
              </div>
              <div className="mb-4 flex items-center">
                <label className="block text-gray-700 font-bold pb-4 w-1/2 pr-2">
                  Display Pack in App :
                </label>
                <Switch
                  id="displayPack"
                  checked={displayPack}
                  onChange={setDisplayPack}
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
              </div>
              <div className="mb-4 flex items-center">
                <label className="block text-gray-700 font-bold pb-4 w-1/2 pr-2">
                  Override Add-On and Variant Status Rista :
                </label>
                <Switch
                  id="overrideStatus"
                  checked={overrideStatus}
                  onChange={setOverrideStatus}
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
              </div>
              <div className="mb-4 flex items-center">
                <label className="block text-gray-700 pb-4 font-bold w-1/2 pr-2">
                  Loyalty earning from Website :
                </label>
                <Switch
                  id="websiteLoyalty"
                  checked={websiteLoyalty}
                  onChange={setWebsiteLoyalty}
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
              </div>
              <div className="mb-4 flex items-center">
                <label className="block text-gray-700 pb-4 font-bold w-1/2 pr-2">
                  Loyalty earning from Cafe :
                </label>
                <Switch
                  id="cafeLoyalty"
                  checked={cafeLoyalty}
                  onChange={setCafeLoyalty}
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
              </div>
              <button
                type="submit"
                className="bg-cyan-500 text-sm hover:bg-cyan-600 text-white font-normal py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              >
                SUBMIT
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Settings;
