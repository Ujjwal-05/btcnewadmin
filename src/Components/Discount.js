import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';


function Discount() {
  const [discounts, setDiscounts] = useState([
    {
      title: 'SUMMER25',
      code: 'Percentage',
      type: 'Regular',
      status: 'Active',
    },
    {
      title: 'WELCOME10',
      code: 'Fixed',
      type: 'Regular',
      status: 'Inactive',
    },
    {
      title: 'FREESHIP',
      code: 'Free Shipping',
      type: 'Regular',
      status: 'Active',
    },
    {
      title: 'FREESHIP',
      code: 'Free Shipping',
      type: 'regular',
      status: 'Active',
    },
    {
      title: 'FREESHIP',
      code: 'Free Shipping',
      type: 'regular',
      status: 'Active',
    },
  ]);

  const [searchQuery, setSearchQuery] = useState('');
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState(null);
  const [editFormData, setEditFormData] = useState({ title: '', code: '', type: '', status: '' });

  const handleEdit = (index) => {
    setCurrentEditIndex(index);
    setEditFormData(discounts[index]);
    setIsEditModalOpen(true);
  };

  const handleEditChange = (e) => {
    const { name, value } = e.target;
    setEditFormData({ ...editFormData, [name]: value });
  };

  const handleEditSubmit = (e) => {
    e.preventDefault();
    const updatedDiscounts = [...discounts];
    updatedDiscounts[currentEditIndex] = editFormData;
    setDiscounts(updatedDiscounts);
    setIsEditModalOpen(false);
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const filteredDiscounts = discounts.filter((discount) =>
    discount.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="bg-cyan-400 p-4 min-h-screen">
      <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10">
        <div className="flex items-center p-4">
          <button className="bg-cyan-300 hover:bg-cyan-400 m-4 text-white font-bold py-2 px-4 rounded mr-2">
            +
          </button>
          <h2 className="text-lg font-semibold text-cyan-600 m-4">Discounts</h2>
          <input
            type="text"
            value={searchQuery}
            onChange={handleSearchChange}
            placeholder="Search by title"
            className="ml-auto bg-gray-100 p-2 rounded-lg  focus:border-cyan-500 focus:shadow-outline-cyan"
          />
        </div>
        <div className="bg-slate-200 p-4 m-4 rounded-lg">
          <table className="w-full text-left text-gray-500 dark:text-gray-400">
            <thead>
              <tr>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Discount Title</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Discount Code</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Discount Type</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Status</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Action</th>
              </tr>
            </thead>
            <tbody>
              {filteredDiscounts.map((discount, index) => (
                <tr key={index} className="bg-white border-b dark:bg-gray-600 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-900">
                  <td className="px-6 py-4 font-light text-gray-600">{discount.title}</td>
                  <td className="px-6 py-4 font-light text-sm text-gray-600">{discount.code}</td>
                  <td className="px-6 py-4 font-light">{discount.type}</td>
                  <td className="px-6 py-4 font-light">
                    <span
                      className={`inline-block px-3 py-2 rounded-full text-sm font-normal ${
                        discount.status === 'Inactive' ? 'bg-gray-200 text-gray-700' : 'bg-cyan-500 text-white p-1'
                      }`}
                    >
                      {discount.status}
                    </span>
                  </td>
                  <td className="px-6 py-4">
                    <button onClick={() => handleEdit(index)} className="text-gray-600 hover:text-gray-900">
                    <FontAwesomeIcon icon={faEdit} className="mr-2" />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {isEditModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-75">
          <div className="bg-white p-6 rounded-lg shadow-lg w-1/3">
            <h2 className="text-xl font-normal text-cyan-500 mb-4">Edit Discount</h2>
            <form onSubmit={handleEditSubmit}>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="title">
                  Discount Title
                </label>
                <input
                  id="title"
                  name="title"
                  type="text"
                  value={editFormData.title}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="code">
                  Discount Code
                </label>
                <input
                  id="code"
                  name="code"
                  type="text"
                  value={editFormData.code}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="type">
                  Discount Type
                </label>
                <input
                  id="type"
                  name="type"
                  type="text"
                  value={editFormData.type}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="status">
                  Status
                </label>
                <select
                  id="status"
                  name="status"
                  value={editFormData.status}
                  onChange={handleEditChange}
                  className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                >
                  <option value="Active">Active</option>
                  <option value="Inactive">Inactive</option>
                </select>
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsEditModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-medium py-2 px-4 rounded-lg mr-2"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="bg-cyan-500 hover:bg-cyan-700 text-white font-medium py-2 px-4 rounded-lg"
                >
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default Discount;
