import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import FontAwesomeIcon
import {faEdit} from "@fortawesome/free-solid-svg-icons"; // Import the trash icon

const ProductTags = () => {
  const tagList = ["New", "Sale", "Limited", "Featured", "Discounted"]; // Predefined list of tags

  const [tags, setTags] = useState([
    { id: 1, name: tagList[0] },
    { id: 2, name: tagList[1] },
    { id: 3, name: tagList[2] },
  ]);

  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editedTag, setEditedTag] = useState(null);
  const [newTagAdded, setNewTagAdded] = useState(false); // Track if a new tag was added

  const handleEdit = (tag) => {
    setEditedTag(tag);
    setEditModalOpen(true);
  };

  const handleCloseModal = () => {
    setEditModalOpen(false);
    setEditedTag(null);
    if (newTagAdded) {
      // Remove the last added tag if cancel is clicked after adding a new tag
      const updatedTags = tags.slice(0, tags.length - 1);
      setTags(updatedTags);
      setNewTagAdded(false);
    }
  };

  const handleSaveChanges = () => {
    if (!editedTag) return; // Safety check

    const updatedTags = tags.map((tag) =>
      tag.id === editedTag.id ? { ...tag, name: editedTag.name } : tag
    );
    setTags(updatedTags);
    setEditModalOpen(false);
    setEditedTag(null);
    setNewTagAdded(false);
  };

  const handleAddTag = () => {
    const newTag = { id: tags.length + 1, name: `Tag ${tags.length + 1}` };
    setTags([...tags, newTag]);
    setEditedTag(newTag);
    setEditModalOpen(true);
    setNewTagAdded(true); // Track that a new tag was added
  };

  return (
    <div className="flex">
    
      <div className="container mx-auto p-4">
        <div className="bg-white rounded-xl shadow-md overflow-hidden p-4">
          <div className="flex justify-between items-center mb-4">
            <h2 className="text-2xl font-light text-cyan-600 flex items-center">
              <button
                className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded"
                onClick={handleAddTag}
              >
                +
              </button>
              Product Tags
            </h2>
            <button
              className="bg-cyan-300 hover:bg-cyan-400 m-3 shadow-lg text-white font-light py-2 px-4 rounded"
              onClick={handleAddTag}
            >
              Add Product Tag
            </button>
          </div>
          <div className="p-4 bg-slate-100 rounded-xl">
            <div className="bg-white p-4 rounded-xl">
              <table className="w-full border-collapse">
                <thead>
                  <tr>
                    <th className="px-4 py-2 border-b text-left">Name</th>
                    <th className="px-4 py-2 border-b text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {tags.map((tag) => (
                    <tr key={tag.id}>
                      <td className="px-6 py-4 border-b font-light">{tag.name}</td>
                      <td className="px-4 py-2 border-b text-center font-light">
                        <button
                          className="inline-flex items-center"
                          onClick={() => handleEdit(tag)}
                        >
                          <FontAwesomeIcon icon={faEdit} className="" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {/* Edit Modal */}
        {editModalOpen && (
          <div className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-white rounded-xl shadow-md p-6 w-full max-w-md">
              <h2 className="text-2xl font-light text-cyan-600 mb-4">
                {editedTag ? "Edit Tag" : "Add Tag"}
              </h2>
              <form>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                  <input
                    type="text"
                    className="border-2 border-gray-200 focus:outline-cyan-500 p-2 w-full"
                    value={editedTag?.name || ""}
                    onChange={(e) => setEditedTag({ ...editedTag, name: e.target.value })}
                  />
                </div>
                <div className="flex justify-end">
                  <button
                    type="button"
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                    onClick={handleCloseModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                    onClick={editedTag ? handleSaveChanges : handleAddTag}
                  >
                    {editedTag ? "Save Changes" : "Create Tag"}
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductTags;
