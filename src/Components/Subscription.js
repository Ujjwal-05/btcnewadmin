import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';


function Subscription() {
  const [subscriptions, setSubscriptions] = useState([
    {
      title: '9th Birthday Blend',
      type: 'PREPAID',
      status: 'Inactive',
    },
    {
      title: '9th Birthday Blend',
      type: 'RECURRING',
      status: 'Inactive',
    },
    {
      title: 'Attikan Estate',
      type: 'PREPAID',
      status: 'Active',
    },
    {
      title: 'Attikan Estate',
      type: 'RECURRING',
      status: 'Inctive',
    },
    {
      title: 'Baarbara Estate',
      type: 'PREPAID',
      status: 'Active',
    },
    {
      title: 'Baarbara Estate',
      type: 'RECURRING',
      status: 'Inactive',
    },
    {
      title: 'Cold Brew Blend Bold',
      type: 'PREPAID',
      status: 'Inactive',
    },
    {
      title: 'Cold Brew Blend Bold',
      type: 'RECURRING',
      status: 'Active',
    },
  ]);

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState(null);
  const [editFormData, setEditFormData] = useState({ title: '', type: '', status: '' });

  const handleEdit = (index) => {
    setCurrentEditIndex(index);
    setEditFormData(subscriptions[index]);
    setIsEditModalOpen(true);
  };

  const handleEditChange = (name, value) => {
    setEditFormData({ ...editFormData, [name]: value });
  };

  const handleEditSubmit = (e) => {
    e.preventDefault();
    const updatedSubscriptions = [...subscriptions];
    updatedSubscriptions[currentEditIndex] = editFormData;
    setSubscriptions(updatedSubscriptions);
    setIsEditModalOpen(false);
  };

  const handleStatusChange = (index) => {
    const updatedSubscriptions = [...subscriptions];
    updatedSubscriptions[index].status = updatedSubscriptions[index].status === 'Active' ? 'Inactive' : 'Active';
    setSubscriptions(updatedSubscriptions);
  };

  return (
    <div className=" p-4 min-h-screen">
      <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10">
        <div className="flex items-center p-4">
          <button className="bg-cyan-300 hover:bg-cyan-400 m-4 text-white font-bold py-2 px-4 rounded mr-2">
            +
          </button>
          <h2 className="text-2xl font-normal p-4 text-cyan-600">Subscription</h2>
        </div>
        <div className="bg-slate-200 p-4 rounded-lg m-4">
        
        <table className="w-full text-left text-gray-500 rounded-lg dark:text-gray-400">
          <thead className="bg-white rounded border-b-2">
            <tr>
              <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Title</th>
              <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Type</th>
              <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Status</th>
              <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Action</th>
            </tr>
          </thead>
          <tbody>
            {subscriptions.map((subscription, index) => (
              <tr key={index} className="bg-white border-b dark:bg-gray-600 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-900">
                <td className="px-6 py-4 text-gray-600">{subscription.title}</td>
                <td className="px-6 py-4 text-sm text-gray-600">{subscription.type}</td>
                <td className="px-6 py-4">
                  <button
                    type="button"
                    className={`px-3 py-2 rounded-full text-sm font-normal cursor-pointer ${subscription.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-200 text-gray-700'}`}
                    onClick={() => handleStatusChange(index)}
                  >
                    {subscription.status}
                  </button>
                </td>
                <td className="px-6 py-4">
                  <button onClick={() => handleEdit(index)} className="text-gray-600 hover:text-gray-900">
                  <FontAwesomeIcon icon={faEdit} className="mr-2" />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        </div>
      </div>

      {isEditModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-75">
          <div className="bg-white p-6 rounded-lg shadow-lg w-1/3">
            <h2 className="text-lg font-semibold text-cyan-500 mb-4">Edit Subscription</h2>
            <form onSubmit={handleEditSubmit}>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="title">
                  Title
                </label>
                <input
                  id="title"
                  name="title"
                  type="text"
                  value={editFormData.title}
                  onChange={(e) => handleEditChange(e.target.name, e.target.value)}
                  className="w-full px-3 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="type">
                  Type
                </label>
                <input
                  id="type"
                  name="type"
                  type="text"
                  value={editFormData.type}
                  onChange={(e) => handleEditChange(e.target.name, e.target.value)}
                  className="w-full px-3 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-cyan-500"
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="status">
                  Status
                </label>
                <div className="flex space-x-2">
                  <button
                    type="button"
                    className={`px-3 py-2 rounded-full text-sm font-normal cursor-pointer ${editFormData.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-200 text-gray-700'}`}
                    onClick={() => handleEditChange('status', 'Active')}
                  >
                    Active
                  </button>
                  <button
                    type="button"
                    className={`px-3 py-2 rounded-full text-sm font-normal cursor-pointer ${editFormData.status === 'Inactive' ? 'bg-cyan-500 text-white' : 'bg-gray-200 text-gray-700'}`}
                    onClick={() => handleEditChange('status', 'Inactive')}
                  >
                    Inactive
                  </button>
                </div>
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsEditModalOpen(false)}
                  className="bg-gray-300 hover:bg-gray-700 text-white font-normal py-2 px-4 rounded-lg mr-2"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="bg-cyan-500 hover:bg-cyan-700 text-white font-normal py-2 px-4 rounded-lg "
                >
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default Subscription;
