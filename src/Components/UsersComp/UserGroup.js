import React, { useState } from "react";
import Switch from "react-switch"; // Import Switch component
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";

const UserGroup = () => {
  const [userGroups, setUserGroups] = useState([
    { name: "Every 12 Days", days: 12, status: "active" },
    { name: "Every Month", days: 30, status: "active" },
    { name: "Every other month", days: 60, status: "inactive" },
    { name: "Every other week", days: 14, status: "active" },
    { name: "Every three weeks", days: 21, status: "inactive" },
    { name: "Every Week", days: 7, status: "active" },
    { name: "Pause", days: 365, status: "inactive" },
    { name: "test", days: 6, status: "inactive" },
  ]);

  const [editModalOpen, setEditModalOpen] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false);
  const [formData, setFormData] = useState({ name: "", days: "", status: "active" });

  const handleStatusChange = (index, checked) => {
    const newStatus = checked ? "active" : "inactive";
    setUserGroups((prevUserGroups) => {
      const updatedUserGroups = [...prevUserGroups];
      updatedUserGroups[index].status = newStatus;
      return updatedUserGroups;
    });
  };

  const handleEdit = (index) => {
    setFormData({ ...userGroups[index], index });
    setIsEditMode(true);
    setEditModalOpen(true);
  };

  const handleAdd = () => {
    setFormData({ name: "", days: "", status: "active" });
    setIsEditMode(false);
    setEditModalOpen(true);
  };

  const handleCloseModal = () => {
    setEditModalOpen(false);
  };

  const handleSaveChanges = () => {
    if (isEditMode) {
      setUserGroups((prevUserGroups) => {
        const updatedUserGroups = [...prevUserGroups];
        updatedUserGroups[formData.index] = { name: formData.name, days: formData.days, status: formData.status };
        return updatedUserGroups;
      });
    } else {
      if (!formData.name.trim() || !formData.days.trim()) {
        alert("Please enter both name and days.");
        return;
      }

      const newUserGroup = {
        name: formData.name.trim(),
        days: parseInt(formData.days.trim()),
        status: formData.status,
      };

      setUserGroups((prevUserGroups) => [...prevUserGroups, newUserGroup]);
    }
    setEditModalOpen(false);
  };

  const handleRemoveUserGroup = (index) => {
    setUserGroups((prevUserGroups) => prevUserGroups.filter((_, i) => i !== index));
  };

  return (
    <div className="flex">
      <div className="flex-1 p-4">
        <div className="bg-white rounded-xl shadow-md overflow-hidden p-4">
          <div className="mb-4 flex justify-between items-center">
            <h2 className="text-2xl font-light text-cyan-600">User Group</h2>
            <button
              className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded"
              onClick={handleAdd}
            >
              <FontAwesomeIcon icon={faPlus} className="mr-2" />
              Add
            </button>
          </div>
          <div className="p-4 bg-slate-100 rounded-xl">
            <div className="bg-white p-4 rounded-xl">
              <table className="w-full border-collapse">
                <thead>
                  <tr>
                    <th className="px-4 py-2 border-b text-left">Name</th>
                    <th className="px-4 py-2 border-b text-center">Type</th>
                    <th className="px-4 py-2 border-b text-center">Status</th>
                    <th className="px-4 py-2 border-b text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {userGroups.map((userGroup, index) => (
                    <tr key={index}>
                      <td className="px-6 py-4 border-b font-light">{userGroup.name}</td>
                      <td className="px-6 py-4 border-b text-center font-light">{userGroup.days}</td>
                      <td className="px-6 py-4 border-b text-center font-light">
                        <div className="flex items-center justify-center font-light">
                          <Switch
                            onChange={(checked) => handleStatusChange(index, checked)}
                            checked={userGroup.status === "active"}
                            onColor="#66E8F9"
                            onHandleColor="#FFFFFF"
                            handleDiameter={20}
                            uncheckedIcon={false}
                            checkedIcon={false}
                            height={12}
                            width={32}
                            className="react-switch"
                          />
                          <span className="ml-2">{userGroup.status === "active" ? "Active" : "Inactive"}</span>
                        </div>
                      </td>
                      <td className="px-4 py-2 border-b text-center">
                        <button
                          className="inline-flex items-center mr-2"
                          onClick={() => handleEdit(index)}
                        >
                          <FontAwesomeIcon icon={faEdit} className="mr-2" /> 
                        </button>
                        <button
                          className="inline-flex items-center font-light text-red-500"
                          onClick={() => handleRemoveUserGroup(index)}
                        >
                          <FontAwesomeIcon icon={faTrash} className="mr-2" /> 
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {/* Add/Edit Modal */}
        {editModalOpen && (
          <div className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-white rounded-xl shadow-md p-6 w-full max-w-md">
              <h2 className="text-2xl font-light text-cyan-600 mb-4">{isEditMode ? "Edit User Group" : "Add User Group"}</h2>
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSaveChanges();
                }}
              >
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                  <input
                    type="text"
                    className="border-2 border-gray-200 focus:outline-cyan-500 p-2 w-full"
                    value={formData.name}
                    onChange={(e) => setFormData({ ...formData, name: e.target.value })}
                  />
                </div>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Type:</label>
                  <input
                    type="number"
                    className="border-2 focus:outline-cyan-500 border-gray-200 p-2 w-full"
                    value={formData.days}
                    onChange={(e) => setFormData({ ...formData, days: e.target.value })}
                  />
                </div>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Status:</label>
                  <select
                    className="border-2 border-gray-200 focus:outline-cyan-500 p-2 w-full"
                    value={formData.status}
                    onChange={(e) => setFormData({ ...formData, status: e.target.value })}
                  >
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                  </select>
                </div>
                <div className="flex justify-end">
                  <button
                    type="button"
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                    onClick={handleCloseModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="submit"
                    className="bg-cyan-300 hover:bg-cyan-400 text-white font-bold py-2 px-4 rounded"
                  >
                    Save
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default UserGroup;
