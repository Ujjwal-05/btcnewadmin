import React, { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import Sidebar from "../Sidebar"; // Adjust the path as necessary

const EditModal = ({ isOpen, onClose, onSave, userId, userName, onInputChange, onUserIdChange }) => {
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
      <div className="bg-white rounded-lg p-6 shadow-md w-1/3">
        <h2 className="text-xl font-semibold text-cyan-500 mb-4">Edit User</h2>
        <div className="mb-4">
          <label className="block mb-2 text-sm font-medium text-gray-700">User ID</label>
          <input
            type="text"
            className="border rounded-md px-2 py-1 w-full focus:outline-none focus:ring-cyan-400 focus:border-cyan-400"
            value={userId}
            onChange={onUserIdChange}
          />
        </div>
        <div className="mb-4">
          <label className="block mb-2 text-sm font-medium text-gray-700">User Name</label>
          <input
            type="text"
            className="border rounded-md px-2 py-1 w-full focus:outline-none focus:ring-cyan-400 focus:border-cyan-400"
            value={userName}
            onChange={onInputChange}
          />
        </div>
        <div className="flex justify-end">
          <button
            className="px-4 py-2 mr-2 bg-gray-100 text-gray-700 rounded-full text-sm font-medium"
            onClick={onClose}
          >
            Cancel
          </button>
          <button
            className="px-4 py-2 bg-cyan-500 text-white rounded-full text-sm font-medium"
            onClick={onSave}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
};

const UserPermission = () => {
  const initialUsers = [
    { userId: 120, name: 'Dhiraj' },
    { userId: 108, name: 'Purav' },
    { userId: 149, name: 'Anup Kumar' },
    { userId: 167, name: 'Hritik Singh' },
    { userId: 135, name: 'Shivani' },
    { userId: 175, name: 'Saurabh' },
    { userId: 164, name: 'Gaurav' },
    { userId: 172, name: 'Nischal Patnaik' },
    { userId: 219, name: 'Abhishek Kumar' },
  ];

  const [users, setUsers] = useState(initialUsers);
  const [editingUserId, setEditingUserId] = useState(null);
  const [editedUserId, setEditedUserId] = useState('');
  const [editedUserName, setEditedUserName] = useState('');
  const [searchTerm, setSearchTerm] = useState('');
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleEditUser = (userId) => {
    const userToEdit = users.find((user) => user.userId === userId);
    if (userToEdit) {
      setEditingUserId(userId);
      setEditedUserId(userToEdit.userId.toString());
      setEditedUserName(userToEdit.name);
      setIsModalOpen(true);
    }
  };

  const handleSaveEdit = () => {
    if (editingUserId !== null) {
      setUsers(users.map((user) =>
        user.userId === editingUserId ? { ...user, userId: parseInt(editedUserId, 10), name: editedUserName } : user
      ));
      setEditingUserId(null);
      setEditedUserId('');
      setEditedUserName('');
      setIsModalOpen(false);
    }
  };

  const handleCancelEdit = () => {
    setEditingUserId(null);
    setEditedUserId('');
    setEditedUserName('');
    setIsModalOpen(false);
  };

  const handleInputChange = (event) => {
    setEditedUserName(event.target.value);
  };

  const handleUserIdChange = (event) => {
    setEditedUserId(event.target.value);
  };

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredUsers = users.filter((user) =>
    user.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
    user.userId.toString().includes(searchTerm)
  );

  return (
    <div className="flex">
      <Sidebar /> {/* Assuming the Sidebar component is correctly imported and used */}
      <div className="flex-1"> {/* Adjust container padding to fit Sidebar */}
        <div className="rounded-md min-h-screen p-4">
          <div className="bg-white rounded-lg shadow-md overflow-hidden p-2">
            <div className="flex items-center justify-between p-4">
              <h2 className="text-xl font-light text-cyan-400 flex items-center">
                <button className="bg-cyan-300 hover:bg-cyan-400 text-white font-bold py-2 px-4 rounded">
                  <FontAwesomeIcon icon={faPlus} />
                </button>
                <span className="ml-2">User Permission</span>
              </h2>
              <div className="relative">
                <input
                  type="text"
                  className="w-40 px-3 py-2 border rounded-md focus:outline-none focus:ring-cyan-400 focus:border-cyan-400"
                  placeholder="Search"
                  value={searchTerm}
                  onChange={handleSearchChange}
                />
                <svg
                  className="absolute right-3 top-1/2 transform -translate-y-1/2 h-4 w-4 text-gray-500"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                </svg>
              </div>
            </div>
            <div className="overflow-x-auto bg-slate-100 shadow-lg p-4 m-4 rounded-xl">
              <div className="bg-white p-4 rounded-lg">
                <table className="w-full table-auto">
                  <thead>
                    <tr className="bg-gray-200">
                      <th className="px-4 py-3 text-left text-sm font-semibold text-gray-800">User ID</th>
                      <th className="px-4 py-3 text-left text-sm font-semibold text-gray-800">Name</th>
                      <th className="px-4 py-3 text-left text-sm font-semibold text-gray-800">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {filteredUsers.map((user) => (
                      <tr key={user.userId} className="border-b border-gray-200">
                        <td className="px-4 py-3 text-left text-sm">{user.userId}</td>
                        <td className="px-4 py-3 text-left text-sm">
                          {user.name}
                        </td>
                        <td className="px-4 py-3 text-left text-sm">
                          <button
                            className="inline-block px-2 py-1 rounded-xl text-lg text-slate-500"
                            onClick={() => handleEditUser(user.userId)}
                          >
                            <FontAwesomeIcon icon={faEdit} className="mr-2" />
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <EditModal
          isOpen={isModalOpen}
          onClose={handleCancelEdit}
          onSave={handleSaveEdit}
          userId={editedUserId}
          userName={editedUserName}
          onInputChange={handleInputChange}
          onUserIdChange={handleUserIdChange}
        />
      </div>
    </div>
  );
};

export default UserPermission;
