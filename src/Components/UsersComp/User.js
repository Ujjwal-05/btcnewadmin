import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

function User() {
  const [users, setUsers] = useState([
    { userId: 406097, name: 'Archish Aggarwal', email: '', phone: '+919871397755', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406095, name: 'Aishwarya Sonthalia', email: '', phone: '+919051936357', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406091, name: 'Shubham Mane', email: '', phone: '+919145935843', loyaltyPoints: 0, status: 'Inactive', action: '', deleted: '' },
    { userId: 406089, name: 'Priyanshu Ekaksha', email: '', phone: '+918800689732', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406085, name: 'Rupam Das', email: '', phone: '+918240826317', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406083, name: 'Suvankar Ghosh', email: '', phone: '+919769801854', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406073, name: 'Sarthak Singh', email: 'flamesprit@gmail.com', phone: '+919990511292', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406072, name: 'Aarohi Agrawal', email: '', phone: '+919691018588', loyaltyPoints: 0, status: 'Active', action: '', deleted: '' },
    { userId: 406069, name: 'Shivam Kothari', email: '', phone: '+919168493991', loyaltyPoints: 0, status: 'Inactive', action: '', deleted: '' },
  ]);

  const [editUser, setEditUser] = useState(null); // State to track the user being edited

  const handleStatusChange = (userId) => {
    setUsers(users.map((user) =>
      user.userId === userId ? { ...user, status: user.status === 'Active' ? 'Inactive' : 'Active' } : user
    ));
  };

  const handleEditUser = (user) => {
    setEditUser(user); // Set the user to be edited when clicking on the edit icon
  };

  const handleCloseModal = () => {
    setEditUser(null); // Close the modal by resetting editUser state
  };

  const handleSaveChanges = () => {
    // Logic to save changes goes here
    handleCloseModal(); // Close the modal after saving changes
  };

  return (
    <div className="flex">
      <div className="min-h-screen p-4 w-full">
        <div className="bg-white rounded-lg shadow-lg overflow-hidden p-4">
          <div className="flex items-center justify-between p-4">
            <h2 className="text-xl font-light text-cyan-400 flex items-center">
              <button className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded">+</button>
              <span className="p-4 text-2xl font-light">Users</span>
            </h2>
          </div>
          <div className="bg-slate-100 p-6 m-4 rounded-lg">
            <table className="w-full text-left table-auto bg-white rounded-xl p-2">
              <thead className="border-b-2">
                <tr>
                  <th className="px-6 py-3 text-lg font-normal text-gray-800">User ID</th>
                  <th className="px-6 py-3 text-lg font-normal text-gray-800">Name</th>
                  <th className="px-6 py-3 text-lg font-normal text-gray-800">Email</th>
                  <th className="px-6 py-3 text-lg font-normal text-gray-800">Phone</th>
                  <th className="px-6 py-3 text-lg font-normal text-gray-800">Loyalty Points</th>
                  <th className="px-6 py-3 text-lg font-normal text-gray-800">Status</th>
                  <th className="px-4 py-3 text-lg font-normal text-gray-800">Action</th>
                  <th className="px-4 py-3 text-lg font-normal text-gray-800">Deleted</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user) => (
                  <tr key={user.userId} className="bg-white border-b border-gray-200 hover:bg-gray-100">
                    <td className="px-6 py-4 font-light text-sm">{user.userId}</td>
                    <td className="px-6 py-4 font-light text-sm">{user.name}</td>
                    <td className="px-6 py-4 font-light text-sm">{user.email}</td>
                    <td className="px-6 py-4 font-light text-sm">{user.phone}</td>
                    <td className="px-6 py-4 font-light text-sm">{user.loyaltyPoints}</td>
                    <td className="px-6 py-4 font-light text-sm">
                      <button
                        onClick={() => handleStatusChange(user.userId)}
                        className={`inline-block px-3.5 py-2 rounded-full text-sm font-normal ${
                          user.status === 'Active' ? 'bg-cyan-500 text-gray-100' : 'bg-gray-300 text-gray-800'
                        }`}
                      >
                        {user.status}
                      </button>
                    </td>
                    <td className="px-4 py-3 text-sm">
                      <FontAwesomeIcon
                        icon={faEdit}
                        className="h-5 w-5 text-gray-400 cursor-pointer hover:text-gray-600"
                        onClick={() => handleEditUser(user)}
                      />
                    </td>
                    <td className="px-4 py-3 text-sm">
                      {user.deleted ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-5 w-5 text-red-500"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                        >
                          <path
                            fillRule="evenodd"
                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a1 1 0 001 1h12a1 1 0 001-1V6a1 1 0 00-1-1h-3.382l-.724-1.447A1 1 0 009 2zm3 16a1 1 0 001 1v2a1 1 0 001-1v-2a1 1 0 00-1-1zm-6 0a1 1 0 001 1v2a1 1 0 001-1v-2a1 1 0 00-1-1z"
                            clipRule="evenodd"
                          />
                        </svg>
                      ) : (
                        ''
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

        {/* Edit Modal */}
        {editUser && (
          <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
            <div className="bg-white rounded-lg shadow-lg overflow-hidden w-1/3">
              <div className="p-4">
                <h2 className="text-xl p-2 font-normal mb-4 text-cyan-500">Edit User</h2>
                {/* Form fields for editing user */}
                <div className="flex flex-col space-y-4">
                  <label htmlFor="editUserName" className="text-sm text-black font-medium">Name</label>
                  <input
                    type="text"
                    id="editUserName"
                    value={editUser.name}
                    onChange={(e) => setEditUser({ ...editUser, name: e.target.value })}
                    className="border border-gray-300 focus:outline-cyan-500 rounded-md p-2"
                  />
                  <label htmlFor="editUserEmail" className="text-sm text-black font-medium">Email</label>
                  <input
                    type="email"
                    id="editUserEmail"
                    value={editUser.email}
                    onChange={(e) => setEditUser({ ...editUser, email: e.target.value })}
                    className="border border-gray-300 focus:outline-cyan-500 rounded-md p-2"
                  />
                  <label htmlFor="editUserPhone" className="text-sm text-black font-medium">Phone</label>
                  <input
                    type="text"
                    id="editUserPhone"
                    value={editUser.phone}
                    onChange={(e) => setEditUser({ ...editUser, phone: e.target.value })}
                    className="border border-gray-300 focus:outline-cyan-500 rounded-md p-2"
                  />
                </div>
                <div className="flex justify-end mt-4">
                  <button
                    onClick={handleCloseModal}
                    className="bg-gray-500 text-white px-4 py-2 rounded-md mr-2"
                  >
                    Cancel
                  </button>
                  <button
                    onClick={handleSaveChanges}
                    className="bg-cyan-500 text-white px-4 py-2 rounded-md"
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default User;
