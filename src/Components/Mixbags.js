import React, { useState, useEffect } from 'react';
import Switch from 'react-switch'; // Import the Switch component
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import FontAwesomeIcon
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons"; // Import icons

const MixBags = ({
  data,
  buttonText = 'Add Mix Bag',
  titleText = 'Mix Bags',
  toggleActive,
  showStatusColumn = true,
  showDeleteIcon = true,
  showEditColumn = false,
}) => {
  const [bags, setBags] = useState(data);
  const [selectedBag, setSelectedBag] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    setBags(data);
  }, [data]);

  const handleAddMixBag = () => {
    const newId = bags.length + 1;
    const newBagData = { id: newId, name: 'New Bag', active: true };
    setBags([...bags, newBagData]);
    setSelectedBag(newBagData); // Set the new bag as selected
    setIsModalOpen(true); // Open the modal immediately
  };

  const handleDeleteBag = (id) => {
    const updatedBags = bags.filter((bag) => bag.id !== id);
    setBags(updatedBags);
  };

  const handleEditBag = (bag) => {
    setSelectedBag(bag);
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setSelectedBag(null);
    setIsModalOpen(false);
  };

  const handleSaveBag = () => {
    const updatedBags = bags.map((bag) => {
      if (bag.id === selectedBag.id) {
        return selectedBag;
      }
      return bag;
    });
    setBags(updatedBags);
    handleCloseModal();
  };

  const toggleActiveStatus = (id, checked) => {
    const updatedData = bags.map((item) => {
      if (item.id === id) {
        return { ...item, active: checked };
      }
      return item;
    });
    setBags(updatedData);
  };

  const MixBag = ({ id, name, active }) => (
    <tr className="border-b font-light">
      <td className="px-4 py-2">{id}</td>
      <td className="px-4 py-2">{name}</td>
      <td className="px-4 py-2"></td>
      {showStatusColumn && (
        <td className="px-4 py-3">
          <div className="flex items-center">
            <Switch
              id={`switch${id}`}
              checked={active}
              onChange={(checked) => toggleActiveStatus(id, checked)}
              onColor="#66E8F9"
              onHandleColor="#FFFFFF"
              handleDiameter={20}
              uncheckedIcon={false}
              checkedIcon={false}
              height={12}
              width={32}
              className="react-switch"
            />
            <span className="ml-2 text-slate-700" style={{ minWidth: '70px' }}>
              {active ? 'Active' : 'Inactive'}
            </span>
          </div>
        </td>
      )}
      <td className="px-4 py-2">
        <div className="flex items-center">
          <FontAwesomeIcon
            icon={faEdit}
            className="cursor-pointer mr-2"
            onClick={() => handleEditBag({ id, name, active })}
          />
          {showDeleteIcon && (
            <FontAwesomeIcon
              icon={faTrash}
              className="cursor-pointer text-red-500 "
              onClick={() => handleDeleteBag(id)}
            />
          )}
        </div>       by7
      </td>
    </tr>
  );

  return (
    <div className="h-screen font-sans flex">
      <div className="bg-white p-4 rounded-xl shadow-md flex-grow m-4">
        <div className="flex justify-between items-center mb-4">
          <div className="flex items-center">
            <button
              className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded"
              onClick={handleAddMixBag}
            >
              +
            </button>
            <h2 className="text-2xl font-light text-cyan-600 p-1">{titleText}</h2>
          </div>
          <button
            onClick={handleAddMixBag}
            className="bg-cyan-500 hover:bg-cyan-600 text-white text-base font-medium py-2 px-4 rounded"
          >
            {buttonText}
          </button>
        </div>
        <div className="bg-slate-100 rounded-xl p-4">
          <div className="bg-white p-4 rounded-xl">
            <table className="w-full">
              <thead className="border-b-2">
                <tr>
                  <th className="px-5 py-3 font-medium text-left">Id</th>
                  <th className="px-5 py-3 font-medium text-left">Name</th>
                  <th className="px-5 py-3 font-medium text-left">Weight</th>
                  {showStatusColumn && (
                    <th className="px-4 py-2 font-medium text-left">Status</th>
                  )}
                  <th className="px-4 py-2 text-left">Action</th>
                </tr>
              </thead>
              <tbody>
                {bags.map((bag) => (
                  <MixBag
                    key={bag.id}
                    id={bag.id}
                    name={bag.name}
                    active={bag.active}
                  />
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="bg-white p-4 rounded-xl shadow-md max-w-md w-full">
            <h2 className="text-xl font-medium text-cyan-500 mb-4">Edit Mix Bag</h2>
            <div className="mb-4">
              <label htmlFor="editName" className="block text-gray-700 font-bold mb-2">
                Name:
              </label>
              <input
                type="text"
                id="editName"
                value={selectedBag?.name || ''}
                onChange={(e) =>
                  setSelectedBag({ ...selectedBag, name: e.target.value })
                }
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-cyan-500"
              />
            </div>
            {showStatusColumn && (
              <div className="mb-4">
                <label htmlFor="editActive" className="block text-gray-700 font-bold mb-2">
                  Status:
                </label>
                <Switch
                  id="editActive"
                  checked={selectedBag?.active || false}
                  onChange={(checked) =>
                    setSelectedBag({ ...selectedBag, active: checked })
                  }
                  onColor="#66E8F9"
                  onHandleColor="#FFFFFF"
                  handleDiameter={20}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={12}
                  width={32}
                  className="react-switch"
                />
                <span className="ml-2 text-slate-700" style={{ minWidth: '70px' }}>
                  {selectedBag?.active ? 'Active' : 'Inactive'}
                </span>
              </div>
            )}
            <div className="flex justify-end">
              <button
                onClick={handleCloseModal}
                className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
              >
                Cancel
              </button>
              <button
                onClick={handleSaveBag}
                className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
              >
                Save
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default MixBags;
  