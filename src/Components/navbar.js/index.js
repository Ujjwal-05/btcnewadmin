import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons'; // Import the hamburger icon

function Navbar({ toggleSidebar }) {
  return (
    <nav className=" flex justify-between items-center">
      <button 
        onClick={toggleSidebar} 
        className="text-white rounded-full ml-2 mt-2 transition-colors duration-200"
      >
        <FontAwesomeIcon icon={faBars} />
      </button>
    </nav>
  );
}

export default Navbar;
 