import React, { useState } from 'react';

const TotalPaymentCollection = () => {
  const [startDate, setStartDate] = useState('2024-07-04');
  const [endDate, setEndDate] = useState('2024-07-09');

  const transactions = [
    { date: '2024-07-04', razorpay: 232031, paytm: 0, blueTokaiCard: 0, other: 0, total: 232031 },
    { date: '2024-07-05', razorpay: 228856, paytm: 0, blueTokaiCard: 0, other: 0, total: 228856 },
    { date: '2024-07-06', razorpay: 285038, paytm: 0, blueTokaiCard: 704, other: 0, total: 285742 },
    { date: '2024-07-07', razorpay: 242259, paytm: 0, blueTokaiCard: 490, other: 0, total: 242749 },
    { date: '2024-07-08', razorpay: 211770, paytm: 0, blueTokaiCard: 830, other: 0, total: 212600 },
    { date: '2024-07-09', razorpay: 204401, paytm: 0, blueTokaiCard: 556, other: 0, total: 204957 },
  ];

  const [filteredTransactions, setFilteredTransactions] = useState(transactions);

  const handleSearch = () => {
    const filtered = transactions.filter(
      (transaction) =>
        new Date(transaction.date) >= new Date(startDate) &&
        new Date(transaction.date) <= new Date(endDate)
    );
    setFilteredTransactions(filtered);
  };

  return (
    <div className="bg-slate-100 p-2 rounded-lg shadow-lg mt-16 mr-4 ml-4 ">
    <div className="bg-white rounded-lg shadow-lg m-2">
      <div className="flex justify-between items-center mb-6 p-4">
        <div className="w-1/3">
          <label htmlFor="start-date" className="block text-sm font-medium text-gray-700">
            Start Date
          </label>
          <input
            type="date"
            id="start-date"
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
            className="mt-1 block w-22 p-2 rounded-md border-gray-300 shadow-sm focus:outline-cyan-500"
          />
        </div>
        <div className="w-1/3">
          <label htmlFor="end-date" className="block text-sm font-medium text-gray-700">
            End Date
          </label>
          <input
            type="date"
            id="end-date"
            value={endDate}
            onChange={(e) => setEndDate(e.target.value)}
            className="mt-1 block w-22 p-2 rounded-md border-gray-300 shadow-sm focus:outline-cyan-500"
          />
        </div>
        <button
          onClick={handleSearch}
          className="ml-4 bg-cyan-500 hover:bg-cyan-700 text-white font-light py-2 px-4 rounded focus:outline-cyan-500"
        >
          Search
        </button>
      </div>

      <table className="w-full table-auto">
        <thead>
          <tr className="bg-gray-100 text-gray-600 uppercase text-xs leading-normal">
            <th className="py-3 px-6 text-left">Date</th>
            <th className="py-3 px-6 text-left">Razorpay</th>
            <th className="py-3 px-6 text-left">Paytm</th>
            <th className="py-3 px-6 text-left">Blue Tokai Card</th>
            <th className="py-3 px-6 text-left">Other</th>
            <th className="py-3 px-6 text-left">Total</th>
          </tr>
        </thead>
        <tbody className="text-gray-600 text-sm font-light">
          {filteredTransactions.map((transaction) => (
            <tr key={transaction.date} className="border-b border-gray-200 hover:bg-gray-50">
              <td className="py-3 px-6 text-left">{transaction.date}</td>
              <td className="py-3 px-6 text-left">{transaction.razorpay}</td>
              <td className="py-3 px-6 text-left">{transaction.paytm}</td>
              <td className="py-3 px-6 text-left">{transaction.blueTokaiCard}</td>
              <td className="py-3 px-6 text-left">{transaction.other}</td>
              <td className="py-3 px-6 text-left">{transaction.total}</td>
            </tr>
          ))}

          <tr className="bg-gray-100 text-gray-600 uppercase text-xs leading-normal">
            <th className="py-3 px-6 text-left">Grand Total</th>
            <th className="py-3 px-6 text-left">
              {filteredTransactions.reduce((sum, transaction) => sum + transaction.razorpay, 0)}
            </th>
            <th className="py-3 px-6 text-left">
              {filteredTransactions.reduce((sum, transaction) => sum + transaction.paytm, 0)}
            </th>
            <th className="py-3 px-6 text-left">
              {filteredTransactions.reduce((sum, transaction) => sum + transaction.blueTokaiCard, 0)}
            </th>
            <th className="py-3 px-6 text-left">
              {filteredTransactions.reduce((sum, transaction) => sum + transaction.other, 0)}
            </th>
            <th className="py-3 px-6 text-left">
              {filteredTransactions.reduce((sum, transaction) => sum + transaction.total, 0)}
            </th>
          </tr>
        </tbody>
      </table>
    </div>
    </div>
  );
};

export default TotalPaymentCollection;
