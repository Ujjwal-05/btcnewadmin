// import React, { useState } from 'react';
// import Switch from 'react-switch';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
// import BTCAppMainCategory from './ProductComp/BTCAppMainCategory';
// import BTCAppSubCategory from './ProductComp/BTCAppSubCategory';
// import ProductCollection from './ProductComp/ProductCollection';
// import ProductGrouping from './ProductComp/ProductGrouping';
// import SyncProducts from './ProductComp/SyncProducts';

// const Product = () => {
//     const [categories, setCategories] = useState([
//         {
//             name: 'Brewed Coffee',
//             subCategories: 2,
//             products: 28,
//             status: 'disabled',
//         },
//         {
//             name: 'Newly Launched',
//             subCategories: 2,
//             products: 9,
//             status: 'disabled',
//         },
//         {
//             name: 'Food',
//             subCategories: 13,
//             products: 38,
//             status: 'enabled',
//         },
//         {
//             name: 'Juices & Smoothies',
//             subCategories: 2,
//             products: 10,
//             status: 'enabled',
//         },
//         {
//             name: 'Suchali\'s Artisan',
//             subCategories: 7,
//             products: 24,
//             status: 'enabled',
//         },
//         {
//             name: 'Other Beverages',
//             subCategories: 3,
//             products: 5,
//             status: 'enabled',
//         },
//         {
//             name: 'Roasted Coffee',
//             subCategories: 4,
//             products: 19,
//             status: 'enabled',
//         },
//     ]);

//     const [searchTerm, setSearchTerm] = useState('');

//     const handleSearch = (e) => {
//         setSearchTerm(e.target.value);
//     };

//     const handleStatusChange = (index) => {
//         setCategories((prevCategories) => {
//             const updatedCategories = [...prevCategories];
//             updatedCategories[index].status =
//                 updatedCategories[index].status === 'enabled' ? 'disabled' : 'enabled';
//             return updatedCategories;
//         });
//     };

//     const handleDeleteCategory = (index) => {
//         setCategories((prevCategories) => {
//             const updatedCategories = [...prevCategories];
//             updatedCategories.splice(index, 1);
//             return updatedCategories;
//         });
//     };

//     const filteredCategories = categories.filter((category) =>
//         category.name.toLowerCase().includes(searchTerm.toLowerCase())
//     );

//     const buttons = [
//         { id: 1, name: 'Main Category' },
//         { id: 2, name: 'Sub-category' },
//         { id: 3, name: 'Product Grouping' },
//         { id: 4, name: 'Sync Products' },
//         { id: 5, name: 'Product Collection' },
//     ];

//     return (
//         <div className="p-6 bg-white shadow rounded-lg m-4 max-w-full">
//             <h1 className="text-2xl font-light mb-6 text-cyan-500">Manage Products</h1>
//             <div className="mb-6 p-4 flex items-center justify-between rounded-lg shadow-md bg-slate-200">
//                 <div className="flex space-x-6">
//                     {buttons.map((button) => (
//                         <button key={button.id} className={button.id === 1 ? '' : 'text-base font-light text-gray-700'}>
//                             {button.name}
//                         </button>
//                     ))}
//                 </div>
//                 <button className="bg-cyan-500 hover:bg-cyan-600 text-white font-light py-2 px-4 rounded-lg">
//                     + Add Category
//                 </button>
//             </div>
//             <BTCAppMainCategory />
//             <BTCAppSubCategory />
//             <ProductCollection />
//             <ProductGrouping />
//             <SyncProducts />

//         </div>
//     );
// };

// export default Product;

import React, { useState } from 'react';
import Switch from 'react-switch';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import BTCAppMainCategory from './ProductComp/BTCAppMainCategory';
import BTCAppSubCategory from './ProductComp/BTCAppSubCategory';
import ProductCollection from './ProductComp/ProductCollection';
import ProductGrouping from './ProductComp/ProductGrouping';
import SyncProducts from './ProductComp/SyncProducts';

const Product = () => {
    const [categories, setCategories] = useState([
        {
            name: 'Brewed Coffee',
            subCategories: 2,
            products: 28,
            status: 'disabled',
        },
        {
            name: 'Newly Launched',
            subCategories: 2,
            products: 9,
            status: 'disabled',
        },
        {
            name: 'Food',
            subCategories: 13,
            products: 38,
            status: 'enabled',
        },
        {
            name: 'Juices & Smoothies',
            subCategories: 2,
            products: 10,
            status: 'enabled',
        },
        {
            name: 'Suchali\'s Artisan',
            subCategories: 7,
            products: 24,
            status: 'enabled',
        },
        {
            name: 'Other Beverages',
            subCategories: 3,
            products: 5,
            status: 'enabled',
        },
        {
            name: 'Roasted Coffee',
            subCategories: 4,
            products: 19,
            status: 'enabled',
        },
    ]);

    const [searchTerm, setSearchTerm] = useState('');
    const [selectedIndex, setSelectedIndex] = useState(0);

    const handleSearch = (e) => {
        setSearchTerm(e.target.value);
    };

    const handleStatusChange = (index) => {
        setCategories((prevCategories) => {
            const updatedCategories = [...prevCategories];
            updatedCategories[index].status =
                updatedCategories[index].status === 'enabled' ? 'disabled' : 'enabled';
            return updatedCategories;
        });
    };

    const handleDeleteCategory = (index) => {
        setCategories((prevCategories) => {
            const updatedCategories = [...prevCategories];
            updatedCategories.splice(index, 1);
            return updatedCategories;
        });
    };

    const filteredCategories = categories.filter((category) =>
        category.name.toLowerCase().includes(searchTerm.toLowerCase())
    );

    const buttons = [
        { id: 0, name: 'Main Category' },
        { id: 1, name: 'Sub-category' },
        { id: 2, name: 'Product Grouping' },
        { id: 3, name: 'Sync Products' },
        { id: 4, name: 'Product Collection' },
    ];

    const renderComponent = () => {
        switch (selectedIndex) {
            case 0:
                return <BTCAppMainCategory />;
            case 1:
                return <BTCAppSubCategory />;
            case 2:
                return <ProductGrouping />;
            case 3:
                return <SyncProducts />;
            case 4:
                return <ProductCollection />;
            default:
                return null;
        }
    };

    return (
        <div className="p-6 bg-white shadow rounded-lg m-4 max-w-full">
            <h1 className="text-2xl font-light mb-6 text-cyan-500">Manage Products</h1>
            <div className="mb-6 p-4 flex items-center justify-between rounded-lg shadow-md bg-slate-200">
                <div className="flex space-x-6">
                    {buttons.map((button, index) => (
                        <button
                            key={button.id}
                            onClick={() => setSelectedIndex(index)}
                            className={selectedIndex === index ? 'text-base font-bold text-gray-900' : 'text-base font-light text-gray-700'}
                        >
                            {button.name}
                        </button>
                    ))}
                </div>
               
            </div>
            {renderComponent()}
        </div>
    );
};

export default Product;

