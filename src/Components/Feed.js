import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';


const Feed = () => {
    const initialItems = [
        { id: 1, title: 'Discover Products', type: 'Product', pinned: true, status: 'Inactive' },
        { id: 2, title: 'ESPRESSO BREWING AND LATTE ART WORKSHOP', type: 'Event', pinned: false, status: 'Inactive' },
        { id: 3, title: 'Attikan Estate', type: 'Farm', pinned: false, status: 'Inactive' },
        { id: 4, title: 'Bibi Plantation', type: 'Farm', pinned: false, status: 'Inactive' },
        { id: 5, title: 'Marvahulla Estate', type: 'Farm', pinned: false, status: 'Inactive' },
        { id: 6, title: 'Thogarihunkal Estate', type: 'Farm', pinned: false, status: 'Active' },
        { id: 7, title: 'Ratnagiri Estate', type: 'Farm', pinned: false, status: 'Active' },
        { id: 8, title: 'Padalo Estate', type: 'Farm', pinned: false, status: 'Active' },
    ];

    const [items, setItems] = useState(initialItems);
    const [filteredItems, setFilteredItems] = useState(initialItems); // State to manage filtered items
    const [searchQuery, setSearchQuery] = useState('');
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [currentEditIndex, setCurrentEditIndex] = useState(null);
    const [editFormData, setEditFormData] = useState({ id: null, title: '', type: '', pinned: false, status: '' });

    // Filter items based on search query
    useEffect(() => {
        if (searchQuery.trim() === '') {
            setFilteredItems(items); // Show all items when search is empty
        } else {
            const lowercasedQuery = searchQuery.toLowerCase();
            const filteredData = items.filter(item =>
                item.title.toLowerCase().includes(lowercasedQuery)
            );
            setFilteredItems(filteredData);
        }
    }, [searchQuery, items]);

    // Function to handle opening the edit modal
    const handleEdit = (index) => {
        setCurrentEditIndex(index);
        setEditFormData(items[index]);
        setIsEditModalOpen(true);
    };

    // Function to handle changes in the edit form fields
    const handleEditChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        setEditFormData({ ...editFormData, [name]: newValue });
    };

    // Function to handle submission of the edit form
    const handleEditSubmit = (e) => {
        e.preventDefault();
        const updatedItems = [...items];
        updatedItems[currentEditIndex] = editFormData;
        setItems(updatedItems);
        setIsEditModalOpen(false);
    };

    // Function to handle status change (toggle between Active and Inactive)
    const handleStatusChange = (id, newStatus) => {
        const updatedItems = items.map(item =>
            item.id === id ? { ...item, status: newStatus } : item
        );
        setItems(updatedItems);
    };

    return (
        <div className="bg-cyan-400 p-4 min-h-screen">
        <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10">
          <div className="flex justify-between p-4">
                    <h1 className="text-2xl font-medium text-cyan-600 flex mr-4 items-center">
                        <button className="bg-cyan-300 hover:bg-cyan-400 text-white font-light py-2 px-4 rounded mr-6">
                            +
                        </button>
                        Feed
                    </h1>
                    <div className="relative">
                        <input
                            type="text"
                            className="rounded-md border border-gray-300 py-2 px-3 m-2 focus:outline-none"
                            placeholder="Search"
                            value={searchQuery}
                            onChange={(e) => setSearchQuery(e.target.value)}
                        />
                    </div>
                </div>
                <div className="bg-slate-200 p-4 m-4 rounded-lg">
                     <div className="relative">
                    
                    <div className="bg-white">
                    <table className="w-full table-auto">
                        <thead className="border-b-2 bg-rounded bg-white">
                            <tr className="bg-white text-gray-700 font-medium">
                                <th className="px-6 py-4 text-left">Id</th>
                                <th className="px-6 py-4 text-left">Title</th>
                                <th className="px-6 py-4 text-left">Type</th>
                                <th className="px-6 py-4 text-left">Pinned</th>
                                <th className="px-6 py-4 text-left">Status</th>
                                <th className="px-6 py-4 text-left">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredItems.map((item, index) => (
                                <tr key={item.id} className="hover:bg-gray-100 border-b border-gray-200">
                                    <td className="px-6 py-4 font-light whitespace-nowrap">{item.id}</td>
                                    <td className="px-6 py-4 font-light whitespace-nowrap">{item.title}</td>
                                    <td className="px-6 py-4 font-light whitespace-nowrap">{item.type}</td>
                                    <td className="px-6 py-4 font-light whitespace-nowrap">{item.pinned ? 'Yes' : 'No'}</td>
                                    <td className="px-6 py-4 font-light whitespace-nowrap">
                                        <button
                                            className={`px-3 py-2 text-sm rounded-full ${item.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-300 text-gray-700'}`}
                                            onClick={() => handleStatusChange(item.id, item.status === 'Active' ? 'Inactive' : 'Active')}
                                        >
                                            {item.status}
                                        </button>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        <button onClick={() => handleEdit(index)} className="text-gray-600 hover:text-gray-900">
                                        <FontAwesomeIcon icon={faEdit} className="mr-2" />
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>

            {isEditModalOpen && (
                <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-75">
                    <div className="bg-white p-6 rounded-lg shadow-lg w-1/3">
                        <h2 className="text-lg font-semibold text-gray-900 mb-4">Edit Item</h2>
                        <form onSubmit={handleEditSubmit}>
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="editTitle">
                                    Title
                                </label>
                                <input
                                    id="editTitle"
                                    name="title"
                                    type="text"
                                    value={editFormData.title}
                                    onChange={handleEditChange}
                                    className="w-full px-3 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-cyan-500"
                                />
                            </div>
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="editType">
                                    Type
                                </label>
                                <input
                                    id="editType"
                                    name="type"
                                    type="text"
                                    value={editFormData.type}
                                    onChange={handleEditChange}
                                    className="w-full px-3 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-cyan-500"
                                />
                            </div>
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="editPinned">
                                    Pinned
                                </label>
                                <input
                                    id="editPinned"
                                    name="pinned"
                                    type="checkbox"
                                    checked={editFormData.pinned}
                                    onChange={handleEditChange}
                                    className="form-checkbox h-5 w-5 text-cyan-500"
                                />
                            </div>
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="editStatus">
                                    Status
                                </label>
                                <select
                                    id="editStatus"
                                    name="status"
                                    value={editFormData.status}
                                    onChange={handleEditChange}
                                    className="w-full px-3 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-cyan-500"
                                >
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                </select>
                            </div>
                            <div className="flex justify-end">
                                <button
                                    type="button"
                                    onClick={() => setIsEditModalOpen(false)}
                                    className="bg-gray-500 hover:bg-gray-600 text-white font-bold py-2 px-4 rounded mr-2"
                                >
                                    Cancel
                                </button>
                                <button
                                    type="submit"
                                    className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                                >
                                    Save Changes
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Feed;
