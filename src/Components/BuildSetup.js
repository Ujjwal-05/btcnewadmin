import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

function BuildSetup() {
  const [builds, setBuilds] = useState([
    { platform: 'Android', build: '1.0.4', type: 'Force', action: 'Edit' },
    { platform: 'iOS', build: '1.0.4', type: 'Force', action: 'Edit' },
    { platform: 'Android', build: '1.0.5', type: 'Force', action: 'Edit' },
    { platform: 'iOS', build: '1.0.5', type: 'Force', action: 'Edit' },
    { platform: 'iOS', build: '200', type: 'Force', action: 'Edit' },
    { platform: 'Android', build: '200', type: 'Force', action: 'Edit' },
    { platform: 'Android', build: '201', type: 'Force', action: 'Edit' },
  ]);

  const [searchTerm, setSearchTerm] = useState('');
  const [selectedBuild, setSelectedBuild] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isSearchVisible, setIsSearchVisible] = useState(false);

  const handleEdit = (index) => {
    setSelectedBuild({ ...builds[index], index });
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setSelectedBuild(null);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setSelectedBuild((prev) => ({ ...prev, [name]: value }));
  };

  const handleSave = () => {
    const updatedBuilds = builds.map((build, index) =>
      index === selectedBuild.index ? selectedBuild : build
    );
    setBuilds(updatedBuilds);
    closeModal();
  };

  const filteredBuilds = builds.filter((build) =>
    build.platform.toLowerCase().includes(searchTerm.toLowerCase()) ||
    build.build.toLowerCase().includes(searchTerm.toLowerCase()) ||
    build.type.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="bg-gray-100 rounded-lg shadow-md p-6 mt-12 m-4 relative">
      <div className="bg-white rounded-lg flex justify-between items-center p-4 mt-2">
        <h2 className="text-2xl font-normal rounded-lg text-cyan-500">Build Setup</h2>
        <div className="relative rounded-lg bg-white">
          {isSearchVisible && (
            <input
              type="text"
              placeholder="Search..."
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
              className="absolute right-0 mb-1 mr-10 p-1 block w-60 rounded-md border-gray-500 shadow-slate-400 focus:outline-cyan-300 z-10"
            />
          )}
          <svg
            className="w-4 h-4 mr-2"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"
            />
          </svg>
        </div>
      </div>
      <table className="w-full text-sm text-left rounded-lg text-gray-500">
        <thead className="text-sms text-gray-700 bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Platform
            </th>
            <th scope="col" className="px-6 py-3">
              Build
            </th>
            <th scope="col" className="px-6 py-3">
              Type
            </th>
            <th scope="col" className="px-6 py-3">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
          {filteredBuilds.map((build, index) => (
            <tr key={index} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-100">
              <td className="px-6 py-4">{build.platform}</td>
              <td className="px-6 py-4">{build.build}</td>
              <td className="px-6 py-4">{build.type}</td>
              <td className="px-6 py-4">
                <button className="p-2"
                 onClick={() => handleEdit(index)}>
                <FontAwesomeIcon icon={faEdit} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {isModalOpen && selectedBuild && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="bg-white p-6 rounded-lg max-w-lg w-full">
            <h2 className="text-2xl font-normal text-cyan-500 mb-4">Edit Build</h2>
            <div className="grid gap-4">
              <div>
                <label htmlFor="platform" className="block text-xl font-light text-gray-700">Platform</label>
                <select
                  name="platform"
                  id="platform"
                  value={selectedBuild.platform}
                  onChange={handleInputChange}
                  className="mt-1 block w-full rounded-md border-gray-300 text-gray-500 shadow-sm focus:outline-cyan-300"
                >
                  <option value="Android">Android</option>
                  <option value="iOS">iOS</option>
                </select>
              </div>
              <div>
                <label htmlFor="build" className="block text-xl font-light text-gray-700">Build</label>
                <input
                  type="text"
                  name="build"
                  id="build"
                  value={selectedBuild.build}
                  onChange={handleInputChange}
                  className="mt-1 block w-full rounded-md border-gray-300 text-gray-500 shadow-sm focus:outline-cyan-300"
                />
              </div>
              <div>
                <label htmlFor="type" className="block text-xl font-light text-gray-700">Type</label>
                <select
                  name="type"
                  id="type"
                  value={selectedBuild.type}
                  onChange={handleInputChange}
                  className="mt-1 block w-full rounded-md border-gray-300 shadow-sm text-gray-500 focus:outline-cyan-300"
                >
                  <option value="Force">Force</option>
                  <option value="Flexible">Flexible</option>
                </select>
              </div>
            </div>
            <div className="mt-6 flex justify-end">
              <button
                onClick={closeModal}
                className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-medium py-2 px-4 rounded-lg mr-2"
              >
                Cancel
              </button>
              <button
                onClick={handleSave}
                className="bg-cyan-400 hover:bg-cyan-500 text-white font-medium py-2 px-4 rounded-lg"
              >
                Save
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default BuildSetup;
