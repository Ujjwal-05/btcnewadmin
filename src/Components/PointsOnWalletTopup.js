import React, { useState, useEffect } from 'react';

const PointsOnWalletTopup = () => {
  const initialData = [
    { id: 1, title: '1001 to 5000', slab: '1001-5000', points: '10 (%)', status: 'Inactive' },
    { id: 2, title: '501 to 1000', slab: '501-1000', points: '20 (%)', status: 'Active' },
    { id: 3, title: 'Upto 500', slab: '1-500', points: '10 (%)', status: 'Inactive' },
  ];

  const [data, setData] = useState(initialData);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage, setRecordsPerPage] = useState(10); // Changed default records per page to 10 for testing

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentRecord, setCurrentRecord] = useState(null);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleRecordsPerPageChange = (event) => {
    setRecordsPerPage(parseInt(event.target.value));
  };

  const handleEditClick = (record) => {
    setCurrentRecord(record);
    setIsEditModalOpen(true);
  };

  const handleSaveRecord = (updatedRecord) => {
    setData((prevData) =>
      prevData.map((record) =>
        record.id === updatedRecord.id ? updatedRecord : record
      )
    );
    setIsEditModalOpen(false);
  };

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = data.slice(indexOfFirstRecord, indexOfLastRecord);

  return (
    <div className="bg-white rounded-lg p-6 ml-4 mr-6 mt-16">
      <div className="flex justify-between items-center">
        <h2 className="text-2xl font-normal text-cyan-500">Loyalty Points On Wallet</h2>
        <button className="bg-cyan-100 pl-4 pr-4 pt-2.5 pb-2.5 align-left rounded-md shadow-md">+</button>
      </div>
      <div className="mt-4 bg-slate-100 rounded-lg p-4">
        <div className="bg-white rounded-lg p-2">
          <table className="w-full text-left table-auto">
            <thead className="border-b pb-2">
              <tr>
                <th className="p-2">Title</th>
                <th className="p-2">Slab</th>
                <th className="p-2">Loyalty Points</th>
                <th className="p-2">Status</th>
                <th className="p-2">Action</th>
              </tr>
            </thead>
            <tbody>
              {currentRecords.map((item) => (
                <tr key={item.id} className="border-b text-slate-600 font-light text-sm hover:bg-gray-100">
                  <td className="p-3">{item.title}</td>
                  <td className="p-3">{item.slab}</td>
                  <td className="p-3">{item.points}</td>
                  <td className="p-3">
                    <button className={`p-2.5 text-sm rounded-3xl ${item.status === 'Active' ? 'bg-cyan-500 text-white' : 'bg-gray-300 text-black'}`}>
                      {item.status}
                    </button>
                  </td>
                  <td className="p-2">
                    <button className="" onClick={() => handleEditClick(item)}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-5 w-5"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          d="M17.414 2.586a2 2 0 0 0-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 0 0 0-2.828z"
                        />
                      </svg>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className="mt-4 flex justify-between items-center">
            <div>
              <label htmlFor="recordsPerPage">Record per page:</label>
              <select
                id="recordsPerPage"
                value={recordsPerPage}
                onChange={handleRecordsPerPageChange}
              >
                <option value={10}>10</option>
                <option value={25}>25</option>
                <option value={50}>50</option>
                <option value={100}>100</option>
              </select>
            </div>
            <div className="flex gap-2">
              <button
                onClick={() => handlePageChange(currentPage - 1)}
                disabled={currentPage === 1}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M12.707 5.293a1 1 0 010 1.414L5.414 10l7.293 7.293a1 1 0 01-1.414 1.414l-8-8a1 1 0 010-1.414l8-8a1 1 0 011.414 1.414z"
                  />
                </svg>
              </button>
              <button
                onClick={() => handlePageChange(currentPage + 1)}
                disabled={currentPage === Math.ceil(data.length / recordsPerPage)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 1.414z"
                  />
                </svg>
              </button>
            </div>
            <p className="text-sm">
              Showing {indexOfFirstRecord + 1} to {indexOfLastRecord} of {data.length} records
            </p>
          </div>
        </div>
      </div>
      <EditModal
        isOpen={isEditModalOpen}
        record={currentRecord}
        onClose={() => setIsEditModalOpen(false)}
        onSave={handleSaveRecord}
      />
    </div>
  );
};

const EditModal = ({ isOpen, record, onClose, onSave }) => {
  const [editedRecord, setEditedRecord] = useState({ ...record });

  useEffect(() => {
    setEditedRecord({ ...record });
  }, [record]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedRecord((prev) => ({ ...prev, [name]: value }));
  };

  const handleSave = () => {
    onSave(editedRecord);
    onClose();
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
      <div className="bg-white rounded-lg p-6 w-96">
        <h2 className="text-2xl font-medium text-cyan-500 mb-4">Edit Record</h2>
        <div className="mb-4">
          <label className="block text-sm">Title</label>
          <input
            type="text"
            name="title"
            value={editedRecord.title}
            onChange={handleInputChange}
            className="w-full p-2 border rounded focus:outline-cyan-300"
          />
        </div>
        <div className="mb-4">
          <label className="block text-sm">Slab</label>
          <input
            type="text"
            name="slab"
            value={editedRecord.slab}
            onChange={handleInputChange}
            className="w-full p-2 border rounded focus:outline-cyan-300"
          />
        </div>
        <div className="mb-4">
          <label className="block text-sm">Loyalty Points</label>
          <input
            type="text"
            name="points"
            value={editedRecord.points}
            onChange={handleInputChange}
            className="w-full p-2 border rounded focus:outline-cyan-300"
          />
        </div>
        <div className="mb-4">
          <label className="block text-sm">Status</label>
          <select
            name="status"
            value={editedRecord.status}
            onChange={handleInputChange}
            className="w-full p-2 border rounded focus:outline-cyan-300"
          >
            <option value="Active">Active</option>
            <option value="Inactive">Inactive</option>
          </select>
        </div>
        <div className="flex justify-end">
          <button
            onClick={handleSave}
            className="bg-cyan-500 text-white p-2 w-20 rounded-md mr-2"
          >
            Save
          </button>
          <button
            onClick={onClose}
            className="bg-gray-300 text-black p-2 w-20 rounded-md"
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default PointsOnWalletTopup;
