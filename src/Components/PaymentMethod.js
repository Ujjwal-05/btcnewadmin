import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCreditCard, faMoneyCheckAlt, faCoins, faMobileAlt, faEdit } from '@fortawesome/free-solid-svg-icons';

const iconMap = [faCreditCard, faMoneyCheckAlt, faCoins, faMobileAlt];

const PaymentMethodRow = ({ method, index, handleEdit, handleStatusChange }) => (
  <tr className="bg-white border-b dark:bg-gray-600 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-900">
    <td className="px-6 py-4">
      <FontAwesomeIcon icon={iconMap[index]} />
    </td>
    <td className="px-6 py-4 text-gray-600">{method.paymentType}</td>
    <td className="px-6 py-4 text-sm text-gray-600">{method.paymentMode}</td>
    <td className="px-6 py-4">
      <span
        className={`inline-block px-3 py-2 rounded-full text-sm font-normal ${method.status === 'Enable' ? 'bg-cyan-500 text-white' : 'bg-gray-200 text-gray-700'}`}
        onClick={() => handleStatusChange(index)}
      >
        {method.status}
      </span>
    </td>
    <td className="px-6 py-4">
      <button onClick={() => handleEdit(index)} className="text-gray-600 hover:text-gray-900">
        <FontAwesomeIcon icon={faEdit} className="mr-2" />
      </button>
    </td>
  </tr>
);

function PaymentMethod() {
  const [paymentMethods, setPaymentMethods] = useState([
    { paymentType: 'Razorpay', paymentMode: 'Card', status: 'Enable' },
    { paymentType: 'Razorpay', paymentMode: 'Netbanking', status: 'Disable' },
    { paymentType: 'Razorpay', paymentMode: 'BTC Wallet', status: 'Enable' },
    { paymentType: 'Razorpay', paymentMode: 'UPI', status: 'Enable' },
  ]);

  const [searchQuery, setSearchQuery] = useState('');
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [currentEditIndex, setCurrentEditIndex] = useState(null);
  const [editFormData, setEditFormData] = useState({ paymentType: '', paymentMode: '', status: '' });

  const handleStatusChange = (index) => {
    setPaymentMethods((prevMethods) => {
      const updatedMethods = [...prevMethods];
      updatedMethods[index].status = updatedMethods[index].status === 'Enable' ? 'Disable' : 'Enable';
      return updatedMethods;
    });
  };

  const handleEdit = (index) => {
    setCurrentEditIndex(index);
    setEditFormData(paymentMethods[index]);
    setIsEditModalOpen(true);
  };

  const handleEditChange = ({ target: { name, value } }) =>
    setEditFormData((prevData) => ({ ...prevData, [name]: value }));

  const handleEditSubmit = (e) => {
    e.preventDefault();
    setPaymentMethods((prevMethods) => {
      const updatedMethods = [...prevMethods];
      updatedMethods[currentEditIndex] = editFormData;
      return updatedMethods;
    });
    setIsEditModalOpen(false);
  };

  const filteredPaymentMethods = paymentMethods.filter(
    (method) =>
      method.paymentType.toLowerCase().includes(searchQuery.toLowerCase()) ||
      method.paymentMode.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="bg-cyan-400 p-4 min-h-screen">
      <div className="bg-white rounded-lg shadow-md overflow-hidden mt-10">
        <div className="flex items-center p-4">
          <button className="bg-cyan-300 hover:bg-cyan-400 text-white font-bold py-2 px-4 rounded mr-6">+</button>
          <h2 className="text-xl font-medium text-cyan-600">Payment Method</h2>
          <input
            type="text"
            placeholder="Search"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            className="ml-auto px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
          />
        </div>
        <div className="bg-slate-200 p-4 m-4 rounded-lg shadow-md">
          <table className="w-full text-left text-gray-500 dark:text-gray-400">
            <thead className="bg-white border-b-2">
              <tr>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Logo</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Payment Type</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Payment Mode</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Status</th>
                <th className="px-6 py-3 text-sm font-semibold text-gray-900 uppercase tracking-wider">Action</th>
              </tr>
            </thead>
            <tbody>
              {filteredPaymentMethods.map((method, index) => (
                <PaymentMethodRow
                  key={index}
                  method={method}
                  index={index}
                  handleEdit={handleEdit}
                  handleStatusChange={handleStatusChange}
                />
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {isEditModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-75">
          <div className="bg-white p-6 rounded-lg shadow-lg w-1/3">
            <h2 className="text-2xl font-medium text-cyan-500 mb-4">Edit Payment Method</h2>
            <form onSubmit={handleEditSubmit}>
              {['Payment Type', 'Payment Mode', 'Status'].map((label, idx) => (
                <div key={idx} className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor={label.toLowerCase().replace(' ', '')}>
                    {label}
                  </label>
                  {label === 'Status' ? (
                    <select
                      id="status"
                      name="status"
                      value={editFormData.status}
                      onChange={handleEditChange}
                      className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                    >
                      <option value="Enable">Enable</option>
                      <option value="Disable">Disable</option>
                    </select>
                  ) : (
                    <input
                      id={label.toLowerCase().replace(' ', '')}
                      name={label.toLowerCase().replace(' ', '')}
                      type="text"
                      value={editFormData[label.toLowerCase().replace(' ', '')]}
                      onChange={handleEditChange}
                      className="w-full px-3 py-2 border rounded-lg text-gray-500 focus:outline-none focus:border-cyan-500"
                    />
                  )}
                </div>
              ))}
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsEditModalOpen(false)}
                  className="mr-4 px-4 py-2 bg-gray-200 text-gray-700 rounded hover:bg-gray-300"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="bg-cyan-500 hover:bg-cyan-700 text-white font-medium py-2 px-4 rounded"
                >
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}

export default PaymentMethod;
