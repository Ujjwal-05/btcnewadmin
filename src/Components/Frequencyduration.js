import React, { useState } from "react";
import Switch from "react-switch"; // Import Switch component
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import FontAwesomeIcon
import {faEdit} from "@fortawesome/free-solid-svg-icons"; // Import the trash icon

const FrequencyDuration = () => {
  const [frequencies, setFrequencies] = useState([
    { name: "Every 12 Days", status: "active" },
    { name: "Every Month", status: "active" },
    { name: "Every other month", status: "inactive" },
    { name: "Every other week", status: "active" },
    { name: "Every three weeks", status: "inactive" },
    { name: "Every Week", status: "active" },
    { name: "Pause", status: "inactive" },
    { name: "test", status: "inactive" },
  ]);

  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editedFrequency, setEditedFrequency] = useState(null);

  const handleStatusChange = (index, checked) => {
    const newStatus = checked ? "active" : "inactive";
    setFrequencies((prevFrequencies) => {
      const updatedFrequencies = [...prevFrequencies];
      updatedFrequencies[index].status = newStatus;
      return updatedFrequencies;
    });
  };

  const handleEdit = (index) => {
    setEditedFrequency({ ...frequencies[index], index });
    setEditModalOpen(true);
  };

  const handleCloseModal = () => {
    setEditModalOpen(false);
  };

  const handleSaveChanges = () => {
    setFrequencies((prevFrequencies) => {
      const updatedFrequencies = [...prevFrequencies];
      updatedFrequencies[editedFrequency.index] = {
        name: editedFrequency.name,
        status: editedFrequency.status,
      };
      return updatedFrequencies;
    });
    setEditModalOpen(false);
  };

  const handleAddFrequency = () => {
    // Clear any previous edited frequency data
    setEditedFrequency(null);
    // Open the edit modal for adding a new frequency
    setEditModalOpen(true);
  };

  const handleModalSubmit = () => {
    if (editedFrequency) {
      if (editedFrequency.index !== undefined) {
        // Edit existing frequency
        const updatedFrequencies = [...frequencies];
        updatedFrequencies[editedFrequency.index] = editedFrequency;
        setFrequencies(updatedFrequencies);
      } else {
        // Add new frequency
        const newFrequency = { ...editedFrequency, index: frequencies.length };
        setFrequencies([...frequencies, newFrequency]);
      }
    }
    setEditModalOpen(false);
  };

  return (
    <div className="flex">
      
      <div className="flex-1 p-4"> {/* Adjust margin-left to accommodate Sidebar width */}
        <div className="bg-white rounded-xl shadow-md overflow-hidden p-4">
          <div className="flex justify-between items-center mb-4">
            <h2 className="text-2xl font-light text-cyan-600 flex items-center">
              <button
                className="bg-cyan-300 hover:bg-cyan-400 m-4 shadow-lg text-white font-bold py-2 px-4 rounded"
              >
                +
              </button>
              Frequency Duration
            </h2>
            <button
              className="bg-cyan-300 hover:bg-cyan-400 m-3 shadow-lg text-white font-light py-2 px-4 rounded"
              onClick={handleAddFrequency}
            >
              Add Frequency
            </button>
          </div>
          <div className="p-4 bg-slate-100 rounded-xl">
            <div className="bg-white p-4 rounded-xl">
              <table className="w-full border-collapse">
                <thead>
                  <tr>
                    <th className="px-4 py-2 border-b text-left">Name</th>
                    <th className="px-4 py-2 border-b text-center">Status</th>
                    <th className="px-4 py-2 border-b text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {frequencies.map((frequency, index) => (
                    <tr key={index}>
                      <td className="px-6 py-4 border-b font-light">{frequency.name}</td>
                      <td className="px-6 py-4 border-b text-center font-light">
                        <div className="flex items-center justify-center font-light">
                          <Switch
                            onChange={(checked) => handleStatusChange(index, checked)}
                            checked={frequency.status === "active"}
                            onColor="#66E8F9"
                            onHandleColor="#FFFFFF"
                            handleDiameter={20}
                            uncheckedIcon={false}
                            checkedIcon={false}
                            height={12}
                            width={32}
                            className="react-switch"
                          />
                          <span className="ml-2">
                            {frequency.status === "active" ? "Active" : "Inactive"}
                          </span>
                        </div>
                      </td>
                      <td className="px-4 py-2 border-b text-center">
                        <button
                          className="inline-flex items-center"
                          onClick={() => handleEdit(index)}
                        >
                          <FontAwesomeIcon icon={faEdit} className="" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {/* Edit Modal */}
        {editModalOpen && (
          <div className="fixed top-0 left-0 z-50 w-full h-full flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-white rounded-xl shadow-md p-6 w-full max-w-md">
              <h2 className="text-2xl font-light text-cyan-600 mb-4">
                {editedFrequency ? "Edit Frequency" : "Add Frequency"}
              </h2>
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleModalSubmit();
                }}
              >
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                  <input
                    type="text"
                    className="border-2 border-gray-200 focus:outline-cyan-500 p-2 w-full"
                    value={editedFrequency?.name || ""}
                    onChange={(e) =>
                      setEditedFrequency({
                        ...editedFrequency,
                        name: e.target.value,
                      })
                    }
                  />
                </div>
                <div className="mb-4">
                  <label className="block text-gray-700 text-sm font-bold mb-2">Status:</label>
                  <div className="flex items-center">
                    <Switch
                      onChange={(checked) =>
                        setEditedFrequency({
                          ...editedFrequency,
                          status: checked ? "active" : "inactive",
                        })
                      }
                      checked={editedFrequency?.status === "active"}
                      onColor="#66E8F9"
                      onHandleColor="#FFFFFF"
                      handleDiameter={20}
                      uncheckedIcon={false}
                      checkedIcon={false}
                      height={12}
                      width={32}
                      className="react-switch"
                    />
                    <span className="ml-2">
                      {editedFrequency?.status === "active" ? "Active" : "Inactive"}
                    </span>
                  </div>
                </div>
                <div className="flex justify-end">
                  <button
                    type="button"
                    className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded mr-2"
                    onClick={handleCloseModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="submit"
                    className="bg-cyan-500 hover:bg-cyan-600 text-white font-bold py-2 px-4 rounded"
                  >
                    {editedFrequency ? "Save Changes" : "Add Frequency"}
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default FrequencyDuration;
